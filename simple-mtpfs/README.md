# Simple-mtpfs upstream
https://github.com/phatina/simple-mtpfs/
Upstream has its own debian/ directory but it was only a reference and not a wholesale source for this dpkg.

# Reason for being in stackrpms
Devuan does not package simple-mtpfs and I want to use it.

# Reverse dependency matrix
Distro         | simple-mtpfs version
-------------- | --------------------
Devuan Ceres   | git master (c9518f3)

# Differences from upstream packaging info
None
