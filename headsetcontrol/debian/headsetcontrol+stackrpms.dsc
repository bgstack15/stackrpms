Format: 3.0 (quilt)
Source: headsetcontrol
Binary: headsetcontrol
Architecture: any
Version: 3.0.0-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/Sapd/HeadsetControl
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/supermario-pkgs/headsetcontrol
Vcs-Git: https://gitlab.com/supermario-pkgs/headsetcontrol.git
Build-Depends: debhelper-compat (= 13), libhidapi-dev, cmake
Package-List:
 headsetcontrol deb sound optional arch=any
Files:
 00000000000000000000000000000000 1 headsetcontrol.orig.tar.gz
 00000000000000000000000000000000 1 headsetcontrol.debian.tar.xz
