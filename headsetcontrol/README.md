# README for headsetcontrol
This is a dpkg for [headsetcontrol](https://github.com/Sapd/HeadsetControl)

## Upstream
The application is at the above link. My dpkg code is hosted at <https://bgstack15.ddns.net/cgit/stackrpms/tree/headsetcontrol>.

## Alternatives
Use iCue software from Corsair vendor, which appears to be Windows only.

## Resaon for existence
I am repackaging and improving an existing dpkg recipe for headsetcontrol ([debian/ vcs](https://gitlab.com/supermario-pkgs/headsetcontrol), [obs](https://build.opensuse.org/package/show/home:mfinelli:supermario/headsetcontrol)).

## Using
Install the package. Plug in the dongle. Configure pulseaudio to use the headset as the default input and output sink.

## Building
Standard debuild. The package author is sane and has a small application with minimal dependencies.

## References
All links are inline above.
