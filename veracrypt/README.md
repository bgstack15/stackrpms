# VeraCrypt upstream
https://www.veracrypt.fr/code/VeraCrypt/snapshot/VeraCrypt_1.24.tar.gz
https://gitlab.com/bgstack15/big-sources/raw/master/veracrypt/VeraCrypt_1.24.tar.gz
Rpm spec forked from [scx copr](https://copr-be.cloud.fedoraproject.org/results/scx/veracrypt/fedora-27-x86_64/00657099-veracrypt/veracrypt.spec which also provides) which also provides the GTK2/3 options
My dpkg is now an overlay on top of [Unit193 on salsa](https://salsa.debian.org/debian-edu-pkg-team/veracrypt) which is also at his [cgit](https://git.unit193.net/cgit/veracrypt.git)

# Reason for being in stackrpms
Fedora does not package veracrypt because its license is tricky. Devuan does not package the application either. I want to compile this myself, and also I don't need some of the systemd features in the unit193 debianized package anyways.

# Alternatives
* pmdpalma
  * [fedora copr](https://copr.fedorainfracloud.org/coprs/pmdpalma/bucket/build/712274/)
  * [opensuse variant](https://build.opensuse.org/package/view_file/home:pmdpalma:bucket/veracrypt/veracrypt.spec?expand=1)
* [scx](https://copr-be.cloud.fedoraproject.org/results/scx/veracrypt/fedora-27-x86_64/00657099-veracrypt/veracrypt-1.21-3.fc27.src.rpm)
* [cyfrost](https://copr.fedorainfracloud.org/coprs/cyfrost/packages/package/veracrypt/)
* [Unit193 on Launchpad (Ubuntu)](https://launchpad.net/~unit193/+archive/ubuntu/encryption/+packages)

# Reverse dependency matrix
Distro     | VeraCrypt version
---------- | -----------------
Devuan     | 1.26.14

# Differences from upstream
##Dpkg
stackrpms/veracrypt/debian/stackrpms-diff.sh | vi -
