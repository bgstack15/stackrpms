#!/bin/sh
# Please make sure this salsa mirror is on branch upstream
cd ~/dev
diff -x 'stackrpms-diff.sh' -x '.*swp' -Naur salsa/veracrypt/debian stackrpms/veracrypt/debian
