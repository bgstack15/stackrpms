Format: 3.0 (quilt)
Source: veracrypt
Binary: veracrypt
Architecture: any
Version: 1.26.20-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://veracrypt.fr/
Standards-Version: 4.6.0
Build-Depends: debhelper-compat (= 12), libayatana-appindicator3-dev | libappindicator3-dev, libfuse3-dev, libpcsclite-dev, libwxgtk3.2-dev | libwxgtk3.0-gtk3-dev, pkgconf, yasm [any-i386 any-amd64]
Package-List:
 veracrypt deb non-free/utils optional arch=any
Files:
 00000000000000000000000000000000 1 veracrypt.orig.tar.gz
 00000000000000000000000000000000 1 veracrypt+stackrpms.debian.tar.xz
