# ccextractor upstream
https://negativo17.org/repos/multimedia/fedora-30/SRPMS/ccextractor-0.88-1.fc30.src.rpm

# Reason for being in stackrpms

Just to not depend on external repos too much. This package is not built in the copr; this is kind of just as a backup to Negativo17.

# Reverse dependency matrix
Distro     | makemkv version | ccextractor version
---------- | --------------- | -------------------
All        | 1.14.5          | any (0.88)

# Differences from upstream
None
