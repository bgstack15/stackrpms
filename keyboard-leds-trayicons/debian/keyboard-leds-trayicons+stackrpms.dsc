Format: 3.0 (quilt)
Source: keyboard-leds-trayicons
Binary: keyboard-leds-trayicons
Architecture: all
Version: 0.0.2-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://gitlab.com/bgstack15/keyboard-leds-trayicons
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), go-md2man, graphicsmagick-imagemagick-compat | imagemagick-6.q16
Package-List:
 keyboard-leds-trayicons deb x11 optional arch=all
Files:
00000000000000000000000000000000 1 keyboard-leds-trayicons.orig.tar.gz
00000000000000000000000000000000 1 keyboard-leds-trayicons+devuan.debian.tar.xz
