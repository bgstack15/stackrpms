# libdvdcss upstream
Forked from https://negativo17.org/repos/multimedia/fedora-30/SRPMS/libdvdcss-1.4.2-2.fc30.src.rpm

# Alternatives
Debian packages libdvdcss in a separate manner, as package libdvd-pkg.

# Reason for being in stackrpms
CentOS and Fedora do not package libdvdcss natively, and I did not want to depend on external repositories long-term. I disliked how Devuan packaged libdvdcss so I packaged it myself.

# Reverse dependency matrix
Distro  | libdvdcss version
------- | ---------------
rpm     | 1.4.2
dpkg    | 1.4.2

# Differences from upstream
See file [stackrpms-libdvdcss.spec.diff](stackrpms-libdvdcss.spec.diff)
