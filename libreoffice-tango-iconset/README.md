# Readme for libreoffice-tango-iconset

## libreoffice-tango-iconset upstream
https://extensions.libreoffice.org/en/extensions/show/tango-icon-theme-for-libreoffice

## Alternatives
Use LibreOffice < 7.0.0.

## Reason for being in stackrpms
LibreOffice 7.0.0 removed the Tango icon set from its defaults. The icon set is still available as an extension, so this package does a system-wide deployment of the extension so users can choose "Tango" icon set in the settings dialog.

## Reverse dependency matrix
Distro     | LibreOffice version | libreoffice-tango-iconset version
---------- | ------------------- | ---------------------------------
All dpkg   | >7.0.0              | 1.0

## References
Starting with LibreOffice 7.0.0 [1], the tango icon set is now absent from
LibreOffice. The icons themselves are still available as an extension
[2] that a user or admin can install [3]. A user then needs to change the
view settings for icon theme to "tango."

This package was adapted from debian sources including [4] and [5].

1. https://salsa.debian.org/libreoffice-team/libreoffice/libreoffice/-/blob/master/changelog
2. https://extensions.libreoffice.org/en/extensions/show/tango-icon-theme-for-libreoffice
3. https://wiki.documentfoundation.org/Documentation/HowTo/install_extension
4. https://salsa.debian.org/libreoffice-team/libreoffice/libreoffice/-/blob/libreoffice_6.4.5-1/control
5. https://snapshot.debian.org/archive/debian/20200627T204538Z/pool/main/libr/libreoffice/libreoffice-style-tango_6.4.5%7Erc1-2%7Ebpo10%2B1_all.deb

## Differences from upstream
Upstream is just the .oxt file which is a zip file of the extension's contents. So the entire packaging instructions were adapted from Debian's old package `libreoffice-style-tango`.
