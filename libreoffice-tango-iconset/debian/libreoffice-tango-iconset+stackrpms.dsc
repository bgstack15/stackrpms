Format: 3.0 (quilt)
Source: libreoffice-tango-iconset
Binary: libreoffice-tango-iconset
Architecture: all
Version: 1.0-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://extensions.libreoffice.org/en/extensions/show/tango-icon-theme-for-libreoffice
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), p7zip-full
Package-List:
 libreoffice-tango-iconset deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 libreoffice-tango-iconset.orig.tar.gz
 00000000000000000000000000000000 1 libreoffice-tango-iconset.debian.tar.xz
