#!/bin/sh
# Package: libreoffice-tango-iconset
# Startdate: 2020-08-01 23:36
# References: notepadpp/build-orig-tarball.sh
# Dependencies:
#    sudo apt-get install wget curl

currdir="${PWD}"
tmpfile1="$( mktemp )"
frontpageurl="https://extensions.libreoffice.org/en/extensions/show/tango-icon-theme-for-libreoffice"
domain="$( echo "${frontpageurl}" | awk -F'/' 'BEGIN{OFS="/"} {print $1,$2,$3}' )"
package_name="libreoffice-tango-iconset"

# FUNCTIONS
ferror() {
   printf "%s\n" "$@" 1>&2
}

get() {
   # get "https://example.com/example.zip"
   # get "https://example.com/example.zip" "outputname.zip"
   ___get_url="${1}"
   ___get_outfile="${2}"
   ___wget_options=""
   test -n "${___get_outfile}" && ___wget_options="-o ${___get_outfile}"

   test -n "${DEBUG}" && ferror "wget --quiet --content-disposition \"${___get_url}\" ${___wget_options}"
   test -z "${DRYRUN}" && wget --quiet --content-disposition "${___get_url}" ${___wget_options}
}

to_filename() {
   # call: to_filename "https://example.com/filename.ext"
   # returns: "filename.ext"
   printf "${1##*/}"
}

### Flow

# check dependencies
#which jq 1>/dev/null 2>&1 || { echo "Please install jq! Aborted." ; exit 1; }

## 1. learn latest version file
url_contents="$( curl -s "${frontpageurl}" )"
latest_file="$( echo "${url_contents}" | awk -F'[=>]' '/class="btn".*>Download</{print $4}' | sed -r -e 's/^"//g;' -e 's/"$//g;' | head -n1 )"
latest_version="$( echo "${url_contents}" | grep -A1 'class="releaseRow"' | grep -E '<span>[0-9\.]+</span>' | sed -r -e 's/\s*<\/?span>\s*//g;' )"
### add domain as needed to filename
if ! echo "${latest_file}" | grep -q "^(ht|f)tps?:\/\/" ;
then
   latest_file="${domain}/${latest_file}"
fi

## 2. dl it
# WORKHERE
get "${latest_file}"

## 5. assemble orig tarball
test -n "${DEBUG}" && ferror "mkdir -p \"${package_name}-${latest_version}\""
test -z "${DRYRUN}" && { mkdir -p "${package_name}-${latest_version}" ; cd "${package_name}-${latest_version}" ; }

test -n "${DEBUG}" && ferror "mv ../\"$( basename "${latest_file}" )\" ."
test -z "${DRYRUN}" && mv ../"$( basename "${latest_file}" )" .

test -n "${DEBUG}" && ferror "cd .."
test -z "${DRYRUN}" && cd ..

test -n "${DEBUG}" && ferror "tar -zcf \"${package_name}_${latest_version}.orig.tar.gz\" \"${package_name}-${latest_version}\""
test -z "${DRYRUN}" && tar -zcf "${package_name}_${latest_version}.orig.tar.gz" "${package_name}-${latest_version}"

# CLEAN UP
rm -rf "${package_name}-${latest_version}/"
