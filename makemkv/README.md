# makemkv upstream
https://negativo17.org/repos/multimedia/fedora-30/SRPMS/makemkv-1.14.5-1.fc30.src.rpm
https://www.makemkv.com/forum/viewtopic.php?f=5&t=1053

# Reason for being in stackrpms
Fedora does not package makemkv because it is only partially open-source. I do not want to depend on external repos too much. I also include some logic for always being able to run makemkv.
As of makemkv 1.14.7, I am diverging from negativo17's spec file.

# Reverse dependency matrix
Distro     | makemkv version
---------- | ---------------
All        | 1.16.4

# Differences from upstream
See file [stackrpms-makemkv.spec.diff](stackrpms-makemkv.spec.diff) which was last applied on version 1.14.5.
