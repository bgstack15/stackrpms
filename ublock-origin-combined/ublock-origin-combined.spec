%global extdir1 %{_libdir}/palemoon/browser/extensions
%global extdir2 %{_libdir}/palemoon-stackrpms/browser/extensions
%global extdir3 %{_datadir}/waterfox/extensions/%{app_id}
%global extdir4 %{_libdir}/newmoon/browser/extensions

%global app_id \{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}

Name:           ublock-origin-combined
Version:        1.16.4.30
Release:        1
Summary:        uBlock Origin installed to the system Mozilla-based extensions directories

Group:          Applications/Internet
License:        GPLv3
URL:            https://github.com/gorhill/uBlock
Source0:        README.md
Source1:        https://github.com/gorhill/uBlock-for-firefox-legacy/releases/download/firefox-legacy-%{version}/uBlock0_%{version}.firefox-legacy.xpi
BuildRequires:  unzip
BuildRequires:  coreutils
BuildArch:      noarch

Obsoletes:      palemoon-ublock-origin <= 1.16.4.25-1, palemoon-stackrpms-ublock-origin <= 1.16.4.25-1, waterfox-ublock-origin <= 1.16.4.25-1
Conflicts:      palemoon-ublock-origin, palemoon-stackrpms-ublock-origin, waterfox-ublock-origin, newmoon-ublock-origin

%if 0%{?fedora} || ( 0%{?rhel} >= 8 )
Suggests:       mozilla-filesystem
Suggests:       waterfox-filesystem
%endif

%description
The "legacy" release of uBlock Origin (NPAPI) for tradional Mozilla-style
web browsers. This deploys the extension for multiple Mozilla-based
browsers.

More than just an ad blocker, uBlock Origin provides the ability to block
at will and easy defaults based on various common lists, such as EasyList,
EasyPrivacy, and Peter Lowe's ad/tracking/malware servers.

%prep
#%setup -q -c

%build

%install
# learn extension id to name the file
%{__unzip} -o %{SOURCE1} install.rdf
newfilename="$( grep 'em:id' install.rdf 2>/dev/null | head -n1 | sed -r -e 's/^.*<em:id>//;' -e 's/<\/em:id>.*$//;' ).xpi"
test -z "${newfilename}" && newfilename="$( basename %{SOURCE1} )"
rm -rf install.rdf

mkdir -p %{buildroot}%{extdir1} %{buildroot}%{extdir2} %{buildroot}%{extdir3} %{buildroot}%{extdir4}

# palemoon
install -Dp -m0644 %{SOURCE1} %{buildroot}%{extdir1}/${newfilename}
# firefox
ln -s %{extdir1}/${newfilename} %{buildroot}%{extdir2}
# newmoon
ln -s %{extdir1}/${newfilename} %{buildroot}%{extdir4}
# waterfox
ln -sf %{extdir1}/${newfilename} %{buildroot}%{extdir3}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{extdir1}/*.xpi
%{extdir2}/*.xpi
%{extdir3}/../*/*.xpi
%{extdir4}/*.xpi
# something gets weird because of the curly braces in the expanded variable, but using the ../* syntax changes the evaluated string so it does not have to try to use curly braces in the owned filepath.

%changelog
* Sat Jul 24 2021 B. Stack <bgstack15@gmail.com> - 1.16.4.30-1
- Update

* Tue Jun 15 2021 B. Stack <bgstack15@gmail.com> - 1.16.4.29-1
- Update

* Thu Mar 18 2021 B. Stack <bgstack15@gmail.com> - 1.16.4.28-2
- Add newmoon

* Mon Feb 01 2021 B. Stack <bgstack15@gmail.com> - 1.16.4.28-1
- Update

* Tue Jan 05 2021 B. Stack <bgstack15@gmail.com> - 1.16.4.27-1
- Update

* Wed Nov 04 2020 B Stack <bgstack15@gmail.com> - 1.16.4.26-1
- Update

* Tue Oct 20 2020 B Stack <bgstack15@gmail.com> - 1.16.4.25-1
- Forked from palemoon-ublock-origin

* Tue Aug 25 2020 B Stack <bgstack15@gmail.com> - 1.16.4.25-1
- Update

* Tue Aug 11 2020 B Stack <bgstack15@gmail.com> - 1.16.4.24-1
- Update

* Wed Jul 22 2020 B Stack <bgstack15@gmail.com> - 1.16.4.23-1
- Update

* Sat Jul 04 2020 B Stack <bgstack15@gmail.com> - 1.16.4.22-1
- Update

* Mon Jun 29 2020 B Stack <bgstack15@gmail.com> - 1.16.4.21-1
- Update

* Wed Mar 18 2020 B Stack <bgstack15@gmail.com> - 1.16.4.20-1
- Update

* Tue Mar 03 2020 B Stack <bgstack15@gmail.com> - 1.16.4.19-1
- Update

* Tue Feb 18 2020 B Stack <bgstack15@gmail.com> - 1.16.4.18-1
- Update

* Mon Jan 27 2020 B Stack <bgstack15@gmail.com> - 1.16.4.16-1
- Update
- Upstream changed locations

* Tue Jan 14 2020 B Stack <bgstack15@gmail.com> - 1.16.4.14-1
- Update

* Wed Dec 18 2019 B Stack <bgstack15@gmail.com> - 1.16.4.12-1
- Update

* Mon Nov 11 2019 B Stack <bgstack15@gmail.com> - 1.16.4.11-1
- Update

* Sat Sep 15 2018 B Stack <bgstack15@gmail.com> 1.16.4.4-2
- Customize for stackrpms

* Wed Aug 2 2017 Ian Firns <firnsy@kororaproject.org>- 1.13.8-1
- Update to 1.13.8 release

* Thu Jan 7 2016 Chris Smart <csmart@kororaproject.org>- 1.9.16-1
- Update to 1.9.16 release

* Thu Jan 7 2016 Chris Smart <csmart@kororaproject.org>- 1.5.3-1
- Update to 1.5.3 release
- Use signed xpi from Mozilla so that it works in Firefox 43

* Tue Nov 3 2015 Chris Smart <csmart@kororaproject.org>- 1.3.2-1
- Initial build
