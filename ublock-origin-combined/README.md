# README for uBlock Origin

## uBlock Origin upstream
https://github.com/gorhill/uBlock/tags
Forked from deprecated project https://github.com/kororaproject/kp-mozilla-ublock-origin/blob/master/build/mozilla-ublock-origin.spec
https://github.com/PhantomX/chinforpms/blob/master/waterfox-ublock-origin/waterfox-ublock-origin.spec

## Reason for being in stackrpms
I bundle uBlock Origin on the system level for the various Mozilla-based web browsers. There was some other distro (perhaps SolydXk or PCLinuxOS), that bundled uBlock Origin and gave me the idea of bundling this. Korora was one of them.
This package is now maintained independently.
As of October 2020, this package, ublock-origin-combined, replaces the following packages (both rpm and dpkg):
* palemoon-ublock-origin
* palemoon-stackrpms-ublock-origin
* waterfox-ublock-origin
* newmoon-ublock-origin

## Reverse dependency matrix
Distro     | uBlock Origin version
---------- | ---------------
All        | 1.16.4.25

## Differences from upstream
N/A
