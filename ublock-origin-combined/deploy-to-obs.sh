#!/bin/sh
# Startdate: 2020-11-04 15:06
# Purpose: make it a oneliner to get into the OBS
# Dependencies:
#    osc

devdir=~/dev
gitdir="${devdir}/stackrpms/ublock-origin-combined"
obsdir="${devdir}/osc/home:bgstack15/ublock-origin-combined"

# Flow
cd "${devdir}"
"${gitdir}/build-orig-tarball.sh"
echo "DONE WITH build-orig-tarball"
tf="$( find . -maxdepth 1 -name 'ublock*orig*z' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 )"
tar -zxf "${tf}"
cd "$( tar -zvtf "${tf}" | awk '/^d/{print $NF}' | head -n1 )"
cp -pr "${gitdir}/debian" .
debuild -us -uc # depends on dpkg-dev=1.19.7 and not 1.20.5 which is still broken in Ceres as of 2020-11-04
test $? -eq 0 || { "debuild failed. Cannot continue. Aborted." ; exit 1 ; }
cd "${devdir}"
dsc_file="$( find . -maxdepth 1 -name 'ublock*dsc' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 | cut -d' ' -f2 )"
debian_tar_file="$( find . -maxdepth 1 -name 'ublock*debian*z' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 | cut -d' ' -f2 )"

# prep obs
cd "${obsdir}"
osc up
osc rm *
cd "${devdir}"
cp -p "${dsc_file}" "${debian_tar_file}" "${tf}" "${obsdir}/"
cd "${obsdir}"
#find . -mindepth 1 -maxdepth 1 ! -name "${dsc_file}" ! -name "${debian_tar_file}" ! -name "${tf}" -delete
osc add *
osc commit # will prompt for commit name
