#!/bin/sh
# Package: libreoffice-tango-iconset
# Startdate: 2020-11-04 14:05
# References:
#    libreoffice-tango-iconset/build-orig-tarball.sh
# Dependencies:
#    sudo apt-get install wget curl sed awk

currdir="${PWD}"
tmpfile1="$( mktemp )"
#frontpageurl="https://extensions.libreoffice.org/en/extensions/show/tango-icon-theme-for-libreoffice"
#domain="$( echo "${frontpageurl}" | awk -F'/' 'BEGIN{OFS="/"} {print $1,$2,$3}' )"
package_name="ublock-origin-combined"

# FUNCTIONS
ferror() {
   printf "%s\n" "$@" 1>&2
}

get() {
   # get "https://example.com/example.zip"
   # get "https://example.com/example.zip" "outputname.zip"
   ___get_url="${1}"
   ___get_outfile="${2}"
   ___wget_options=""
   test -n "${___get_outfile}" && ___wget_options="-O ${___get_outfile}"

   test -n "${DEBUG}" && ferror "wget --quiet --content-disposition \"${___get_url}\" ${___wget_options}"
   test -z "${DRYRUN}" && wget --quiet --content-disposition "${___get_url}" ${___wget_options}
}

to_filename() {
   # call: to_filename "https://example.com/filename.ext"
   # returns: "filename.ext"
   printf "${1##*/}"
}

show_latest_tarball_for_github_repo() {
   # call: show_latest_tarball_for_github_repo "https://github.com/gorhill/uBlock-for-firefox-legacy"
   # returns: "https://github.com/gorhill/uBlock-for-firefox-legacy/archive/firefox-legacy-1.16.4.26.tar.gz"
   # Improve: accept archive type, such as .tar.gz or .zip, to return
   # And yes, I know this parses html with awk. Get over it.
   # Works, but unused as of 2020-11-04
   ___repo="${1}"
   _page="$( curl -s "${___repo}/tags" )"
   # tail grabs the highest number, so most recent available tarball from the page
   echo "${_page}" | grep -oE "href=[\"'][^\"']+archive[^\"']+tar\.gz\"" | sed -r -e 's/^href=.//;' -e 's/"$//;' | sort -n | uniq | tail -n1 | sed -r -e "s/^/https:\/\/github.com/;"
}

show_xpi_for_latest_tag() {
   # call: fetch_xpi_for_latest_tag "https://github.com/gorhill/uBlock-for-firefox-legacy"
   # returns: "https://github.com/gorhill/uBlock-for-firefox-legacy/releases/download/firefox-legacy-1.16.4.26/uBlock0_1.16.4.26.firefox-legacy.xpi"
   # How it works:
   # 1. visit list of tags
   # 2. visit highest sorted tag (can only see page one of github results)
   # 3. visit that tag page, and find assets section and then find .xpi file. So this is hard-coded for exactly one xpi file.
   ___repo="${1}"
   _page1="$( curl -s "${___repo}/tags" )"
   _url2="https://github.com$( echo "${_page1}" | grep -oE 'href=[^>]+\/tag\/[^>]+' | sort -n | tail -n1 | sed -r -e "s/^href=[\"']?//;" -e 's/"$//;' )"
   _page2="$( curl -s "${_url2}" )"
   #echo "${_page2}"
   _asset="https://github.com$( echo "${_page2}" | sed -n '/Assets/,$p' | grep -oE "href=.*\.xpi" | sed -r -e "s/^href=[\"']?//;" )"
   echo "${_asset}"
}

### Flow

# check dependencies
#which jq 1>/dev/null 2>&1 || { echo "Please install jq! Aborted." ; exit 1; }

## 1. learn latest version file
#url_contents="$( curl -s "${frontpageurl}" )"
#latest_file="$( echo "${url_contents}" | awk -F'[=>]' '/class="btn".*>Download</{print $4}' | sed -r -e 's/^"//g;' -e 's/"$//g;' | head -n1 )"
latest_file="$( show_xpi_for_latest_tag "https://github.com/gorhill/uBlock-for-firefox-legacy" )"
latest_version="$( basename "${latest_file}" | grep -oE '_[0-9\.]+' | sed -r -e 's/^_//;' -e 's/\.$//;' )"

## 2. dl it
get "${latest_file}" "uBlock0.firefox-legacy.xpi"

## 5. assemble orig tarball
test -n "${DEBUG}" && ferror "mkdir -p \"${package_name}-${latest_version}\""
test -z "${DRYRUN}" && { mkdir -p "${package_name}-${latest_version}" ; cd "${package_name}-${latest_version}" ; }

test -n "${DEBUG}" && ferror "mv ../\"uBlock0.firefox-legacy.xpi\" ."
test -z "${DRYRUN}" && mv ../"uBlock0.firefox-legacy.xpi" .

test -n "${DEBUG}" && ferror "cd .."
test -z "${DRYRUN}" && cd ..

test -n "${DEBUG}" && ferror "tar -zcf \"${package_name}_${latest_version}.orig.tar.gz\" \"${package_name}-${latest_version}\""
test -z "${DRYRUN}" && tar -zcf "${package_name}_${latest_version}.orig.tar.gz" "${package_name}-${latest_version}"

# CLEAN UP
rm -rf "${package_name}-${latest_version}/" "${tmpfile1}"
