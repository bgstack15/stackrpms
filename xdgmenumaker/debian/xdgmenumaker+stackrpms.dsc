Format: 3.0 (quilt)
Source: xdgmenumaker
Binary: xdgmenumaker
Architecture: any
Version: 2.3-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/gapan/xdgmenumaker
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), dh-python, txt2tags
Package-List:
 xdgmenumaker deb x11 optional arch=any
Files:
 00000000000000000000000000000000 1 xdgmenumaker.orig.tar.gz
 00000000000000000000000000000000 1 xdgmenumaker+stackrpms.debian.tar.xz
