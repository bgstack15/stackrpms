# xdgmenumaker upstream
https://github.com/gapan/xdgmenumaker

# Reason for being in stackrpms
My distros do not package xdgmenumaker, so I build it for myself. I use Fluxbox, which does not natively use xdg menus, but I want application discovery. This menu, combined with a fluxbox config to use it, will make that discovery possible.

Install xdgmenumaker.
Configure fluxbox to use it upon startup.

    /usr/bin/xdgmenumaker -f fluxbox -i > ~/.fluxbox/xdg-menu &

Configure fluxbox menu to include this file.

    # cat ~/.flubox/menu
    [begin] (fluxbox)
    [include] (.fluxbox/xdg-menu)
    [include] (/etc/X11/fluxbox/fluxbox-menu)
    [end]

# Alternatives
https://wiki.archlinux.org/index.php/Xdg-menu

# Reverse dependency matrix
Distro     | xdgmenumaker version
---------- | ----------------
All        | 1.6

# Differences from upstream
None
