# Readme for xzoom
## Overview
xzoom is a small screen magnification utility that stays focused on the same spot until you move its focus.

## Upstream
The original upstream is [https://webdiis.unizar.es/pub/unix/X11/xzoom-0.3.tgz](https://webdiis.unizar.es/pub/unix/X11/xzoom-0.3.tgz).
Debian packages this on [salsa](https://salsa.debian.org/debian/xzoom).
SUSE packages exist too: [1](https://build.opensuse.org/package/show/home:zhonghuaren/xzoom) [2](https://build.opensuse.org/package/show/home:Akoellh/xzoom)

## Reason for being in stackrpms
Fedora does not package any third-party loupe programs.

## Alternatives
I could not find my old non-free OS application i.Look, but it runs in Wine anyways.
`kmag` sounds like a decent KDE alternative, but I didn't want to install 127MB of KDE just for a screen magnifier.
GNOME Shell has some built-in viewer, but I do not use GNOME.

## Dependencies
Distro     | Packages
---------- | ---------
Fedora 32  | bash, glibc, libX11, libXext, libXt (these are all standard packages)

## Differences from upstream
Spec file is modified from the Suse ones to be more Fedora-like, based on [Fedora Packaging Guidelines](https://docs.fedoraproject.org/en-US/packaging-guidelines/).
