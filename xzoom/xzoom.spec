# Startdate: 2021-02-16
# References
#    spec adapted mostly from https://build.opensuse.org/package/view_file/home:Akoellh/xzoom/xzoom.spec?expand=1
#    https://build.opensuse.org/package/show/home:zhonghuaren/xzoom
#    https://salsa.debian.org/debian/xzoom
%define debug_package %{nil}
Name:           xzoom
Version:        0.3
Release:        1
License:        X11
Summary:        Magnify part of X display, with real-time updates
Url:            https://webdiis.unizar.es/pub/unix/X11
Group:          System/X11/Utilities
Source0:        https://webdiis.unizar.es/pub/unix/X11/%{name}-%{version}.tgz
Source1:        https://salsa.debian.org/debian/xzoom/-/raw/debian/master/debian/xzoom.desktop
### Debian upstream patch
Patch0:         https://cdn-fastly.deb.debian.org/debian/pool/main/x/%{name}/%{name}_0.3-24.diff.gz
Patch1:         %{name}-0.3.shm.diff
Patch2:         001-geometry.patch
BuildRequires:  imake
BuildRequires:  desktop-file-utils
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xt)

%description

Like xmag, xzoom magnifies a section of the X display. Xzoom is different
because it will continuously update the magnified area as the display
changes. It is fast enough to enlarge small animations, for example.

It can also mirror or rotate a part of the screen.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
cp -p %{SOURCE1} .

%build
xmkmf
make %{?_smp_mflags}

%install
install -Dm 755 %{name} $RPM_BUILD_ROOT%{_bindir}/%{name}
install -Dm 644 %{name}.man $RPM_BUILD_ROOT%{_mandir}/man1/%{name}.1
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%posttrans
update-desktop-database 1>/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*
%doc README %{name}.lsm

%changelog
* Tue Feb 16 2021 B. Stack <bgstack15@gmail.com> - 0.3-1
- Build for Fedora 32 and above

* Thu Oct 12 2017 <AxelKoellhofer@web.de> - 0.3
- initial package, based on upstream packages from Debian and Fedora

* Wed Nov 13 2013 Huaren Zhong <huaren.zhong@gmail.com> 0.3
- Rebuild for Fedora

* Tue Apr 12 2011 Fr. Br. George <george@altlinux.ru> 0.3-alt3
- BuildRequires recalculated

* Tue Dec 09 2008 Fr. Br. George <george@altlinux.ru> 0.3-alt2
- libXext-devel added

* Mon Aug 27 2007 Fr. Br. George <george@altlinux.ru> 0.3-alt1
- Initial build for ALT

* Mon Nov 11 2002 - ro@suse.de
- changed neededforbuild <xf86 xdevel> to <x-devel-packages>

* Mon Dec 03 2001 - egmont@suselinux.hu
- Initial release
