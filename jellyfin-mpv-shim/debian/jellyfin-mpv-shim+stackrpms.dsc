Format: 3.0 (quilt)
Source: jellyfin-mpv-shim
Binary: jellyfin-mpv-shim
Architecture: any
Version: 2.6.0-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/jellyfin/jellyfin-mpv-shim
Standards-Version: 4.5.1
Testsuite: autopkgtest
Build-Depends: debhelper-compat (= 13), dh-python, python3-all-dev, python3-webview, python3-setuptools, jellyfin-apiclient-python (>= 1.9.2)
Package-List:
 jellyfin-mpv-shim deb video optional arch=any
Checksums-Sha1:
 75e1b1e11e09f96be2521e32c209b2f3879b5813 3297690 jellyfin-mpv-shim_2.6.0.orig.tar.gz
 0d1e727f6c52c96324c6f917ad469c37bc2a930e 15028 jellyfin-mpv-shim_2.6.0-1+stackrpms.debian.tar.xz
Checksums-Sha256:
 a8d01582e5c64a92b3b64b2e4612ee788fb92b81ebd4008d03140569bf28be46 3297690 jellyfin-mpv-shim_2.6.0.orig.tar.gz
 651496806e6da62796d78e52e2f25ad19d3273a724fa212f2117f64f1b659e9a 15028 jellyfin-mpv-shim_2.6.0-1+stackrpms.debian.tar.xz
Files:
 85eda0c90615b1671ca32b136f3e0734 3297690 jellyfin-mpv-shim_2.6.0.orig.tar.gz
 88075846328615f8e8c957199a906e44 15028 jellyfin-mpv-shim_2.6.0-1+stackrpms.debian.tar.xz
