#!/bin/sh
# Startdate: 2024-01-25-5 15:11
# Reference: scite/stackrpms-diff.sh
# Usage: stackrpms/waterfox-g/stackrpms-diff.sh | vi -
# Purpose: handle the renamed files because of dropping the -kpe suffix
# History:
#    2024-06-27 updated to upse hawkeye upstream waterfox-kde which replaced waterfox-g-kpe
left="/usr/src/waterfox/waterfox-deb-rpm-arch-AppImage/waterfox-kde"
right="stackrpms/waterfox-g/debian"
cd ~/dev
# do diff color=always if this is going to a terminal
_diff_color="never"
\test -c /proc/$$/fd/1 && _diff_color="always"
{ test -n "${DIFF_COLOR}" || test -n "${DIFF_COLORS}" ; } && { _diff_color="always" ; }
diff -x '.*.swp' \
   --exclude-from="${right}/../diff-excludes" \
   --color="${_diff_color}" \
   -aur "${left}" "${right}"
for word in dirs dsc install links lintian-overrides manpages postinst postrm preinst prerm links ;
do
   _result="$( diff --color="${_diff_color}" -aur "${left}/waterfox-kde.${word}" "${right}/waterfox.${word}" )"
   test -n "${_result}" && {
      echo diff -aur "${left}/waterfox-kde.${word}" "${right}/waterfox.${word}"
      echo "${_result}"
   }
done
