# Readme for Waterfox G
Waterfox G is a customizable privacy-conscious web browser with primary support for webextensions.

## Upstream
<https://www.waterfox.net/>
Forked from <https://build.opensuse.org/package/show/home:hawkeye116477:waterfox/waterfox-g-kpe>

## Reason for being in stackrpms
I want a non-KDE focused Waterfox G package for myself. Hawkeye builds his with KDE integrations that are unnecessary for my use case.

## Alternatives
* Hawkeye116477's package

## Dependencies
See the build recipe.

## Using
It is very similar to Firefox or Librewolf.

## Building
This package is designed to be built on the [Open Build Service](https://build.opensuse.org).

### Credits
This package, is made possible by the concerted efforts of many people and groups.
* [Mozilla Firefox](https://www.mozilla.org)
* [Hawkeye116477](https://build.opensuse.org/project/subprojects/home:hawkeye116477)
* My work on [waterfox-classic package](https://bgstack15.ddns.net/cgit/stackrpms/tree/waterfox) which was also based on Hawkeye116477's work.

### References

## Differences from upstream
I strip out KDE things and add my prefs.js file.
