Format: 3.0 (quilt)
Source: spacecadetpinball
Binary: spacecadetpinball
Architecture: any
Version: 2.1.0-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/k4zmu2a/SpaceCadetPinball
Standards-Version: 4.6.1
Build-Depends: debhelper-compat (= 13), cmake, libsdl2-dev, libsdl2-mixer-dev
Package-List:
 spacecadetpinball deb games optional arch=any
Files:
 00000000000000000000000000000000 1 spacecadetpinball.orig.tar.gz
 00000000000000000000000000000000 1 spacecadetpinball.debian.tar.xz
