#!/bin/sh
# Startdate: 2024-11-01-6 16:12
# Purpose: make it a oneliner to get into the OBS
# Reference:
#    irfanview/deploy-to-obs.sh
# Dependencies:
#    osc

devdir=~/dev
gitdir="${devdir}/stackrpms/spacecadetpinball"
obsdir="${devdir}/osc/home:bgstack15/spacecadetpinball"

# Flow
cd "${devdir}"
sh -x "${gitdir}/build-orig-tarball.sh"
echo "DONE WITH build-orig-tarball"
tf="$( find . -maxdepth 1 -name 'spacecadetpinball*orig*z' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 )"
tar -zxf "${tf}"
cd "$( tar -zvtf "${tf}" | awk '/^d/{print $NF}' | head -n1 )"
cp -pr "${gitdir}/debian" .
debuild -us -uc
test $? -eq 0 || { "debuild failed. Cannot continue. Aborted." ; exit 1 ; }
cd "${devdir}"
dsc_file="$( find . -maxdepth 1 -name 'spacecadetpinball*dsc' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 | cut -d' ' -f2 )"
debian_tar_file="$( find . -maxdepth 1 -name 'spacecadetpinball*debian*z' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 | cut -d' ' -f2 )"

# prep obs
cd "${obsdir}"
osc up
osc rm *
cd "${devdir}"
cp -p "${dsc_file}" "${debian_tar_file}" "${tf}" "${obsdir}/"
cd "${obsdir}"
#find . -mindepth 1 -maxdepth 1 ! -name "${dsc_file}" ! -name "${debian_tar_file}" ! -name "${tf}" -delete
osc add *
osc commit # will prompt for commit name
