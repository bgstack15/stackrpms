# Readme for spacecadetpinball

## Overview
This is a dpkg for [SpaceCadetPinball](https://github.com/k4zmu2a/SpaceCadetPinball) which is not packaged for Devuan.

## Upstream
<https://github.com/k4zmu2a/SpaceCadetPinball>
<https://archive.org/download/SpaceCadet_Plus95/Space_Cadet.rar>

## Reason for being in stackrpms
This package is not in a dpkg in Devuan. I didn't research any pre-built dpkgs anywhere; it was easy enough to build.

## Alternatives

* Build from source.
* Other pinball?

## Dependencies
Basic stuff like SDL2 listed in [d/control](debian/control).

## References

* <https://aur.archlinux.org/packages/spacecadetpinball-git>

## Differences from upstream
While upstream supports "Full Tilt!" assets, I have not researched or added that here. I only wanted the Windows XP-era pinball game.
