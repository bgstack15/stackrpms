#!/bin/sh
# Startdate: 2024-10-31-5 15:12
# Usage:
#    FORCE_DOWNLOAD=1 to redownload git scm and .rar of graphics/music assets
# Purpose: generate a custom spacecadetpinball_2.1.0.orig.tar.gz and debian.tar.xz. The .dsc file will be in my scm stackrpms debian/ like usual.
# References:
#    https://aur.archlinux.org/packages/spacecadetpinball-git
#    https://github.com/k4zmu2a/SpaceCadetPinball
#    https://fabulous.systems/posts/2024/05/3d-pinball-for-windows-space-cadet-the-mission-continues/
# Dependencies:
#    dep-devuan: wget, curl

scm_url=https://github.com/k4zmu2a/SpaceCadetPinball
dev_dir=~/dev
stackrpms_dir=~/dev/stackrpms

show_latest_tarball_for_github_repo() {
   # call: show_latest_tarball_for_github_repo "https://github.com/gorhill/uBlock-for-firefox-legacy"
   # returns: "https://github.com/gorhill/uBlock-for-firefox-legacy/archive/firefox-legacy-1.16.4.26.tar.gz"
   # Improve: accept archive type, such as .tar.gz or .zip, to return
   # And yes, I know this parses html with awk. Get over it.
   # Written 2020-11-04 from ublock-origin-combined/build-orig-tarball.sh
   ___repo="${1}"
   _page="$( curl -s "${___repo}/tags" )"
   # tail grabs the highest number, so most recent available tarball from the page
   echo "${_page}" | grep -oE "href=[\"'][^\"']+archive[^\"']+tar\.gz\"" | sed -r -e 's/^href=.//;' -e 's/"$//;' | sort -n | uniq | tail -n1 | sed -r -e "s/^/https:\/\/github.com/;"
}

# asset 1: the source code, e.g., https://github.com/k4zmu2a/SpaceCadetPinball/archive/refs/tags/Release_2.1.0.tar.gz
source_url="$( show_latest_tarball_for_github_repo "${scm_url}" )"
source_tgz="$( basename "${source_url}" )"
version="${source_tgz%%.tar.gz}" ; version="${version##Release_}"
cd ~/dev
if test -d "spacecadetpinball-${version}" ;
then
   ( cd "spacecadetpinball-${version}" ; git checkout master ; git pull ; )
else
   git clone "${scm_url}" "spacecadetpinball-${version}"
fi

# asset 2: the rar file, or alternatively my build of it
# use https://archive.org/download/SpaceCadet_Plus95/Space_Cadet.rar
# Alternative: use my own assets, such as the Windows XP iso file
if test ! -f "Space_Cadet.rar" || test -n "${FORCE_DOWNLOAD}" ; then wget "https://archive.org/download/SpaceCadet_Plus95/Space_Cadet.rar" ; fi
(
   mkdir -p "spacecadetpinball-${version}/space_cadet"
   cd "spacecadetpinball-${version}/space_cadet"
   ls -altr ../../Space_Cadet.rar ;
   unrar -y x ../../Space_Cadet.rar ;
)
rm -f "spacecadetpinball_${version}.orig.tar.gz" ; tar -z -c --exclude ".git" --exclude "debian" -f "spacecadetpinball_${version}.orig.tar.gz" "spacecadetpinball-${version}"

if test -n "${BUILD}" ;
then
   cp -pr "${stackrpms_dir}/spacecadetpinball/debian/"* "${dev_dir}/spacecadetpinball-${version}/debian/"
   cd "spacecadetpinball-${version}"
   debuild -us -uc
fi

if test -z "${NO_CLEAN}" ;
then
   cd "${dev_dir}" ; rm -rf "spacecadetpinball-${version}/"
fi
