# Readme for stackrpms

## upstream
The [copr](https://copr.fedorainfracloud.org/coprs/bgstack15/stackrpms/) stores all the repo files for the different rpm-based distros.

## Reason for being in stackrpms
This `stackrpms` package is just the package that deploys the yum repo file.

## Alternatives
There are plenty of cool yum repos for Fedora out there. A few that I look at include:
* [chinforpms](https://github.com/PhantomX/chinforpms)
* [Fedora multimedia repo at Negativo](https://negativo17.org/repositories/#Multimedia_includes_Nvidia_driver_CUDA)

## Reverse dependency matrix
Distro     | stackrpms version
---------- | ----------------
All rpm    | N/A

## Differences from upstream
This is a custom rpm. Copr official way to deploy a repo is to just download the .repo file, but I wanted a nice rpm to handle it.
