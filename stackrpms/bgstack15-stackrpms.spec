Name:           bgstack15-stackrpms
Version:        1.0.0
Release:        1
Summary:        bgstack15-stackrpms repository configuration

Group:          System Environment/Base
License:        CC-BY-SA 4.0

URL:            https://copr.fedorainfracloud.org/coprs/bgstack15/stackrpms/
Source0:        https://copr-be.cloud.fedoraproject.org/results/bgstack15/stackrpms/pubkey.gpg
Source1:        https://copr.fedorainfracloud.org/coprs/bgstack15/stackrpms/repo/epel-7/bgstack15-stackrpms-epel-7.repo

BuildArch:     noarch

%description
This package contains the bgstack15-stackrpms repository
GPG key as well as configuration for yum/dnf.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} .
mv $( basename %{SOURCE1} ) bgstack15-stackrpms.repo
sed -i -r -e '/^baseurl/s/epel-[0-9]+/epel-\$releasever/;' %{SOURCE1}

%build


%install
rm -rf $RPM_BUILD_ROOT

#GPG Key
install -Dpm 644 %{SOURCE0} \
    $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-bgstack15-stackrpms

# yum
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 bgstack15-stackrpms.repo $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%post
{
if grep -qi fedora /etc/*release ;
then
   # configure for fedora
   sed -i -r -e '/^baseurl/s:epel-:fedora-:;' /etc/yum.repos.d/bgstack15-stackrpms.repo
else
   # configure for el
   static_releasever="$( grep -i version /etc/*release | head -n1 | tr -dc '[0-9\.]' | awk -F '.' '{print $1}' )"
   test -z "${static_releasever}" && static_releasever=7
   sed -i -r -e "/^baseurl/s:.releasever:${static_releasever}:;" /etc/yum.repos.d/bgstack15-stackrpms.repo
fi

} 1>/dev/null 2>&1
true

%files
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Wed Jan 23 2019 B Stack <bgstack15@gmail.com> - 1.0.0-1
- Fork to adapt for stackrpms

* Mon Oct 02 2017 Kevin Fenzi <kevin@scrye.com> - 7-11
- Add Conflicts on fedora-release to prevent people from installing on Fedora systems. Fixes bug #1497702
