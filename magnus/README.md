# Readme for magnus
This is a dpkg for Magnus, a screen magnifier.

## Upstream

* <https://bgstack15.ddns.net/cgit/magnus>
* <https://github.com/jjustra/magnus/tree/set-fixed-position-from-command-line>
* Original: <https://github.com/stuartlangridge/magnus>

## Reason for being in stackrpms
While Debian/Devuan has [this package](https://packages.debian.org/sid/magnus), I want some extra patches.

## Alternatives
Any of the links in this readme.

## Dependencies
See file [debian/control](debian/control).

## Differences from upstream
I want some patches like zoom levels 1-16, draggable position instead of following the mouse, and a reticule for showing RGB of the current pixel. The package recipe only differs from Debian to facilitate using my source.
