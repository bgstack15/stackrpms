Format: 3.0 (quilt)
Source: magnus
Binary: magnus
Architecture: all
Version: 1:1.0.3-400+stackrpms
Maintainer: Debian+Ubuntu MATE Packaging Team <debian-mate@lists.debian.org>
Uploaders: Martin Wimpress <martin.wimpress@ubuntu.com>, Mike Gabriel <sunweaver@debian.org>,
Homepage: https://github.com/stuartlangridge/magnus
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-mate-team/magnus
Vcs-Git: https://salsa.debian.org/debian-mate-team/magnus.git
Build-Depends: debhelper-compat (= 13), dh-python, python3, python3-distutils-extra, python3-setuptools
Package-List:
 magnus deb utils optional arch=all
Files:
 00000000000000000000000000000000 1 magnus.orig.tar.gz
 00000000000000000000000000000000 1 magnus.debian.tar.xz
