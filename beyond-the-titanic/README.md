# Beyond the Titanic upstream
https://gitlab.com/bgstack15/beyond-the-titanic forked from https://jxself.org/git/?p=beyond-the-titanic.git;a=summary

# Alternatives
https://github.com/samuelbrian/beyond-the-titanic

# Reason for being in stackrpms
This is an original package of the GPLv3 release of the Apogee game Beyond the Titanic.

# Reverse dependency matrix
Distro     | Beyond the Titanic version
---------- | ---------------
All        | 0.0.1

# Differences from upstream
None
