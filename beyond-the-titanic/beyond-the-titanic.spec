%global     use_master 1
%if 0%{use_master}
%global vername master
%else
%global vername %{version}
%endif

# turn off debug package
%define debug_package %{nil}

Name:       beyond-the-titanic
Version:    0.0.1
Release:    2%{?dist}
Summary:    Text adventure game from 1988

Group:      Games/AdventureGame
License:    AGPL
URL:        https://gitlab.com/bgstack15/%{name}
Source0:    %{URL}/-/archive/%{vername}/%{name}-%{vername}.tar.gz

Packager:   B Stack <bgstack15@gmail.com>
BuildRequires: fpc
BuildRequires: ImageMagick
# do not require the icon theme. the rpm can make the dirs, and it won't hurt on terminal-only systems to have the directories with just these icons.
#Requires:      hicolor-icon-theme

%description
Beyond the Titanic is a 1988 text adventure game published by Apogee.
In 2009 the source code was released under the GPL. This fork is
derived from the jxself release.

%prep
#%%setup -n %%{name}-%%{vername}
tar -zxf %{SOURCE0}

%build
cd %{name}-*
%make_build

%install
cd %{name}-*
%make_install

# desktop files
# handled by upstream makefile

# application start script
# handled by upstream makefile

# man pages
# none

# mimetypes
# none

# icons
for res in 16 22 24 32 48 64 96 128 256 ;do
  dir=%{buildroot}%{_datadir}/icons/hicolor/${res}x${res}/apps
  mkdir -p ${dir}
  for icon in %{name} ; do
    convert ship.svg -filter Lanczos -resize ${res}x${res}  \
      ${dir}/${icon}.png
  done
done
scalable_dir="%{buildroot}%{_datadir}/icons/hicolor/scalable/apps"
install -d "${scalable_dir}"
install -m0644 ship.svg "${scalable_dir}"

%clean
%{__rm} -rf %{buildroot} || :

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database &> /dev/null || :

%preun
# is it a final removal?
#if test "$1" = "0" ;
#then
#fi

%postun
update-desktop-database &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
#%license %attr(444, -, -) License.txt
#%doc %attr(444, -, -) Changelog.txt
%license %attr(444, -, -) %{_licensedir}/*
%doc %attr(444, -, -) %{_docdir}/*/*
%{_bindir}/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/%{name}

%changelog
* Fri Apr  5 2019 B Stack <bgstack15@gmail.com> - 0.0.1-2
- add build dependencies
- fix docdir for el

* Sun Mar 31 2019 B Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial rpm built
