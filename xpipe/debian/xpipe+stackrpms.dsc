Format: 3.0 (quilt)
Source: xpipe
Binary: xpipe
Architecture: any
Version: 0.0.2-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/jschauma/xpipe
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), libbsd-dev, openssl
Package-List:
 xpipe deb utils optional arch=any
Files:
 00000000000000000000000000000000 1 xpipe.orig.tar.gz
 00000000000000000000000000000000 1 xpipe.debian.tar.xz
