# Readme for xpipe
This package exists because pretty much nobody packages xpipe(1), which splits standard input based on number of bytes, or pattern, and passes it to separate invocations of the given command.

## xpipe upstream
[https://github.com/jschauma/xpipe](https:////github.com/jschauma/xpipe)

## Alternatives
None that I know of. You have to just write awk or sed scripts proficiently.

## Reason for being in stackrpms
I need a tool like this, and I have had to replicate its functionality manually in the past.

## Dependencies
Distro    | xpipe
--------- | -----
All dpkg  | 0.0.1

## Additional info

## References
None

## Differences from upstream
N/A
