#!/bin/sh
# Date: 2020-04-06
# Goal: convert the fresh dsc file to a generic one for obs that omits version and checksum info on filenames
# Use in debian/rules:
#    APPNAME=name-of-binary-package
#    override_dh_auto_build:
#       dh_auto_build
#       sh debian/make-dsc-for-obs.sh
tf="../$( find .. -maxdepth 1 -name "${APPNAME}_*dsc" -printf '%T@ %f\n' | sort | tail -n1 | awk '{print $NF}' )"
of="debian/$( basename "$( readlink -f "${tf}" )" | sed -r -e 's/_[0-9_\.]+[0-9_](-[0-9])?//;' )"
awk 'BEGIN{a=0} a > 0 {$2="1";gsub(/_[0-9_\.]+[0-9_](-[0-9])?/,"");} /^Files/{a=1} {print}' "${tf}" | sed -r -e '/Checksums-.{0,8}:\s*$/,/^Files/{/Files/!{d};}' -e '/^Files/,${s/^ ?[^\s]{32}/ 00000000000000000000000000000000/;};' > "${of}"
