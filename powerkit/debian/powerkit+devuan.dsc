Format: 3.0 (quilt)
Source: powerkit
Binary: powerkit, libpowerkit1, libpowerkit-dev
Architecture: any
Version: 1.0.0-2+devuan
Maintainer: Ben Stack <bgstack15@gmail.com>
Homepage: https://github.com/rodlie/powerkit
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), libqt5core5a | libqt5core5, libqt5dbus5, libqt5gui5 | libqt5gui5-gles, libx11-dev, libxrandr-dev, libxss-dev, qt5-qmake, qtbase5-dev
Package-List:
 libpowerkit-dev deb libdevel optional arch=any
 libpowerkit1 deb libs optional arch=any
 powerkit deb admin optional arch=any
Files:
 00000000000000000000000000000000 1 powerkit.orig.tar.xz
 00000000000000000000000000000000 1 powerkit+devuan.debian.tar.xz
