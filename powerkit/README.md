# Readme for powerkit

## Upstream
[https://gitlab.com/rodlie/powerkit](https://gitlab.com/rodlie/powerkit)

## Reason for being in stackrpms
My distros do not package powerkit, and it sounds like a useful utility. [Arch](https://aur.archlinux.org/packages/powerkit/) and [slackware](https://slackbuilds.org/repository/14.2/system/powerkit/) bundle it, but not any Debian-based distros.

## Alternatives
Disparate tools for each individual configuration.

## Reverse dependency matrix
Distro     | powerkit version
---------- | ----------------
Devuan     | 0.0.1

## Additional info

## Differences from upstream
None
