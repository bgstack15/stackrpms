%global dummy_package   0
%global appname newmoon
%global prettyname Newmoon web browser
%global stackrpms_custom 1
# derive from inside the source tree or from https://repo.palemoon.org/MoonchildProductions/Pale-Moon/releases
# git submodule | awk -v "name=platform" '$2 == name {gsub("-","",$1); print $1}'
%global submodule_platform_tag RB_20230219
%global badname palemoon
%global git_commit db5ee3c1968212742b4ed8b9883069ea7b03f0e1
%global tarballdir pale-moon

# additional repos to get python27 and devtoolset-7
# for el6 and el7: Software Collection;, for x86_64 only
# http://mirror.centos.org/centos/6/sclo/x86_64/rh/
# http://mirror.centos.org/centos/6/sclo/x86_64/sclo/
# for el6.i386:
# https://copr-be.cloud.fedoraproject.org/results/ewdurbin/pythons-el6/epel-6-$basearch/
# https://copr-be.cloud.fedoraproject.org/results/mlampe/devtoolset-7/epel-6-$basearch/
%define scl_env %{nil}
%define scl_buildreq coreutils
%if 0%{?el6}
   %define scl_buildreq python27, devtoolset-7-toolchain
   %ifarch x86_64
      %define scl_env python27 devtoolset-7
   %endif
   %ifarch i386 || i686 || i586
      %define scl_env devtoolset-7
   %endif
%endif
%if 0%{?el7}
   %define scl_env devtoolset-7
   %define scl_buildreq devtoolset-7-toolchain
%endif

# suppress binary stripping on el6 which fails for some reason
%if 0%{?el6}
   %global __os_install_post %{nil}
%endif

%if 0%{stackrpms_custom}
Name:		newmoon
%global prettyname Pale Moon
%else
Name:    newmoon
%endif
Summary:	Newmoon web browser
Version:	32.0.1
Release:	1

Group:	Networking/Web
License:	MPLv2.0
URL:		http://linux.palemoon.org/
Source0: https://repo.palemoon.org/MoonChildProductions/Pale-Moon/archive/%{version}_Release.tar.gz
#Source0: https://gitlab.com/opensource-tracking/palemoon/-/archive/%%{git_commit}/palemoon-%%{git_commit}.tar.gz
Source1:	https://repo.palemoon.org/MoonchildProductions/UXP/archive/%{submodule_platform_tag}.tar.gz
Source2: newmoon-mozconfig
Source3: newmoon.desktop
Source4: newmoon-mimeinfo.xml
%if 0%{stackrpms_custom}
Source5: bgstack15-newmoon-prefs.js
Source6: bluemoon-icons.tgz
Patch0: small-icons.patch
Patch1: pm-fedora.patch
Patch2: pm-to-nm.patch
%endif
Packager:	B. Stack <bgstack15@gmail.com>

%if "%{?scl_env}" != ""
BuildRequires: %{scl_buildreq}
%endif
BuildRequires:	alsa-lib-devel
BuildRequires:	autoconf213
BuildRequires:	bzip2-devel
BuildRequires:	dbus-glib-devel
BuildRequires:	desktop-file-utils
BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	GConf2-devel
BuildRequires:	glib2-devel
BuildRequires:	gtk2-devel
BuildRequires:	libXt-devel
BuildRequires:	mesa-libGL-devel
BuildRequires:	notification-daemon
BuildRequires:	%{_libdir}/libssl.so
%if 0%{?fedora} >= 38
# somehow fedora 38 does not include /usr/bin/perl by default in build environment
BuildRequires:	perl-interpreter
%endif
BuildRequires:	pkgconfig
BuildRequires:	pkgconfig(gtk+-2.0)
BuildRequires:	pulseaudio-libs-devel
BuildRequires:	sqlite-devel
BuildRequires:	unzip
BuildRequires:	yasm
BuildRequires:	zip
BuildRequires:	zlib-devel
%if 0%{?fedora} >= 30 || 0%{?rhel} >= 8
BuildRequires:	python2-devel
%endif
BuildRoot:     %{_tmppath}/%{name}-%{version}
Provides:      mimehandler(application/x-xpinstall)
Provides:      newmoon
Provides:      webclient

%description
Newmoon offers you a browsing experience in a browser completely built
from its own, independently developed source that has been forked off from
Firefox/Mozilla code, with carefully selected features and optimizations to
improve the browser's speed*, resource use, stability and user experience,
while offering full customization and a growing collection of extensions
and themes to make the browser truly your own.

# To avoid the debug .build-id files that conflict with %%{badname}-stackrpms:
%global _build_id_links none
%global debug_package %{nil}
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

%prep
%setup -q -n %{tarballdir}
tar -C platform --strip-components=1 -zxf %{SOURCE1} # git submodule
%{__cp} %{SOURCE2} ./.mozconfig
%if 0%{?fedora} >= 30
sed -i -r -e '/enable-optimize/s/"\s*$/ -Wno-format-overflow"/' ./.mozconfig
%endif

%if 0%{stackrpms_custom}
%patch0 -p1
%patch1 -p1
%patch2 -p1

! test -d %{badname}/branding/unofficial.unbuilt && {
   cp -pr %{badname}/branding/unofficial %{badname}/branding/unofficial.unbuilt && \
   tar -C %{badname}/branding -zxf %{SOURCE6}
} || :
%endif

%build
%if "%{?scl_env}" != ""
   thispwd=$( pwd )
   scl enable %{scl_env} /bin/bash << EOF
   pushd $thispwd
%endif
%if !%{dummy_package}
   ./mach build
%endif
%if "%{?scl_env}" != ""
   popd || :
EOF
%endif

%install
rm -rf %{buildroot}

%if "%{?scl_env}" != ""
   thispwd=$( pwd )
   scl enable %{scl_env} /bin/bash << EOF
   pushd $thispwd
%endif
%if !%{dummy_package}
   make -f client.mk \
      DESTDIR=%{buildroot} prefix=/usr \
      idldir=%{_datadir}/idl/%{appname} \
      includedir=%{_includedir}/%{appname} \
      installdir=%{_libdir}/%{appname} \
      sdkdir=%{_libdir}/%{appname}-devel \
      install
   rm -rf %{buildroot}%{_datadir}/idl \
      %{buildroot}%{_libdir}/%{appname}-devel \
      %{buildroot}%{_includedir} || :
   rm -rf %{buildroot}%{_datarootdir}/idl
   rm -rf %{buildroot}%{_libdir}/%{appname}-devel
   rm -rf %{buildroot}%{_includedir}
   # remove vestigial duplicate file
   rm -rf %{buildroot}%{_libdir}/%{appname}/%{appname}-bin || :
   mv %{buildroot}%{_libdir}/%{appname}/%{badname} %{buildroot}%{_libdir}/%{appname}/%{appname} || :
   rm -rf %{buildroot}%{_libdir}/%{appname}/%{appname}-bin || :

   mv %{buildroot}%{_bindir}/%{badname} %{buildroot}%{_bindir}/%{appname} || :
   rm -rf %{buildroot}%{_bindir}/%{appname}
   ln -sf %{_libdir}/%{appname}/%{appname} %{buildroot}%{_bindir}/%{appname}
%endif
%if "%{?scl_env}" != ""
   popd || :
EOF
%endif

# cleanup
%{__rm} -rf  %{buildroot}%{_datadir}/idl/%{appname} \
   %{buildroot}%{_includedir}/%{appname} \
   %{buildroot}%{_libdir}/%{appname}-devel

# desktop files
%{__mkdir_p} %{buildroot}%{_datadir}/applications
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE3}

%{__sed} -i -r -e "s/@NAME@/%{prettyname}/;" \
   -e "s/@EXEC@/%{appname}/;" \
   -e "s/@ICON@/%{appname}/;" \
   %{buildroot}%{_datadir}/applications/%{appname}.desktop

# application start script
# none

# man pages
# none

# mimetypes
%{__install} -Dm0644 %{SOURCE4} \
   %{buildroot}%{_datadir}/mime/packages/%{name}.xml

# icons
%{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/symbolic/apps
for s in 16 22 24 32 48 256 ;
do
   %{__mkdir_p} %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps
   %{__cp} -p %{badname}/branding/official/default${s}.png \
   %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/%{appname}.png
done

# Register as an application to be visible in the software center
# EXPERIMENTAL
mkdir -p %{buildroot}%{_datadir}/appdata
cat > %{buildroot}%{_datadir}/appdata/%{appname}.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!-- Adapted from content by Richard Hughes <richard-at-hughsie-dot-com> -->
<application>
  <id type="desktop">%{appname}.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <description>
    <p>
       Pale Moon offers you a browsing experience in a browser completely built from its own, independently developed source that has been forked off from Firefox/Mozilla code a number of years ago, with carefully selected features and optimizations to improve the browser stability and user experience, while offering full customization and a growing collection of extensions and themes to make the browser truly your own.
    </p>
  </description>
  <url type="homepage">http://www.palemoon.org/</url>
  <screenshots>
    <screenshot type="default">http://www.palemoon.org/images/screenshots/Win10-start-portal.png</screenshot>
  </screenshots>
  <!-- FIXME: change this to an upstream email address for spec updates
  <updatecontact>someone_who_cares@upstream_project.org</updatecontact>
   -->
</application>
EOF

# Add distribution.ini
# skipped

# filelist lifted straight from https://build.opensuse.org/package/view_file/network/palemoon/palemoon.spec?expand=1
# get a list of files
# $2 - dirlist
# $3 - dir of fileslist
# $4 - excludelist
filelist() {
  filelist=$PWD/$1
  pushd %{buildroot}
    find ./ | while read list; do
      if [ -n "$4" ]; then
        ! echo $list | grep "$4" || continue
      fi
      _file=$(echo $list | sed 's/[^/]//')
      _dir=$(dirname $_file)
      if echo $_dir | grep "$2"; then
        grep -xqs "\%dir\ $_dir" $filelist || echo "%dir $_dir" >> $filelist
      fi
      if echo $_file | grep "$3"; then
        [ -d "$list" ] || echo $_file >> $filelist
      fi
    done
  popd
}

%if 0%{stackrpms_custom}
%{__install} -d %{buildroot}%{_libdir}/%{name}/browser/defaults/preferences/
%{__install} -Dm0644 %{SOURCE5} \
   %{buildroot}%{_libdir}/%{name}/browser/defaults/preferences/
%endif

filelist file.lst %{appname} usr langpack

%clean
%{__rm} -rf %{buildroot} || :

%post
touch --no-create %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :

%preun
# is it a final removal?
#if test "$1" = "0" ;
#then
#fi

%postun
if test "$1" = "0" ;
then
   touch --no-create %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :
fi

%posttrans
update-desktop-database 1>/dev/null 2>&1 & :
gtk-update-icon-cache %{_datadir}/icons/hicolor 1>/dev/null 2>&1 & :
update-mime-database -n ${_datadir}/mime 1>/dev/null 2>&1 & :

%files -f file.lst
%defattr(-,root,root)
%doc AUTHORS LICENSE

%changelog
* Thu Feb 16 2023 B. Stack <bgstack15@gmail.com> - 32.0.0-1
- update version

* Tue Nov 22 2022 B. Stack <bgstack15@gmail.com> - 31.4.0-1
- update version

* Tue Nov 11 2022 B. Stack <bgstack15@gmail.com> - 31.3.1-1
- update version

* Wed Aug 03 2022 B. Stack <bgstack15@gmail.com> - 31.2.0-1
- update version

* Mon Jul 11 2022 B. Stack <bgstack15@gmail.com> - 31.1.1-1
- update version

* Sun May 22 2022 B. Stack <bgstack15@gmail.com> - 30.0.0-1
- update version
- remove revert-mach-file patch

* Thu Jan 20 2022 B. Stack <bgstack15@gmail.com> - 29.4.4-1
- update version

* Tue Jan 04 2022 B. Stack <bgstack15@gmail.com> - 29.4.3-1
- update version
- add revert-mach-file patch from stevepusser
- add nm-gcc-11 patch from gentooman

* Thu Nov 11 2021 B. Stack <bgstack15@gmail.com> - 29.4.2-1
- update version

* Wed Sep 15 2021 B. Stack <bgstack15@gmail.com> - 29.4.1-1
- Use new intermediate git source because upstream
  has stopped sharing git.

* Tue Aug 17 2021 B. Stack <bgstack15@gmail.com> - 29.4.0-1
- update version

* Mon Jul 19 2021 B. Stack <bgstack15@gmail.com> - 29.3.0-1
- update version

* Wed Jun 09 2021 B. Stack <bgstack15@gmail.com> - 29.2.1-1
- update version

* Tue Apr 27 2021 B. Stack <bgstack15@gmail.com> - 29.2.0-1
- update version

* Thu Apr 01 2021 B. Stack <bgstack15@gmail.com> - 29.1.1-1
- update version

* Tue Mar 16 2021 B. Stack <bgstack15@gmail.com> - 29.1.0-1
- Fork rpm to newmoon
- remove "stackrpms" suffix

* Tue Mar 02 2021 B. Stack <bgstack15@gmail.com> - 29.1.0-1
- update version

* Mon Feb 15 2021 B. Stack <bgstack15@gmail.com> - 29.0.1-1
- update version

* Tue Feb 02 2021 B. Stack <bgstack15@gmail.com> - 29.0.0-1
- update version

* Fri Dec 18 2020 B. Stack <bgstack15@gmail.com> - 28.17.0-1
- update version

* Wed Nov 25 2020 B. Stack <bgstack15@gmail.com> - 28.16.0-1
- update version

* Tue Oct 27 2020 B. Stack <bgstack15@gmail.com> - 28.15.0-1
- update version
- change sources to upstream new location, repos.palemoon.org

* Sat Oct 03 2020 B. Stack <bgstack15@gmail.com> - 28.14.2-1
- update version

* Fri Sep 25 2020 B. Stack <bgstack15@gmail.com> - 28.13.0-2
- use valid RELBASE. Upstream probably unpublished RELBASE_20200831

* Fri Sep 04 2020 B. Stack <bgstack15@gmail.com> - 28.13.0-1
- update version

* Wed Aug 05 2020 B. Stack <bgstack15@gmail.com> - 28.12.0-1
- update version

* Tue Jul 14 2020 B. Stack <bgstack15@gmail.com> - 28.11.0-1
- update version

* Fri Jun 05 2020 B. Stack <bgstack15@gmail.com> - 28.10.0-1
- update version

* Tue May 26 2020 B. Stack <bgstack15@gmail.com> - 28.9.3-2
- add the aboutdialog customization

* Fri May 08 2020 B. Stack <bgstack15@gmail.com> - 28.9.3-1
- update version

* Tue May 05 2020 B. Stack <bgstack15@gmail.com> - 28.9.2-2
- Add customization to about dialog

* Thu Apr 30 2020 B. Stack <bgstack15@gmail.com> - 28.9.2-1
- update version

* Fri Apr 10 2020 B. Stack <bgstack15@gmail.com> - 28.9.1-1
- update version

* Thu Mar 26 2020 B. Stack <bgstack15@gmail.com> - 28.9.0.2-1
- update version
- add git submodule support

* Mon Mar 02 2020 B. Stack <bgstack15@gmail.com> - 28.8.4-1
- update version

* Tue Feb 18 2020 B. Stack <bgstack15@gmail.com> - 28.8.3-1
- update version

* Wed Feb 05 2020 B. Stack <bgstack15@gmail.com> - 28.8.2.1-1
- update version
- drop the vanilla release

* Tue Jan 28 2020 B. Stack <bgstack15@gmail.com> - 28.8.2-1
- update version

* Mon Jan 13 2020 B. Stack <bgstack15@gmail.com> - 28.8.1-1
- update version
- place binary in /usr/bin instead of /usr/local/bin

* Wed Dec 11 2019 B. Stack <bgstack15@gmail.com> - 28.8.0-1
- update version
- fc30 now also explicitly requires python2-devel. Fc31 already did.

* Wed Oct 30 2019 B. Stack <bgstack15@gmail.com> - 28.7.2-1
- update version
- remove custom patch for mz1533969 because it was upstreamed
- buildrequires use ssl lib name instead of package name to use either package

* Thu Sep 12 2019 B. Stack <bgstack15@gmail.com> - 28.7.1-1
- update version

* Wed Sep 04 2019 B. Stack <bgstack15@gmail.com> - 28.7.0-1
- update version

* Thu Jul 25 2019 B. Stack <bgstack15@gmail.com> - 28.6.1-1
- update version

* Mon Jul  8 2019 B. Stack <bgstack15@gmail.com> - 28.6.0.1-2
- remove custom patches for stock build

* Fri Jul  5 2019 B. Stack <bgstack15@gmail.com> - 28.6.0.1-1
- update version

* Tue Jul  2 2019 B. Stack <bgstack15@gmail.com> - 28.6.0-1
- update version
- improve scriptlets for streamlining

* Tue Jun  4 2019 B. Stack <bgstack15@gmail.com> - 28.5.2-1
- update version

* Tue Apr 30 2019 B. Stack <bgstack15@gmail.com> - 28.5.0-1
- update version

* Tue Feb 19 2019 B. Stack <bgstack15@gmail.com> 28.4.0-1
- update version

* Wed Jan 23 2019 B. Stack <bgstack15@gmail.com> 28.3.1-1
- update version

* Tue Jan 15 2019 B. Stack <bgstack15@gmail.com> 28.3.0-1
- update version

* Mon Dec 10 2018 B. Stack <bgstack15@gmail.com> 28.2.2-1
- update version

* Thu Sep 20 2018 B. Stack <bgstack15@gmail.com> 28.1.0-1
- update version

* Thu Sep 13 2018 B. Stack <bgstack15@gmail.com> 28.0.1-1
- update version

* Wed Sep 05 2018 B. Stack <bgstack15@gmail.com> 28.0.0-1
- rpm spec updated
