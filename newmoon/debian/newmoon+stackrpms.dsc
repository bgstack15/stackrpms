Format: 3.0 (quilt)
Source: newmoon
Binary: newmoon
Architecture: any
Version: 33.1.1-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: http://www.palemoon.org/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12), gcc (<< 11) | gcc-10, g++ (<< 11) | g++-10, autoconf2.13, libasound2-dev, libdbus-glib-1-dev (>= 0.60), libfontconfig1-dev, libgconf2-dev (>= 1.2.1), libgtk2.0-dev (>= 2.14), libpulse-dev, libx11-xcb-dev, libxt-dev, mesa-common-dev, pkg-config, python2 (>= 2.7.18-2~) | python (>= 2.7) | python2.7, unzip, yasm (>= 1.1), zip
Package-List:
 newmoon deb web optional arch=any
Files:
 00000000000000000000000000000000 1 newmoon.orig.tar.gz
 00000000000000000000000000000000 1 newmoon+stackrpms.debian.tar.xz
