#!/bin/sh
# Startdate: 2024-06-20-5 21:11
# Purpose: oneliner to pull down stevenpusser's latest debian.tar.xz
# Dependencies:
#    obs-dl, my custom script
# REAL https://build.opensuse.org/source/home:stevenpusser/palemoon/palemoon_33.1.1-1.gtk2.debian.tar.xz
#      https://build.opensuse.org/package/show/home:stevenpusser/palemoon/palemoon_33_1_1-1_gtk2_debian.tar.xz
inpage="https://build.opensuse.org/package/show/home:stevenpusser/palemoon"
prefix="https://build.opensuse.org/source/home:stevenpusser/palemoon/"
workdir=/usr/src/newmoon/

# process
# Improve: check if file exists already. if already exists, skip the download
output="$( curl -L "${inpage}" )"
debian_tar_xz_file="$( echo "${output}" | awk -F"'" '/tr id=.file-.*debian_tar_xz/{gsub("file-","",$2);gsub("_",".",$2);gsub("palemoon.","palemoon_",$2);print $2}' )"
cd "${workdir}"
# sometimes OBS gets hung up and never answers, so just cancel out after so long.
timeout 10 obs-dl "${prefix%%/}/${debian_tar_xz_file}"
tar -Jxf "${debian_tar_xz_file}"
diff -aur /usr/src/newmoon/debian ~/dev/stackrpms/newmoon/debian > ~/foo-sp1
echo "# Now run:"
echo "vi ~/foo-sp1 ${workdir%%/}/debian/* ~/dev/stackrpms/newmoon/debian/*"
