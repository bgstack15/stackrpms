// file: /usr/lib/palemoon/browser/defaults/preferences/bgstack15-palemoon-prefs.js
// deployed with palemoon-stackrpms package (rpm or dpkg) built by bgstack15
// last modified 2020-04-15
// reference:
//    https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
pref("browser.allTabs.previews", false);
pref("browser.backspace_action", 0);
pref("browser.ctrlTab.previews", false);
pref("browser.ctrlTab.recentlyUsedOrder", false);
pref("browser.download.useDownloadDir", true);
pref("browser.newtab.choice", 1);
pref("browser.newtabpage.storageVersion", 1);
pref("browser.search.update", false);
pref("browser.sessionstore.restore_on_demand", false);
pref("browser.startup.page", 3);
pref("browser.tabs.closeWindowWithLastTab", false);
// These two have to stay undefined in Firefox 77+ in order for the drop-down for autocompletion to still work.
//pref("browser.urlbar.disableExtendForTests", true);
//pref("browser.urlbar.maxRichResults", 0);
pref("browser.urlbar.trimURLs", false);
pref("browser.urlbar.update1", false);
pref("browser.xul.error_pages.enabled", false);
pref("captivedetect.canonicalURL", "http://127.0.0.1:9980");
pref("dom.event.clipboardevents.enabled", false);
pref("extensions.enabledAddons", "%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:28.3.0");
pref("extensions.shownSelectionUI", true);
pref("extensions.update.autoUpdateDefault", false);
pref("general.warnOnAboutConfig", false);
pref("network.automatic-ntlm-auth.trusted-uris", ".ipa.smith122.com");
pref("network.cookie.prefsMigrated", true);
pref("network.http.spdy.enabled", false);
pref("network.negotiate-auth.trusted-uris", ".ipa.smith122.com");
pref("network.stricttransportsecurity.preloadlist", false);
pref("privacy.sanitize.migrateFx3Prefs", true);
pref("security.cert_pinning.enforcement_level", 0);
pref("services.sync.declinedEngines", "");
pref("startup.homepage_override_url", "");
pref("toolkit.telemetry.reportingpolicy.firstRun", false);
pref("xpinstall.whitelist.add", "");
// Control DNS over HTTPS (DoH) and Trusted Recursive Resolver (TRR).
// More about DoH: https://github.com/bambenek/block-doh
// https://blog.nightly.mozilla.org/2018/06/01/improving-dns-privacy-in-firefox/
// https://support.mozilla.org/en-US/kb/configuring-networks-disable-dns-over-https
// https://wiki.mozilla.org/Trusted_Recursive_Resolver
// 0: Off by default, 1: Firefox chooses faster, 2: TRR default w/DNS fallback,
// 3: TRR only mode, 4: Use DNS and shadow TRR for timings, 5: Disabled.
pref("network.trr.mode", 0);
pref("extensions.pocket.enabled", false);
pref("extensions.pocket.api", "http://localhost:9980");
pref("extensions.pocket.site", "http://localhost:9980");
