# README for tmdbv3api
Project [tmdbv3api](https://github.com/AnthonyBloomer/tmdbv3api) brings The Movie Database (TMDb) API to python3.

## Upstream
This project's upstream is at <https://github.com/AnthonyBloomer/tmdbv3api>. The stackrpms build recipe is at <https://bgstack15.ddns.net/cgit/stackrpms/tree/tmdbv3api>.

## Alternatives
Use a python venv and the [pip](https://pypi.org/project/tmdbv3api/) package.

Set up a venv:

    python3 -m venv ~/venv1 
    source ~/venv1/bin/activate
    pip3 install tmdbv3api

Use the venv:
    
    source ~/venv1/bin/activate
    python3

## Reason for existence
This library is not packaged by anybody else in dpkg format. I got tired of using a venv that only exists where I have prepared it ahead of time. I want a general solution.

## Using
Install it.

## Dependencies
See original package dependencies, but it was just `python3`. Obviously when using the library you will need a tmdb account.

## Building
I use the Open Build Service to build this [package](https://build.opensuse.org/package/show/home:bgstack15/tmdbv3api).

## References
