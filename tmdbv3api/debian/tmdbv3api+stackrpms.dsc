Format: 3.0 (quilt)
Source: tmdbv3api
Binary: tmdbv3api
Architecture: any
Version: 1.9.1-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/AnthonyBloomer/tmdbv3api
Standards-Version: 4.6.1
Build-Depends: debhelper-compat (= 13), dh-python, python3, python3-requests
Package-List:
 tmdbv3api deb python optional arch=any
Files:
 00000000000000000000000000000000 1 tmdbv3api.orig.tar.gz
 00000000000000000000000000000000 1 tmdbv3api.debian.tar.xz
