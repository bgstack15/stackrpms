# Readme for logout-manager

## Upstream
[https://gitlab.com/bgstack15/logout-manager](https://gitlab.com/bgstack15/logout-manager)

## Reason for being in stackrpms
Logout Manager is a homegrown tool and has no place in any official repositories.

## Alternatives
I did not research any alternatives. The project is based on general ideas such as included in Xfce, where an easy button for similar logout options exists as an option for the Xfce panel.

## Reverse dependency matrix
Distro     | logout-manager version
---------- | ----------------
Devuan     | 0.0.2

## Additional info
This project also exists as a proof-of-concept for myself, for various combinations of things in Python3, including [svg graphics in tkinter](https://bgstack15.ddns.net/blog/posts/2019/07/13/display-svg-in-tkinter-python3/), and Gtk3 applications and status icons, and menus with icons on the items.

## Differences from upstream
None
