Format: 3.0 (quilt)
Source: logout-manager
Binary: logout-manager
Architecture: all
Version: 0.0.4-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.wordpress.com/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), go-md2man
Package-List:
 logout-manager deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 logout-manager.orig.tar.gz
 00000000000000000000000000000000 1 logout-manager+stackrpms.debian.tar.xz
