Format: 3.0 (quilt)
Source: xcur2png
Binary: xcur2png
Architecture: any
Version: 0.7.1-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/eworm-de/xcur2png
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), dh-autoreconf, autotools-dev, automake, autoconf, libxcursor-dev, pkg-config, libpng-dev
Package-List:
 xcur2png deb x11 optional arch=any
Files:
 00000000000000000000000000000000 1 xcur2png.orig.tar.gz
 00000000000000000000000000000000 1 xcur2png+stackrpms.debian.tar.xz
Original-Maintainer: tks mashiw <tksmashiw@gmail.com>
