# xcur2png upstream
https://github.com/eworm-de/xcur2png

# Reason for being in stackrpms
Fedora, CentOS, and Devuan do not package xcur2png.

# Alternatives
xcur2png is a cli tool for convert X11 cursor files to png. The x11-apps file `xcursorgen` converts png files to xcursor format. A gimp plugin exists for manipulating X11 cursor files.

# Reverse dependency matrix
Distro         | xcur2png version
-------------- | ----------------
Devuan Ceres   | 0.7.1

# Additional info
None

# Differences from upstream
Instead of using the provided debian/, I rebuilt it entirely.
