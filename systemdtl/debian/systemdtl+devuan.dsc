Format: 3.0 (quilt)
Source: systemdtl
Binary: systemdtl
Architecture: all
Version: 0.0.1-2+devuan
Maintainer: Ben Stack <bgstack15@gmail.com>
Homepage: https://gitlab.com/bgstack15/systemdtl
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~)
Files:
 00000000000000000000000000000000 1 systemdtl.orig.tar.gz
 00000000000000000000000000000000 1 systemdtl.debian.tar.xz
