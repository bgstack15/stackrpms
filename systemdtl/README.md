# systemdtl upstream
https://gitlab.com/bgstack15/systemdtl

# Reason for being in stackrpms
This is the upstream package of the systemdtl script.

# DEPRECATED
As of 2021-06-11, systemdtl is now maintained in Devuan upstream directly in package [systemctl-service-shim](https://pkginfo.devuan.org/cgi-bin/policy-query.html?c=package&q=systemctl-service-shim&x=submit). And that package's source is maintained at [https://git.devuan.org/devuan/systemctl-service-shim](https://git.devuan.org/devuan/systemctl-service-shim).

# Reverse dependency matrix
Distro     | systemdtl version | bgscripts-core version
---------- | ----------------- | ----------------------
All        | 0.0.1             | 1.4.0

# Differences from upstream
N/A
