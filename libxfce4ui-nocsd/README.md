# Readme for libxfce4ui-nocsd
This package helps minimize the client-side decorations introduced in Xfce 4.16.

!! This package is no longer necessary! See [Xfce-Classic issue #13](https://github.com/Xfce-Classic/libxfce4ui-nocsd/issues/13#issuecomment-1248887700)

## libxfce4ui-nocsd upstream
Origin [https://github.com/Xfce-Classic/libxfce4ui-nocsd](https://github.com/Xfce-Classic/libxfce4ui-nocsd)
List of updates at Unit193's [launchpad page](https://launchpad.net/~xubuntu-dev/+archive/ubuntu/experimental/+packages)
Unit193 of Xubuntu fame is the main dpkg packager of libxfce4ui-nocsd.

## Alternatives
[Gtk3-nocsd](https://github.com/PCMan/gtk3-nocsd/) is built-in to Devuan [Ceres](https://pkginfo.devuan.org/cgi-bin/policy-query.html?c=package&q=gtk3-nocsd) and covers gtk3 in general. It can actually be used in tandem with this package.

### How to use
Install package `libxfce4ui-nocsd`. After a logout or perhaps reboot, the client-side decorations (CSD) should be gone for Xfce applications.

## Reason for being in stackrpms
`Gtk3-nocsd` is already packaged by my main distro, but libxfce4ui-nocsd is not. To benefit from it, I need to make it installable within my environment for myself.

Deprecated as of 2022-12!

## Reverse dependency matrix
Distro       | libxfce4ui-nocsd
------------ | --------
Devuan Ceres | 4.16.0-1

## Build process for dpkg
Download from the upstream releases page the .tar.bz2 file. I am uncertain what is different between the "libxfce4ui-4.16.0.tar.bz2" file and the "Source code (tar.gz)" file, but the bz2 one is the only one that compiles nicely with the debuild process.
Then copy this debian/ directory and run a `debuild -us -uc` like normal.

## References
Front page for Unit193's [vanir repo](https://launchpad.net/~xubuntu-dev/+archive/ubuntu/staging/+sourcepub/11957369/+listing-archive-extra)
Unit193's vanir repository [debian.tar.xz file](https://launchpad.net/~xubuntu-dev/+archive/ubuntu/staging/+sourcefiles/libxfce4ui-nocsd/4.16.0-1vanir1~20.10/libxfce4ui-nocsd_4.16.0-1vanir1~20.10.debian.tar.xz)
You can also install package `gtk3-nooverlayscrollbar` which disables the auto-hide feature of gtk3 scrollbars.

## Differences from upstream
Only superficial ones for package name and maintainer name
