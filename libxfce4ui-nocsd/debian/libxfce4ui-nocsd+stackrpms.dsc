Format: 3.0 (quilt)
Source: libxfce4ui-nocsd
Binary: libxfce4ui-nocsd-2-0, libxfce4ui-nocsd-2-dev, libxfce4ui-nocsd-common, gir1.2-libxfce4ui-nocsd-2.0, libxfce4ui-nocsd-glade, libxfce4ui-nocsd-utils
Architecture: any all
Version: 4.17.0-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/Xfce-Classic/libxfce4ui-nocsd
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 12), gobject-introspection, gtk-doc-tools, intltool, libgirepository1.0-dev, libgladeui-dev, libgtk-3-dev, libgtop2-dev, libstartup-notification0-dev, libxfce4util-dev (>= 4.15.6), libxfconf-0-dev, pkg-config, xfce4-dev-tools
Files:
 00000000000000000000000000000000 1 libxfce4ui-nocsd.orig.tar.bz2
 00000000000000000000000000000000 1 libxfce4ui-nocsd.debian.tar.xz
Original-Maintainer: Unit 193 <unit193@debian.org>
