# gcc49 upstream
https://copr.fedorainfracloud.org/coprs/davidva/gcc49/

# Reason for being in stackrpms
I was trying to compile Palemoon < 28.0.0 which depended on gcc 4.9, and CentOS 7 only had gcc 4.8. This should be irrelevant now that Palemoon can use gcc 5-9.

# Reverse dependency matrix
Distro         | Palemoon version | gcc version
-------------- | ---------------- | -----------
CentOS 7       | 27.9.4           | 4.9

# Differences from upstream
Unknown
