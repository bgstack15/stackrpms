# README for pasystray
This package is my fork of [Debian's package](https://packages.debian.org/sid/pasystray) to achieve some minor effects I want.

## Upstream
The origin of the appplication is on [github](https://github.com/christophgysin/pasystray). Debian's vcs is on [salsa](https://salsa.debian.org/debian/pasystray).

## Alternatives
Use native Devuan package, that has broken, and also unwanted, features.

For alsa based systems, use package `volumeicon-alsa`.

## Reason for being in stackrpms
I wanted small modifications to this application. In 2024 I started [using pulseaudio](https://bgstack15.ddns.net/blog/posts/2024/03/18/change-default-audio-input-on-devuan-with-pulseaudio/) and the alsa-based volumeicon did not control pulseaudio-only sinks, like the "combined sink."

## Dependencies
Read the dpkg recipe.

## References
All links in this document.

## Differences from upstream.
The application code itself is unchanged. My dpkg recipe varies from upstream dpkg slightly, and includes very small patches to the app code, to accomplish the following:

* Disable using libayatana-appindicator3-1 and libavahi garbage. The fallback of Gtk3 StatusIcon works better.
* Use pulsemixer instead of pavucontrol.
