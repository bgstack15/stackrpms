Format: 3.0 (quilt)
Source: pasystray
Binary: pasystray
Architecture: any
Version: 0.8.2-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/christophgysin/pasystray
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian/pasystray
Vcs-Git: https://salsa.debian.org/debian/pasystray.git
Build-Depends: debhelper-compat (= 13), libgtk-3-dev, libnotify-dev, libpulse-dev, pkgconf
Files:
 00000000000000000000000000000000 1 pasystray.orig.tar.gz
 00000000000000000000000000000000 1 pasystray.debian.tar.xz
