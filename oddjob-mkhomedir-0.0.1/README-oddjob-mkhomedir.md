# Introduction
This oddjob-mkhomedir-0.0.1 directory is a dummy, which is designed to be installed on a Devuan system so you can install freeipa-client.
This is the "source" contents that will be used to build a dpkg from the oddjob-mkhomedir/ directory in this same source tree.

To actually make home directories upon user first login:

    tf=/etc/pam.d/common-session ; ! grep -q 'mkhomedir' "${tf}" && { thisline="$(( $( grep -nE 'session\s+optional' "${tf}" | head -n1 | awk -F':' '{print $1}' ) - 0 ))" ; awk -v thisline="$thisline" 'NR == (thisline) {print "session optional        pam_mkhomedir.so"; } {print;}' "${tf}" > "${tf}.2" ; test -f "${tf}.2" && mv "${tf}.2" "${tf}" ; }
