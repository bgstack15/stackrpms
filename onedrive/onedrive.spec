%define          debug_package %{nil}

Summary:         A complete tool to interact with OneDrive on Linux. Built following the UNIX philosophy.
Name:            onedrive
Version:         2.3.3
Release:         1
Source:          https://github.com/abraunegg/%{name}/archive/v%{version}.tar.gz
License:         GPL-3
Group:           System Environment/Daemons
# on el7, get ldc from copr://harbottle/main
BuildRequires:   ldc
BuildRequires:   gcc
BuildRequires:   sqlite-devel
BuildRequires:   libcurl-devel
Requires:        libcurl
Requires:        sqlite
Requires(post):  systemd
Requires(preun): systemd

%description
A complete tool to interact with OneDrive on Linux. Built following the UNIX philosophy.

%prep
%setup
#%patch0 -p0

%build
make DC=ldc2 PREFIX=/usr/ DESTDIR=$RPM_BUILD_ROOT all

%install
mkdir -p %{buildroot}/usr/bin
install -D onedrive %{buildroot}/usr/bin/onedrive
install -D -m 644 onedrive.service %{buildroot}/usr/lib/systemd/user/onedrive.service

%files
%doc CHANGELOG.md README.md config
%license LICENSE
%defattr(-,root,root)
%attr(0555,bin,bin) /usr/bin/onedrive
%attr(0555,bin,bin) /usr/lib/systemd/user/onedrive.service

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Fri May 31 2019 B Stack <bgstack15@gmail.com> - 2.3.3-1
- move to abraunegg distrubtion of onedrive client from skillion

* Sat Feb 10 2018 Patrick Pichon <patrick@pichon.me>
add config file, and few addition prerequisites

* Fri Feb 9 2018 Patrick Pichon <patrick@pichon.me>
ready for Copr

* Thu Feb 8 2018 Patrick Pichon <patrick@pichon.me>
initial version
