# onedrive upstream
https://github.com/abraunegg/onedrive

# Reason for being in stackrpms
I was attempting to use my work OneDrive account from a Linux system. Additional authentication was required so my goal failed, so this project is considered deprecated. This is an original package of the application.

# Reverse dependency matrix
Distro     | onedrive version
---------- | ---------------
CentOS 7   | 2.3.3

# Differences from upstream
None
