# plymouth upstream
https://www.freedesktop.org/software/plymouth/releases/
https://salsa.debian.org/debian/plymouth/-/tags/debian%2F0.9.4-1.1

# Reason for being in stackrpms
Devuan ceres does not package plymouth, which I wanted to include in my Chicago95 dpkg collection.

# Reverse dependency matrix
Distro       | Chicago95 version | plymouth version
------------ | ----------------- | ---------------
Devuan ceres | 0.0.1             | any

# Differences from upstream
See file [stackrpms-plymouth-debian.diff](stackrpms-plymouth-debian.diff)
