Format: 3.0 (quilt)
Source: palemoon
Binary: palemoon
Architecture: any
Version: 29.4.0-1+devuan
Maintainer: B Stack <bgstack15@gmail.com>
Homepage: http://www.palemoon.org/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12), autoconf2.13, libasound2-dev, libdbus-glib-1-dev (>= 0.60), libgconf2-dev (>= 1.2.1), libgtk2.0-dev (>= 2.14), libssl-dev, libx11-xcb-dev, libxt-dev, lsb-release, mesa-common-dev, pkg-config, python2 (>= 2.7.18-2~) | python (>= 2.7), unzip, yasm (>= 1.1), zip
Package-List:
 palemoon deb web optional arch=any
Files:
 00000000000000000000000000000000 1 palemoon.orig.tar.gz
 00000000000000000000000000000000 1 palemoon+devuan.debian.tar.xz
Original-Maintainer: Steven Pusser <stevep@mxlinux.org>
