# Palemoon upstream
https://github.com/MoonchildProductions/UXP/archive/PM28.7.2_Release.tar.gz

# Reason for being in stackrpms
All my distros do not package Palemoon, so I package it myself. This at one point in 2018 was approved as a officially-endorsed third-party release of Palemoon but the package has since not been re-certified.

# Alternatives
Palemoon itself is a fork from Mozilla Firefox, which is still actively maintained and packaged by pretty much every distro. Waterfox is another fork of the original codebase, and is also packaged here in this repo.

# Reverse dependency matrix
Distro     | Palemoon version
---------- | ---------------
All        | 28.10.0

# Additional notes
Credit goes to so many sources and inspirations:
* trava90 of the Pale Moon project
* firefox-61.0.2-3.fc28.src.rpm
* https://build.opensuse.org/package/view_file/network/palemoon/palemoon.spec?expand=1
* instructions for compiling on centos 6 and 7 http://developer.palemoon.org/Developer_Guide:Build_Instructions/Pale_Moon/Linux#head:CentOS_6

Fold the debian changelog notes with:

    :'<,'>!sed -r -e 's/^ */* /;' | fold -w 72 -s | sed -r -e 's/^([^\*])/      \1/;' -e 's/^\*/    */;'

# Differences from upstream
None
