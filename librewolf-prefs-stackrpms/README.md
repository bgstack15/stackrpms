# Readme for librewolf-prefs-stackrpms

## Overview

This package owns the one prefs file for customizing LibreWolf.

## librewolf-prefs-stackrpms upstream
None

## Reason for being in stackrpms
This package owns the stackrpms prefs.js file for LibreWolf. This package's existence means that I do not need to maintain the [librewolf stackrpms](../librewolf) package ([OBS](https://build.opensuse.org/package/show/home:bgstack15/librewolf)) on top of the official AfterMozilla [LibreWolf](https://build.opensuse.org/package/show/home:bgstack15:aftermozilla/librewolf) package ([sources](https://gitlab.com/librewolf-community/browser/debian)) which I also maintain.

## Alternatives
I just use the [AfterMozilla LibreWolf](https://build.opensuse.org/package/show/home:bgstack15:aftermozilla/librewolf) package which I also build. That is the upstream for this package.

## Dependencies
Exact same as Mozilla Firefox.

## References
Check the contents of internal prefs package for the various web browsers' prefs.js-type files.

## Differences from upstream
N/A
