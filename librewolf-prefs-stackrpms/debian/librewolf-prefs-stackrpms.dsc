Format: 3.0 (quilt)
Source: librewolf-prefs-stackrpms
Binary: librewolf-prefs-stackrpms
Architecture: all
Version: 0.0.1-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.ddns.net/blog/
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 librewolf-prefs-stackrpms deb libs optional arch=all
Files:
 00000000000000000000000000000000 1 librewolf-prefs-stackrpms.orig.tar.gz
 00000000000000000000000000000000 1 librewolf-prefs-stackrpms.debian.tar.xz
