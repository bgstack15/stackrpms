%define debug_package %{nil}
Name:           librewolf-prefs-stackrpms
Version:        0.0.1
Release:        1%
Summary:        Librewolf prefs for stackrpms
License:        GPLv3
URL:            https://bgstack15.ddns.net/
Source0:        README.md
Source1:        stackrpms-librewolf-prefs.js
%if 0%{?fedora} || 0%{?rhel} >= 8
Recommends:       librewolf
%endif

%description
Provides the librewolf librewolf-prefs-stackrpms.js file

%install
%{__install} -m0755 -d %{buildroot}%{_libdir}/librewolf/browser/defaults/preferences
%{__install} %{SOURCE1} %{buildroot}%{_libdir}/librewolf/browser/defaults/preferences

%files
%{buildroot}%{_libdir}/librewolf/browser/defaults/preferences/*.js

%changelog
* Wed Oct 12 2022 B. Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial spec
