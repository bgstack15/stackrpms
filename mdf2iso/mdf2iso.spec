# References:
#    manpage location rpmrebuild -e vim-common
#    http://deb.debian.org/debian/pool/main/m/mdf2iso/mdf2iso_0.3.1-2.debian.tar.xz
%define upstreamname upstream
Name:		mdf2iso
Version:	0.3.1
Release:	0%{?dist}
Summary:	simple utility to convert mdf to iso / cue / toc images

Group:		Filesystem/Utils
License:	   GPLv2
URL:		https://salsa.debian.org/debian/mdf2iso/
Source0:	https://salsa.debian.org/debian/%{name}/-/archive/%{upstreamname}/0.3.1/%{name}%-%{version}.tar.gz
Source1: %{name}.1
Packager:   B Stack <bgstack15@gmail.com>

BuildRequires:	gcc
BuildRequires:	automake
#Requires:	

%description
mdf2iso is a very simple utility to convert an Alcohol 120% mdf image to an
iso, toc / dat or cue / bin image.

%prep
%setup -q -c %{name}%{?upstreamname:-%{upstreamname}}-%{version}
mv %{name}%{?upstreamname:-%{upstreamname}}-%{version}-*/* .

%build
%{__autoconf}
%{__aclocal}
%configure PREFIX=/usr
%{__make} %{?_smp_mflags}

%install
%{__mkdir_p} %{buildroot}%{_bindir} ; sleep 1
%make_install

# man pages
%{__install} -d %{buildroot}%{_mandir}/man1
%{__install} -Dm 0644 -t %{buildroot}%{_mandir}/man1 %{SOURCE1}

%files
%attr(0755, -, -) %{_bindir}/%{name}
%doc ChangeLog
%attr(0644, root, root) %doc %{_mandir}/*/*
%license gpl.txt

%changelog
* Sun Feb 24 2019 B Stack <bgstack15@gmail.com> 0.3.1-0
- initial rpm built
