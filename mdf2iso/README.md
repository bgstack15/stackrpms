# mdf2iso upstream
http://deb.debian.org/debian/pool/main/m/mdf2iso/mdf2iso_0.3.1-2.debian.tar.xz

# Reason for being in stackrpms
I needed to convert an mdf file, and Fedora does not package this tool.

# Reverse dependency matrix
Distro     | mdf2iso version
---------- | ---------------
All        | 0.3.1

# Differences from upstream
None
