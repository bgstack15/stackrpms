# README for MediaElch
This is a dpkg for [MediaElch](https://www.kvibes.de/mediaelch/). This is for editing .nfo files for media files.

## Upstream
The upstream app source is at [github](https://github.com/Komet/MediaElch/). Upstream even provides a few [dpkgs for a few distros](https://mediaelch.github.io/mediaelch-doc/download/linux.html#linux-repositories), but I want to package my own because I can.

## Alternatives
TinyMediaManager is only freemium and also some java monstrosity I wasn't able to compile even the libre bits.

I have not tried the binary releases of MediaElch.

## Reason for existence
I want to compile it myself.

## Using
My builds should be on my [obs space](https://build.opensuse.org/project/show/home:bgstack15).

## Dependencies
See file [debian/control](debian/control) for that.

## Building
Standard debuild. Be sure to get the git submodules.


    git submodule update --init


See upstream's [build docs](https://mediaelch.github.io/mediaelch-doc/contributing/build/index.html).

## References

All links seen above.
