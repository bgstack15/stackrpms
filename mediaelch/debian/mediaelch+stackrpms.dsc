Format: 3.0 (quilt)
Source: mediaelch
Binary: mediaelch
Architecture: any
Version: 2.10.6.4-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
XSBC-Original-Maintainer: Andre Meyering <info@andremeyering.de>
Homepage: http://www.mediaelch.de
Standards-Version: 4.1.4
Vcs-Browser: https://github.com/Komet/MediaElch/
Vcs-Git: https://github.com/Komet/MediaElch.git
Build-Depends: debhelper (>= 8.0.0), qtchooser, cdbs, libmediainfo0 | libmediainfo0v5, libmediainfo-dev, zlib1g-dev, libzen-dev, libcurl4-openssl-dev | libcurl4-gnutls-dev, libpulse-dev, cmake, libquazip1-qt6-dev, libxkbcommon-dev, qt6-base-dev, qt6-multimedia-dev, qt6-svg-dev, qt6-tools-dev
Package-List:
 mediaelch deb misc extra arch=any
Files:
 00000000000000000000000000000000 1 mediaelch.orig.tar.xz
 00000000000000000000000000000000 1 mediaelch.debian.tar.xz
