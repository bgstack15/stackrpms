# README for Dosbos-x
This is a dpkg for [dosbox-x](https://dosbox-x.com/)

DEPRECATED! Devuan now has this in the official repos. It entered unstable a few months after I converted this package build, so around 2023-09.

## Upstream
The original application is at the above link. This repo <https://bgstack15.ddns.net/cgit/stackrpms> is the original location of the dpkg recipe.

## Alternatives
Use Dosbox-X on Fedora, or with a Flatpak or from source. See PhantomX's pastured [rpm spec](https://github.com/PhantomX/chinforpms/blob/main/_pasture/dosbox-x/dosbox-x.spec).

## Reason for existence
I wanted a dpkg of dosbox-x, and my skillset includes converting rpm specs to debian recipes.

## Using

## Dependencies
See file [debian/control](debian/control) for that.

## Building
Standard debuild.

## References

1. <https://copr.fedorainfracloud.org/coprs/rob72/DOSBox-X/>
2. I [shared](https://github.com/joncampbell123/dosbox-x/issues/3356#issuecomment-1563077184) this in a discussion on the upstream repo.
