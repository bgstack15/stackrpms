Format: 3.0 (quilt)
Source: dosbox-x
Binary: dosbox-x
Architecture: any
Version: 2023.05.01-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://dosbox-x.com/
Standards-Version: 4.5.1
Vcs-Browser: https://bgstack15.ddns.net/cgit/dosbox-x
Vcs-Git: https://bgstack15.ddns.net/git/dosbox-x
Testsuite: autopkgtest
Build-Depends: debhelper-compat (= 13), dh-autoreconf, javahelper, libswscale-dev, libavformat-dev, libasound2-dev, g++, libsdl2-dev, libtool, libsdl2-mixer-dev, libfluidsynth-dev, libfreetype-dev | libfreetype6-dev, libpcap-dev | libpcap0.8-dev, libx11-dev, libxext-dev, libxkbcommon-dev, libslirp-dev, libpng-dev, libxrandr-dev, make, libgl1-mesa-dev, libncurses-dev, libpulse-dev, libsdl2-net-dev, zlib1g-dev
Package-List:
 dosbox-x deb games optional arch=any
Files:
 00000000000000000000000000000000 1 dosbox-x.orig.tar.gz
 00000000000000000000000000000000 1 dosbox-x.debian.tar.xz
