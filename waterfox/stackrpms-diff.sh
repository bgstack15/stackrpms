# use this file to display the differences between chinfo upstream and my work.
# usage: stackrpms/waterfox/stackrpms-diff.sh | vi -
diff -x 'README.md' -x 'bgstack15-waterfox-prefs.js' -x debian -x stackrpms*.diff -x stackrpms*.sh -x '*z' -x .*.swp -Naur chinforpms/waterfox stackrpms/waterfox
