# Waterfox upstream
https://github.com/MrAlex94/Waterfox/archive
Maintained in parallel to https://github.com/PhantomX/chinforpms/blob/master/waterfox/waterfox.spec
https://github.com/hawkeye116477/waterfox-deb/tree/master/waterfox-classic-kpe

# Reason for being in stackrpms
My distros do not package Waterfox, so I build it for myself.

# Alternatives
Palemoon is another Mozilla Firefox fork.
https://build.opensuse.org/package/view_file/home:hawkeye116477:waterfox/waterfox-current-kpe/rust_1.39.patch?expand=1

# Reverse dependency matrix
Distro     | Waterfox version
---------- | ----------------
All        | 2020.01-classic

# Additional info
* 2020-03-19 [https://src.fedoraproject.org/rpms/redhat-rpm-config/blob/master/f/buildflags.md](https://src.fedoraproject.org/rpms/redhat-rpm-config/blob/master/f/buildflags.md) is where the rpm scriptlet `_legacy_common_support 1` comes from, for telling gcc 10 to use `-fcommon`
* [https://github.com/hawkeye116477/waterfox-deb-rpm-arch-AppImage](https://github.com/hawkeye116477/waterfox-deb-rpm-arch-AppImage)

# Differences from upstream
## rpm
View the output of script [stackrpms-diff.sh](stackrpms-diff.sh) with this command:

    cd ~/dev
    stackrpms/waterfox/stackrpms-diff.sh | vi -
