// file: /usr/lib/waterfox/browser/defaults/preferences/bgstack15-waterfox-prefs.js
// deployed with waterfox package (rpm or dpkg) built by bgstack15
// last modified 2020-04-15
// reference:
//    https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
//    bgstack15-palemoon-prefs.js
pref("app.update.auto",                     false);
pref("app.update.autoInstallEnabled",       false);
pref("app.update.enabled",                  false);
pref("browser.allTabs.previews", false);
pref("browser.backspace_action", 0);
pref("browser.ctrlTab.previews", false);
pref("browser.ctrlTab.recentlyUsedOrder", false);
pref("browser.download.useDownloadDir", true);
pref("browser.newtab.choice", 1);
pref("browser.newtabpage.enabled", false);
pref("browser.newtabpage.storageVersion", 1);
pref("browser.safebrowsing.malware.enabled", false);
pref("browser.safebrowsing.phishing.enabled", false);
pref("browser.search.hiddenOneOffs", "DuckDuckGo");
pref("browser.search.suggest.enabled", false);
pref("browser.search.update", false);
pref("browser.search.widget.inNavBar", false);
pref("browser.sessionstore.restore_on_demand", false);
pref("browser.shell.checkDefaultBrowser",   false);
pref("browser.startup.homepage",            "data:text/plain,browser.startup.homepage=https://start.duckduckgo.com/");
pref("browser.startup.page", 3);
pref("browser.tabs.closeWindowWithLastTab", false);
pref("browser.uiCustomization.state", "{\"placements\":{\"PanelUI-contents\":[\"edit-controls\",\"zoom-controls\",\"new-window-button\",\"e10s-button\",\"privatebrowsing-button\",\"save-page-button\",\"print-button\",\"history-panelmenu\",\"fullscreen-button\",\"find-button\",\"preferences-button\",\"add-ons-button\",\"developer-button\",\"sync-button\"],\"addon-bar\":[\"addonbar-closebutton\",\"status-bar\"],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"urlbar-container\",\"bookmarks-menu-button\",\"downloads-button\",\"home-button\",\"jid1-n8wh2cbfc2qauj_jetpack-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"_f73df109-8fb4-453e-8373-f59e61ca4da3_-browser-action\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"toolbar-menubar\":[\"menubar-items\"]},\"seen\":[\"jid1-n8wh2cbfc2qauj_jetpack-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"_f73df109-8fb4-453e-8373-f59e61ca4da3_-browser-action\",\"developer-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\",\"PanelUI-contents\",\"addon-bar\"],\"currentVersion\":6,\"newElementCount\":0}");
pref("browser.uidensity", 1);
// These two have to stay undefined in Firefox 77+ in order for the drop-down for autocompletion to still work.
//pref("browser.urlbar.disableExtendForTests", true);
//pref("browser.urlbar.maxRichResults", 0);
pref("browser.urlbar.trimURLs", false);
pref("browser.urlbar.update1", false);
pref("browser.xul.error_pages.enabled", false);
pref("camera.control.face_detection.enabled", false);
pref("captivedetect.canonicalURL", "http://127.0.0.1:9999/");
pref("devtools.devedition.promo.url", "https://127.0.0.1:9999/");
pref("distribution.stackrpms.bookmarksProcessed", true);
pref("dom.push.serverURL", "wss://127.0.0.1:9999/");
pref("dom.event.clipboardevents.enabled", false);
pref("experiments.activeExperiment", false);
pref("extensions.enabledAddons", "%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:28.3.0");
pref("extensions.shownSelectionUI", true);
pref("extensions.update.autoUpdateDefault", false);
pref("extensions.webextensions.uuids", "{\"uBlock0@raymondhill.net\":\"7f64930e-0e43-4813-97c3-6fcb8a82e63b\",\"jid1-n8wH2cBfc2QaUj@jetpack\":\"5b1c5018-34cd-4778-902b-08741e3d0002\",\"{f73df109-8fb4-453e-8373-f59e61ca4da3}\":\"b7ece467-f6eb-4254-a815-1029330a9793\"}");
pref("findbar.highlightAll", true);
pref("gecko.handlerService.migrated", true);
pref("general.warnOnAboutConfig", false);
pref("geo.enabled", false);
pref("marionette.prefs.recommended", false);
pref("network.automatic-ntlm-auth.trusted-uris", ".ipa.smith122.com");
pref("network.captive-portal-service.enabled", false);
pref("network.cookie.prefsMigrated", true);
pref("network.dns.disablePrefetch", true);
pref("network.negotiate-auth.trusted-uris", ".ipa.smith122.com");
pref("network.predictor.enabled", false);
pref("network.prefetch-next", false);
pref("network.stricttransportsecurity.preloadlist", false);
pref("privacy.sanitize.migrateFx3Prefs", true);
pref("reader.parse-on-load.enabled", false);
pref("security.cert_pinning.enforcement_level", 0);
pref("security.ssl.errorReporting.url", "http://127.0.0.1:9999/");
pref("services.settings.server", "http://127.0.0.1:9999/");
pref("services.sync.declinedEngines", "");
pref("startup.homepage_override_url",       "");
pref("startup.homepage_override_url", "");
pref("toolkit.telemetry.reportingpolicy.firstRun", false);
pref("webextensions.storage.sync.serverURL", "http://127.0.0.1:9999/");
pref("xpinstall.whitelist.add", "");
// Control DNS over HTTPS (DoH) and Trusted Recursive Resolver (TRR).
// More about DoH: https://github.com/bambenek/block-doh
// https://blog.nightly.mozilla.org/2018/06/01/improving-dns-privacy-in-firefox/
// https://support.mozilla.org/en-US/kb/configuring-networks-disable-dns-over-https
// https://wiki.mozilla.org/Trusted_Recursive_Resolver
// 0: Off by default, 1: Firefox chooses faster, 2: TRR default w/DNS fallback,
// 3: TRR only mode, 4: Use DNS and shadow TRR for timings, 5: Disabled.
pref("network.trr.mode", 0);
pref("extensions.pocket.enabled", false);
pref("extensions.pocket.api", "http://localhost:9980");
pref("extensions.pocket.site", "http://localhost:9980");
