Format: 3.0 (quilt)
Source: waterfox
Binary: waterfox
Architecture: any
Version: 2022.11-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://www.waterfox.net/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12), autoconf2.13, autotools-dev, binutils-avr, cargo (>= 0.35), ccache, clang (>= 4.0) | clang-4.0 | clang-6.0 | clang-7, dpkg-dev (>= 1.16.1.1~), libasound2-dev, libbz2-dev, libclang-dev (>= 4.0) | libclang-4.0-dev | libclang-6.0-dev | libclang-7-dev, libdbus-glib-1-dev, libevent-dev (>= 1.4.1), libgconf2-dev, libglib2.0-dev (>= 2.16.0), libgtk2.0-dev (>= 2.10), libgtk-3-dev, libiw-dev, libjpeg-dev, libjsoncpp-dev, libnotify-dev, libreadline-dev, libstartup-notification0-dev, libtinfo-dev, libx11-dev, libx11-xcb-dev, libxt-dev, llvm-dev (>= 4.0) | llvm-4.0-dev | llvm-6.0-dev | llvm-7-dev, locales, lsb-release, nasm (>= 2.13), python2 (>= 2.7.18-2~) | python (>= 2.7) | python2.7, rustc (>= 1.34), unzip, xauth, xfonts-base, xvfb, yasm (>= 1.1), zip, zlib1g-dev
Package-List:
 waterfox deb web optional arch=any
Files:
 0000000000000000000000000000000 1 waterfox.orig.tar.gz
 0000000000000000000000000000000 1 waterfox.debian.tar.gz
