# Readme for notepadpp

## Upstream
[https://notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/)

## Reason for being in stackrpms
Notepad++ is a program for a non-free OS and I wanted to have the option to use it on GNU/Linux. The native alternatives were insufficient.

## Alternatives
Package `notepadqq` does not accomplish the same effect I wanted.

## Reverse dependency matrix
Distro     | notepadpp version
---------- | ----------------
Devuan     | 8.6.9

## Additional info
To run `osc` on Devuan as of 2024-07, you have to install `python3-zombie-imp`.

## Differences from upstream
None
