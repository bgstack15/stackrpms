#!/bin/sh
# Package: notepadpp
# Startdate: 2019-12-28 11:01
# Dependencies:
#    sudo apt-get install jq wget curl p7zip

currdir="${PWD}"
tmpfile1="$( mktemp )"

# FUNCTIONS
ferror() {
   printf "%s\n" "$@" 1>&2
}

get() {
   # get "https://example.com/example.zip"
   # get "https://example.com/example.zip" "outputname.zip"
   ___get_url="${1}"
   ___get_outfile="${2}"
   ___wget_options=""
   test -n "${___get_outfile}" && ___wget_options="-o ${___get_outfile}"

   test -n "${DEBUG}" && ferror "wget --quiet --content-disposition \"${___get_url}\" ${___wget_options}"
   test -z "${DRYRUN}" && wget --quiet --content-disposition "${___get_url}" ${___wget_options}
}

to_filename() {
   # call: to_filename "https://example.com/filename.ext"
   # returns: "filename.ext"
   printf "${1##*/}"
}

### Flow

# check dependencies
which jq 1>/dev/null 2>&1 || { echo "Please install jq! Aborted." ; exit 1; }

## 1. learn latest version
latest_version="$( curl --user-agent 'Mozilla (X11)' -s "https://notepad-plus-plus.org/downloads/" | grep -oiE "Current Version [0-9\.]+" | grep -oE "[0-9\.a-zA-Z]+$" )"
ferror "USING VERSION ${latest_version}"


## 2. dl npp main version, 32 and 64
# magic awk written 2020-10-05 to handle naming convention change from 7.8.9.bin to 7.9.portable.
lv_compare="$( echo "${latest_version}" | awk -v 'maxsections=3' -F'.' 'NF < maxsections {printf("%s",$0);for(i=NF;i<maxsections;i++)printf("%s",".0");printf("\n")} NF >= maxsections {print}' | awk -v 'maxdigits=2' -F'.' '{print $1*10^(maxdigits*2)+$2*10^(maxdigits)+$3}' )"
export fn_section=".portable"
short_version="${latest_version}"
test ${lv_compare} -lt 70900 && export fn_section=".bin"
test ${lv_compare} -ge 80000 && export short_version="8"
test ${lv_compare} -ge 80100 && export short_version="${latest_version}"
get "https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v${short_version}/npp.${latest_version}${fn_section}.7z"
get "https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v${short_version}/npp.${latest_version}${fn_section}.x64.7z"

## 3. dl 32 plugins
# fetch master list of plugins
curl -L -s https://github.com/notepad-plus-plus/nppPluginList/raw/master/src/pl.x86.json > "${tmpfile1}"

# Compare plugin
url_current_plugin_32_Compare="$( cat "${tmpfile1}" | jq '.["npp-plugins"][] | select(."display-name" == "Compare") | .repository' -r )"
file_current_plugin_32_Compare="$( to_filename "${url_current_plugin_32_Compare}" )"
get "${url_current_plugin_32_Compare}"
# TextFX plugin
url_current_plugin_32_TextFX="$( cat "${tmpfile1}" | jq '.["npp-plugins"][] | select(."display-name" == "TextFX Characters") | .repository' -r )"
file_current_plugin_32_TextFX="$( to_filename "${url_current_plugin_32_TextFX}" )"
get "${url_current_plugin_32_TextFX}"

## 4. dl 64 plugins
curl -L -s https://github.com/notepad-plus-plus/nppPluginList/raw/master/src/pl.x64.json > "${tmpfile1}"

# Compare plugin
url_current_plugin_64_Compare="$( cat "${tmpfile1}" | jq '.["npp-plugins"][] | select(."display-name" == "Compare") | .repository' -r )"
file_current_plugin_64_Compare="$( to_filename "${url_current_plugin_64_Compare}" )"
get "${url_current_plugin_64_Compare}"

## 5. assemble orig tarball
test -n "${DEBUG}" && ferror "mkdir -p \"notepadpp-${latest_version}\" ; cd \"notepadpp-${latest_version}\""
test -z "${DRYRUN}" && { mkdir -p "notepadpp-${latest_version}" ; cd "notepadpp-${latest_version}" ; }

test -n "${DEBUG}" && ferror "mkdir -p notepadpp-bin32 notepadpp-bin64"
test -z "${DRYRUN}" && { mkdir -p notepadpp-bin32 notepadpp-bin64 ; }

test -n "${DEBUG}" && ferror "cd notepadpp-bin32 ; 7za -bd -aoa x ../../\"npp.${latest_version}${fn_section}.7z\"     ; cd plugins ; 7za -bd -aoa x ../../../\"${file_current_plugin_32_Compare}\" ; 7za -bd -aoa x ../../../\"${file_current_plugin_32_TextFX}\" ; cd ../..  "
test -z "${DRYRUN}" && { cd notepadpp-bin32 ; 7za -bd -aoa x ../../"npp.${latest_version}${fn_section}.7z"     ; cd plugins ; 7za -bd -aoa x ../../../"${file_current_plugin_32_Compare}" ; 7za -bd -aoa x ../../../"${file_current_plugin_32_TextFX}" ; cd ../..; }
test -n "${DEBUG}" && ferror "cd notepadpp-bin64 ; 7za -bd -aoa x ../../\"npp.${latest_version}${fn_section}.x64.7z\" ; cd plugins ; 7za -bd -aoa x ../../../\"${file_current_plugin_64_Compare}\" ; cd ../.. "
test -z "${DRYRUN}" && { cd notepadpp-bin64 ; 7za -bd -aoa x ../../"npp.${latest_version}${fn_section}.x64.7z" ; cd plugins ; 7za -bd -aoa x ../../../"${file_current_plugin_64_Compare}" ; cd ../.. ; }
test -n "${DEBUG}" && ferror "cd .."
test -z "${DRYRUN}" && cd ..
test -n "${DEBUG}" && ferror "tar -zcf \"notepadpp_${latest_version}.orig.tar.gz\" \"notepadpp-${latest_version}\""
test -z "${DRYRUN}" && tar -zcf "notepadpp_${latest_version}.orig.tar.gz" "notepadpp-${latest_version}"

# CLEAN UP
rm -rf "${tmpfile1}" "notepadpp-${latest_version}/" "${file_current_plugin_32_Compare}" "${file_current_plugin_32_TextFX}" "${file_current_plugin_64_Compare}" "npp.${latest_version}${fn_section}.7z" "npp.${latest_version}${fn_section}.x64.7z"
