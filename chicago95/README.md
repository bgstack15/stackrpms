# Chicago95 upstream
https://github.com/grassmunk/Chicago95/
https://build.opensuse.org/package/show/home:antergos/lightdm-webkit2-greeter

# Alternatives
Many other Windows 95 themes exist for Xfce and other DEs.

# Reason for being in stackrpms
This is an original package of the Chicago95 Xfce theme that provides a complete conversion mod to make Xfce resemble Windows 95. Related packages include the following:
* lightdm-webkit2-greeter
* plymouth

# Reverse dependency matrix
Distro       | Chicago95 version
------------ | ---------------
dpkg distros | 0.0.1

# Differences from upstream
None
