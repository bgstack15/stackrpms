# xfdesktop upstream
https://src.fedoraproject.org/rpms/xfdesktop.git

# Reason for being in stackrpms
CentOS 8 does not package xfdesktop yet, and neither does copr://nikitines/XFCE so any version of xfdesktop will suffice.

# Reverse dependency matrix
Distro     | xfdesktop version
---------- | ----------------
CentOS 8   | 4.14.1

# Differences from upstream
CentOS 8 uses libgudev-devel instead of libgudev1-devel, so changing the one dependency name allowed the rpmbuild to finish successfully.
