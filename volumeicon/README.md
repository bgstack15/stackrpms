# volumeicon upstream
https://src.fedoraproject.org/rpms/volumeicon.git

# Reason for being in stackrpms
Any of the standard CentOS 8 repos do not package volumeicon yet, and it is a pleasant alternative for controlling volume from the notification area.

# Reverse dependency matrix
Distro     | volumeicon version
---------- | ----------------
CentOS 8   | 0.5.1

# Differences from upstream
CentOS 8 uses keybinder3-devel instead of keybinder-devel.
