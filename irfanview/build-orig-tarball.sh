#!/bin/sh
# Package: irfanview
# Startdate: 2020-11-06 14:05
# References:
#    ublock-origin-combined/build-orig-tarball.sh
#    irfanview/debian/README.debian from this package
# Dependencies:
#    sudo apt-get install wget curl sed awk

frontpageurl="https://www.irfanview.com/"
package_name="irfanview"

# FUNCTIONS
ferror() {
   printf "%s\n" "$@" 1>&2
}

get() {
   # get "https://example.com/example.zip"
   # get "https://example.com/example.zip" "outputname.zip"
   ___get_url="${1}"
   ___get_outfile="${2}"
   ___wget_options=""
   test -n "${___get_outfile}" && ___wget_options="-O ${___get_outfile}"

   test -n "${DEBUG}" && ferror "wget --quiet --referer \"${___get_url}\" --user-agent='Mozilla (X11)' --content-disposition \"${___get_url}\" ${___wget_options}"
   test -z "${DRYRUN}" && wget --quiet --referer "${___get_url}" --user-agent='Mozilla (X11)' --content-disposition "${___get_url}" ${___wget_options}
}

to_filename() {
   # call: to_filename "https://example.com/filename.ext"
   # returns: "filename.ext"
   printf "${1##*/}"
}

### Flow

# check dependencies
#which jq 1>/dev/null 2>&1 || { echo "Please install jq! Aborted." ; exit 1; }

## 1. learn latest version file
url_contents="$( curl -s "${frontpageurl}" )"
latest_version="$( echo "${url_contents}" | grep -oiE "current version.{0,8}[0-9\.]+" | awk '{print $NF}' | sort -n | tail -n1 )"
latest_version_no_dot="$( echo "${latest_version}" | tr -dc '[0-9]' )"

## 2. dl it
test -z "${NOFETCH}" && {
   get "http://www.irfanview.info/files/iview${latest_version_no_dot}.zip"
   get "http://www.irfanview.info/files/iview${latest_version_no_dot}_x64.zip"
   get "http://www.irfanview.info/files/iview${latest_version_no_dot}_plugins.zip"
   get "http://www.irfanview.info/files/iview${latest_version_no_dot}_plugins_x64.zip"
}

## 5. assemble orig tarball
iver="${latest_version_no_dot}"

# original logic which is not needed due to the automation above.
#dir_iver="$( echo "${iver}" | head -c1 ).$( echo "${iver}" | tail -c3 )"

test -n "${DEBUG}" && ferror "mkdir -p \"${package_name}-${latest_version}\"; cd \"${package_name}-${latest_version}\""
test -z "${DRYRUN}" && { mkdir -p "${package_name}-${latest_version}" ; cd "${package_name}-${latest_version}" ; }

test -n "${DEBUG}" && ferror "mkdir -p \"${package_name}-bin32\" \"${package_name}-bin64\""
test -z "${DRYRUN}" && { mkdir -p "${package_name}-bin32" "${package_name}-bin64" ; }

test -n "${DEBUG}" && ferror "cd \"${package_name}-bin32\" ; unzip -o ../../iview${iver}.zip     ; cd Plugins ; unzip -o ../../../iview${iver}_plugins.zip     ; cd ../.."
test -z "${DRYRUN}" && { cd "${package_name}-bin32" ; unzip -o ../../iview${iver}.zip     ; cd Plugins ; unzip -o ../../../iview${iver}_plugins.zip     ; cd ../.. ; }

test -n "${DEBUG}" && ferror "cd \"${package_name}-bin64\" ; unzip -o ../../iview${iver}_x64.zip ; cd Plugins ; unzip -o ../../../iview${iver}_plugins_x64.zip ; cd ../.."
test -z "${DRYRUN}" && { cd "${package_name}-bin64" ; unzip -o ../../iview${iver}_x64.zip ; cd Plugins ; unzip -o ../../../iview${iver}_plugins_x64.zip ; cd ../.. ; }

test -n "${DEBUG}" && ferror "cd .."
test -z "${DRYRUN}" && { cd .. ; }

test -n "${DEBUG}" && ferror "tar -zcf \"${package_name}_${latest_version}.orig.tar.gz\" \"${package_name}-${latest_version}\""
test -z "${DRYRUN}" && tar -zcf "${package_name}_${latest_version}.orig.tar.gz" "${package_name}-${latest_version}"

# CLEAN UP
rm -rf "${package_name}-${latest_version}/"
