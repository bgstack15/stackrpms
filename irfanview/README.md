# Irfanview upstream
https://www.irfanview.com

# Reason for being in stackrpms
Irfanview is a freeware program and is not packaged by my distros. This is an original package.

# Alternatives
At least one distro does package Irfanview:
* PCLinuxOS

# Reverse dependency matrix
Distro    | Irfanview version
--------- | -----------------
all       | 4.54

# Differences from upstream packaging
None
