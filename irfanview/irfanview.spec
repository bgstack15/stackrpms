%global dummy_package 0
%global include_plugins 1
%global devtty /dev/null
%global upstream_url http://gitlab.com/bgstack15/big-sources/raw/master/%{name}
#global upstream_url http://www.irfanview.info/files

Name:		irfanview
Version:	4.62
Release:	1
Summary:	graphics viewer from non-free OS
%define version_num %( echo %version | tr -d '\.' )

Group:   Applications/Graphics
License:	Freeware
URL:		http://bgstack15.wordpress.com
# from original upstream, you must download like:
#    curl 'https://www.irfanview.info/files/iview459_x64.zip' --compressed -H 'Referer: https://www.irfanview.info/files/iview459_x64.zip' -A 'Mozilla (X11)' -ORJ
Source0: %{upstream_url}/iview%{version_num}.zip
Source1: %{upstream_url}/iview%{version_num}_plugins.zip
Source2: %{upstream_url}/iview%{version_num}_x64.zip
Source3: %{upstream_url}/iview%{version_num}_plugins_x64.zip
Source4: %{name}.desktop
Source5: %{name}32
Source6: %{name}64
Source7: %{name}-vlc
Source8: i_view32.ini
Source9: %{name}-icons.tgz
Source10: %{name}-common

Packager:	Bgstack15 <bgstack15@gmail.com>
Buildarch:	noarch
BuildRequires: desktop-file-utils
BuildRequires: ImageMagick
BuildRequires: unzip

Provides:	application(irfanview.desktop)
Provides:	mimehandler(image/bmp)
Provides:	mimehandler(image/gif)
Provides:	mimehandler(image/ico)
Provides:	mimehandler(image/jpeg)
Provides:	mimehandler(image/jpg)
Provides:	mimehandler(image/png)
Provides:	mimehandler(image/tiff)
Provides:	mimehandler(vnd.adobe.photoshop)
Provides:	mimehandler(x-content/image-dcf)

%description
Irfanview is an amazing graphics application for a different platform. Irfanview is ported to GNU/Linux using wine.

%package common
Summary:	common components for irfanview
Requires(pre): bgscripts-core >= 1.1-20

%description common
Common elements like desktop file and icons.

%package bin32
Requires:	/usr/bin/wine32
Requires:	%{name}-common = %{version}-%{release}
Provides:	irfanview
Obsoletes:  irfan < 4.51-2
Obsoletes:  irfanview < 4.53-1
Summary: irfanview 32-bit

%description bin32
The 32-bit release of irfanview

%package bin64
Requires:	/usr/bin/wine64
Requires:	%{name}-common = %{version}-%{release}
Provides:	irfanview
Obsoletes:  irfan < 4.51-2
Obsoletes:  irfanview < 4.53-1
Summary: irfanview 64-bit

%description bin64
The 64-bit release of irfanview

%prep
export UNZIP=-o
mkdir -p irfanview-bin32 irfanview-bin64
pushd irfanview-bin32 ; unzip %{SOURCE0} ; cd Plugins ; unzip %{SOURCE1} ; popd
pushd irfanview-bin64 ; unzip %{SOURCE2} ; cd Plugins ; unzip %{SOURCE3} ; popd

%build
# the upstream source is the binaries; nothing to do here
:

%install
install -d %{buildroot}%{_datadir}/%{name}32 %{buildroot}%{_datadir}/%{name}64 %{buildroot}%{_bindir} %{buildroot}%{_datadir}/applications %{buildroot}%{_docdir}/%{name}

# architectures
for thisarch in 32 64 ;
do
   cd %{name}-bin${thisarch}
   install -m0755 -t %{buildroot}%{_datadir}/%{name}${thisarch} i_view${thisarch}.exe
   install -m0644 -t %{buildroot}%{_datadir}/%{name}${thisarch} *.chm
   cp -pr Languages Plugins Toolbars %{buildroot}%{_datadir}/%{name}${thisarch}
   test ${thisarch} -eq 32 && __ts=%{SOURCE5} || __ts=%{SOURCE6}
   install -m0666 -t %{buildroot}%{_bindir} ${__ts}
   cp -pr %{SOURCE8} %{buildroot}%{_datadir}/%{name}${thisarch}/i_view${thisarch}.ini
   cd ..
done

# desktop files
%{__mkdir_p} %{buildroot}%{_datadir}/applications
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{SOURCE4}

# application start and default scripts
%{__install} -p -D -m0754 %{SOURCE8} %{buildroot}%{_bindir}/%{name}-vlc
%{__install} -p -D -m0754 %{SOURCE10} %{buildroot}%{_bindir}/%{name}-common

# man pages
pushd %{name}-bin32
%{__install} -p -D -m0644 -t %{buildroot}%{_docdir}/%{name} *.txt *.chm
%{__install} -p -m0755 -d %{buildroot}%{_docdir}/%{name}/Html
%{__install} -p -D -m0644 -t %{buildroot}%{_docdir}/%{name}/Html Html/*
popd

# icons
%{__tar} -zxvf %{SOURCE9} %{name}-circle.svg

ff=" -filter Lanczos"
for s in 16 22 24 32 48 64 96 128 256 ;
do
   dir=%{buildroot}%{_datadir}/icons/hicolor/${s}x${s}
   rr=" -resize ${s}x${s}"
   mkdir -p ${dir}/apps
   # apps
   convert -background none ${ff} ${rr} %{name}-circle.svg ${dir}/apps/%{name}.png
   # mimetypes
   # none
done

mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps
cp -p %{name}-circle.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg

# system services
# none

%clean
rm -rf %{buildroot} || :

%post
touch --no-create %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :

%post bin32
case "${1}" in
   1|2)
      update-alternatives --install /usr/bin/irfanview irfanview /usr/bin/irfanview32 70
      update-alternatives --install /usr/bin/x-graphics-viewer x-graphics-viewer /usr/bin/irfanview32 70
      ;;
esac

%post bin64
case "${1}" in
   1|2)
      update-alternatives --install /usr/bin/irfanview irfanview /usr/bin/irfanview64 60
      update-alternatives --install /usr/bin/x-graphics-viewer x-graphics-viewer /usr/bin/irfanview64 60
      ;;
esac

%preun bin32
case "${1}" in
   0)
      update-alternatives --remove irfanview /usr/bin/irfanview32
      update-alternatives --remove x-graphics-viewer /usr/bin/irfanview32
      ;;
esac

%preun bin64
case "${1}" in
   0)
      update-alternatives --remove irfanview /usr/bin/irfanview64
      update-alternatives --remove x-graphics-viewer /usr/bin/irfanview64
      ;;
esac

%postun
if test "$1" = "0" ;
then
   touch --no-create %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :
fi

%posttrans
update-desktop-database 1>/dev/null 2>&1 & :
gtk-update-icon-cache %{_datadir}/icons/hicolor 1>/dev/null 2>&1 & :
update-mime-database -n ${_datadir}/mime 1>/dev/null 2>&1 & :

%files common
%{_docdir}/%{name}
%{_bindir}/%{name}-*
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/*/*
%ghost %attr(755, -, -) %{_bindir}/%{name}

%files bin32
%config %attr(666, -, -) %{_datadir}/%{name}32/i_view32.ini
%attr(0755, -, -) %{_bindir}/%{name}32
%{_datadir}/%{name}32

%files bin64
%config %attr(666, -, -) %{_datadir}/%{name}64/i_view64.ini
%attr(0755, -, -) %{_bindir}/%{name}64
%{_datadir}/%{name}64

%changelog
* Mon Jan 02 2023 B. Stack <bgstack15@gmail.com> - 4.62-1
- version bump

* Thu Mar 24 2022 B. Stack <bgstack15@gmail.com> - 4.60-1
- version bump

* Mon Dec 06 2021 B. Stack <bgstack15@gmail.com> - 4.59-1
- version bump

* Wed May 26 2021 B. Stack <bgstack15@gmail.com> - 4.58-1
- version bump

* Mon Feb 01 2021 B. Stack <bgstack15@gmail.com> - 4.57-1
- version bump

* Fri Nov 06 2020 B Stack <bgstack15@gmail.com> - 4.56-1
- version bump

* Tue Jul 14 2020 B Stack <bgstack15@gmail.com> - 4.54-3
- Improve filename handling again

* Thu Jun 18 2020 B Stack <bgstack15@gmail.com> - 4.54-2
- Improve filename handling

* Wed Dec 18 2019 B Stack <bgstack15@gmail.com> - 4.54-1
- version bump

* Sun Jun 16 2019 B Stack <bgstack15@gmail.com> - 4.53-2
- improve app start scripts and mimetypes

* Sat May 18 2019 B Stack <bgstack15@gmail.com> - 4.53-1
- version bump
