%global pname openssl
%global	debug_package %{nil}
Name:		openssl110
Version:	1.1.0i
Release:	1
Summary:	Bundle of openssl 1.1.0f for el7 for internal use

# References
# https://linuxscriptshub.com/update-openssl-1-1-0-centos-6-9-7-0/
# https://syslint.com/blog/tutorial/how-to-upgrade-openssl-on-centos-7-or-rhel-7/
# https://webhostinggeeks.com/howto/how-to-install-and-update-openssl-on-centos-6-centos-7/
Group:		System Environment/Libraries
License:	OpenSSL
URL:		http://www.openssl.org/
Source0:	https://www.openssl.org/source/openssl-%{version}.tar.gz

BuildRequires:	coreutils
BuildRequires:	perl

%description
Custom build of openssl 1.1.0 to provide the openssl s_client -proxy flag.
Binary name is openssl110.

%prep
%setup -q -c

%build
cd %{pname}-%{version}
./config
make

%install
cd %{pname}-%{version}
# skip the install_docs
make install_sw install_ssldirs DESTDIR=%{buildroot}

# customizations so this can coexist with the system openssl
pushd %{buildroot}
mv .%{_prefix}/local/bin/%{pname} .%{_prefix}/local/bin/%{name}
rm .%{_prefix}/local/bin/c_rehash || :
install -d %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "/usr/local/lib64" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}.conf

popd

%post
ldconfig 1>/dev/null 2>&1 || :

%files
%doc
#%%defattr(0644, root, root)
%{_sysconfdir}/ld.so.conf.d/*
%{_prefix}/local/bin
%{_prefix}/local/%{_lib}
%{_prefix}/local/ssl
%{_prefix}/local/include

%changelog
* Thu Sep 06 2018 B Stack <bgstack15@gmail.com> 1.1.0i-1
- rpm rebuilt
