# openssl110 upstream
https://www.openssl.org/source/openssl-1.1.0i.tar.gz

# Reason for being in stackrpms
I wanted the flag -proxy available on the openssl command in CentOS 7, so I compiled a fairly generic rpm. This is not the openssl library compiled for specific use with FreeFileSync. See https://gitlab.com/bgstack15/ffs-dependencies/tree/master/openssl-freefilesync for that one.

# Reverse dependency matrix
Distro     | openssl110 version
---------- | ----------------------
CentOS 7   | 1.1.0i

# Differences from upstream
None
