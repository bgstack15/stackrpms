# Readme for `mime_types_editor`

## Upstream
[https://github.com/i026e/mime_types_editor](https://github.com/i026e/mime_types_editor)
My fork is at [https://gitlab.com/bgstack15/mime_types_editor](https://gitlab.com/bgstack15/mime_types_editor)

## Alternatives
`xfce4-mime-settings` from dpkg `xfce4-settings` is a compiled tool that does the exact same thing.
The plethora of cli tools.
A qt [alternative](https://github.com/rchaput/xdg-prefs)

## Reason for being in stackrpms
No one packages this small project for any distros that I know of. I liked this one because I can read Python, and I rpefer gtk over qt.

## Reverse dependency matrix
Distro     | mime_types_editor version
---------- | ---------------
All dpkg   | any git branch

## Additional info

## Differences from upstream
My fork includes a search box and a working about screen.
