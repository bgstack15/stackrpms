Format: 3.0 (quilt)
Source: mime-types-editor
Binary: mime-types-editor
Architecture: all
Version: 0.0.1-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/i026e/mime_types_editor
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), go-md2man
Package-List:
 mime-types-editor deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 mime_types_editor.orig.tar.gz
 00000000000000000000000000000000 1 mime-types-editor+stackrpms.debian.tar.xz
