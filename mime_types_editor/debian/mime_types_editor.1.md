mime_types_editor 1 "March 2020" mime_types_editor "User Manual"
================================================================
# NAME
mime_types_editor
# SYNOPSIS
mime_types_editor
# DESCRIPTION
Display a graphical mimetype editor. Using either a category or application view, view and edit the mimetypes installed on the system.  
No command-line parameters exist.
# AUTHOR
Pavel Klevakin (i026e)
# REPORTING BUGS
Mime_types_editor: `<https://github.com/i026e/mime_types_editor>`  
Dpkg: `<https://gitlab.com/bgstack15/stackrpms/>`
# COPYRIGHT
Copyright (C) 2016 Pavel Klevakin. License GPLv2: GNU GPL version 2.
This is free software: you are free to change it and redistribute it. There is NO WARRANTY, to the extent permitted by law.
# SEE ALSO
Application source at /usr/libexec/mime-types-editor/
# MAN PAGE
Manual pages written by Ben Stack `<https://bgstack15.wordpress.com/>` for Devuan GNU+Linux.
