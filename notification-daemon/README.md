# notification-daemon upstream
https://mirrors.rit.edu/fedora/fedora/linux/releases/30/Everything/source/tree/Packages/n/notification-daemon-3.20.0-7.fc30.src.rpm

# Reason for being in stackrpms
CentOS 8 does not package notification-daemon yet, so any version of notification-daemon will suffice. This version was available from Fedora. Palemoon needs this application.

# Reverse dependency matrix
Distro     | palemoon version | notification-daemon version
---------- | ---------------- | ---------------
CentOS 8   | 28.7.1           | 3.20.0

# Differences from upstream
Change Source to https for copr.
