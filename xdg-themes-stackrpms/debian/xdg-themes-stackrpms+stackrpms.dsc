Format: 3.0 (quilt)
Source: xdg-themes-stackrpms
Binary: xdg-themes-stackrpms
Architecture: all
Version: 0.0.5-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.ddns.net/blog/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~)
Package-List:
 xdg-themes-stackrpms deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 xdg-themes-stackrpms.orig.tar.gz
 00000000000000000000000000000000 1 xdg-themes-stackrpms.debian.tar.xz
