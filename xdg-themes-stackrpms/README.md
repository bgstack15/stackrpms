# Readme for xdg-themes-stackrpms
This package stores my xdg-compliant desktop environment themes.

## Upstream
The contents were originally based on a few things but are so far removed that it is not worth listing any direct upstreams. Theme `bgstack15-red` was based on Arc theme circa 2016 and some unspecified Cinnamon/metacity theme from Korora Linux.

## Alternatives
Any gtk theme you want. 

## Reason for being in stackrpms
My own package.

## Dependencies
Distro          | xdg-themes-stackrpms version  | libgtk-3-0 (gtk3-classic)
--------------- | ----------------------------- | ---------------------
Devuan unstable | (master branch)               | 3.24.38-103+stackrpms

The `libgtk-3-0` should be [my build](https://bgstack15.ddns.net/cgit/gtk3-classic-build/) (packages for [Devuan](https://build.opensuse.org/project/show/home:bgstack15:gtk3-classic)) of [gtk3-classic](https://github.com/lah7/gtk3-classic).

## References
[Arc theme](https://github.com/horst3180/Arc-theme)

## Differences from upstream
N/A
