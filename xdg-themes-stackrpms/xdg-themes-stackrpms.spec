%define debug_package %{nil}
%global tarballdir xdg-themes-stackrpms-master
Name:    xdg-themes-stackrpms
Summary: stackrpms themes for xdg-compliant desktop environments
Version: 0.0.5
Release: 1%{?dist}
License: CC-BY-SA 4.0
Url:     https://gitlab.com/bgstack15/
BuildArch: noarch
Source0: README.md
Source1: %{url}/%{name}/-/archive/master/%{name}-master.tar.gz
%if 0%{?fedora} || 0%{?rhel} >= 8
Recommends: gtk3-automemonics
Recommends: gtk3-nocsd
Recommends: gtk3-nooverlayscrollbar
Recommends: numix-icon-theme-circle
%endif

%description
My custom themes.

%prep
%{__tar} -zxf %{SOURCE1}

%build
echo "no build action required"

%install
cd %{tarballdir}
%{__install} -m0755 -d %{buildroot}%{_datadir}/themes
cp -pr src/%{_datadir}/themes/* %{buildroot}%{_datadir}/themes/

%if 0%{?fedora}
%{__install} -m0755 -d %{buildroot}%{_docdir}/%{name}
cp -p %{SOURCE0} %{buildroot}%{_docdir}/%{name}/
%endif

%check
:

%files
%if 0%{?fedora}
%doc README.md
%endif
%{_datadir}/themes/bgstack15-red

%changelog
* Sun Jan 07 2023 B. Stack <bgstack15@gmail.com> - 0.0.5-1
- Fix want#1 Add treeview alternating colors

* Wed Oct 05 2022 B. Stack <bgstack15@gmail.com> - 0.0.4-1
- Version bump
- Fix #3 gtk2 automnemonics

- Add recommended gtk3 inhibitors
* Wed Jul 13 2021 B. Stack <bgstack15@gmail.com> - 0.0.3-1
- Version bump
- Add recommended gtk3 inhibitors

* Mon Mar 22 2021 B. Stack <bgstack15@gmail.com> - 0.0.2
- Version bump

* Wed Feb 12 2020 B. Stack <bgstack15@gmail.com> - 0.0.1
- Initial rpm release
- use source1 trick for copr
