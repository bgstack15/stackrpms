%global  dummy_package   0
%global  use_git  1
%if 0%{use_git}
%global tarballdir %{name}-master
%else
%global tarballdir %{name}-%{version}
%endif
%define license_files %{srcdir}/License.txt

# turn off debug package
%define debug_package %{nil}

Name:		move-to-next-monitor
Version:	0.0.1
Release:	2
BuildArch: noarch
Summary:	Script that facilitates moving a window to the next monitor

Group:	Utility
License:	GPL-3.0
URL:		https://gitlab.com/bgstack15/move-to-next-monitor
%if 0%{use_git}
# Source0: https://github.com/vanaoff/move-to-next-monitor/archive/master.zip
Source0: https://gitlab.com/bgstack15/%{name}/-/archive/master/%{name}-master.tar.gz
%else
Source0: https://gitlab.com/bgstack15/%{name}/-/archive/%{version}/%{name}-master.tar.gz
%endif
Source1: move-to-next-monitor.Makefile
Source2: README.md
#Patch1:  none.patch

Packager:	Bgstack15 <bgstack15@gmail.com>
BuildRequires:	coreutils
Requires: python3
Requires: wmctrl
Requires: xdotool
%if 0%{?rhel} == 7
Requires: xorg-x11-utils
%else
Requires: (xorg-x11-utils or ((xprop and xrandr) and xwininfo))
BuildRequires: make
%endif

%description
move-to-next-monitor is a small script that makes it easy for display managers without this native functionality to provide it. You can assign a shortcut key in the display manager settings to the script to use it.

%prep
%setup -q -c
# copy in custom Makefile and adjust to use tarball directory as source directory
%{__cp} -p %{SOURCE1} ./Makefile
%{__sed} -i -r -e '/^SRCDIR\s*=/s:\$\(CURDIR\)\s*$:$(CURDIR)/'"%{tarballdir}"':;' ./Makefile

#pushd %{tarballdir}
#%patch0 -p1
#popd

%build
:

%install
%make_install

# man page
%{__mkdir_p} %{buildroot}%{_pkgdocdir}
%{__cp} -p "%{SOURCE2}" %{buildroot}%{_pkgdocdir}/

%clean
%{__rm} -rf %{buildroot} || :

%post
:

%preun
# is it a final removal?
#if test "$1" = "0" ;
#then
#fi   
:

%postun
:

%posttrans
:

%files
%if 0%{?rhel} != 7
%license LICENSE
%endif
%doc %{_pkgdocdir}/*
%{_bindir}/%{name}

%changelog
* Tue Nov 30 2021 B. Stack <bgstack15@gmail.com> 0.0.1-2
- rebuild for Fedora 34 with split x utils packages

* Fri Nov 30 2018 B. Stack <bgstack15@gmail.com> 0.0.1-1
- initial package built
