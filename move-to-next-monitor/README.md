# Readme for move-to-next-monitor
To take full advantage of move-to-next-monitor, set it up on a keyboard shortcut in your display manager settings.

A certain non-free operating system uses Super+Shift+Left and Super+Shift+Right.

| Command                        | Shortcut key      |
| ------------------------------ | ----------------- |
| move-to-next-monitor           | Super+Shift+right |
| move-to-next-monitor --reverse | Super+Shift+left  |

### Alternatives
Alternative includes: [https://github.com/calandoa/movescreen](https://github.com/calandoa/movescreen)
This tool is unnecessary in Fluxbox, because Fluxbox users can put in the ~/.fluxbox/keys file these entries:

    Mod4 Shift 113 :MacroCmd {SendToPrevHead}
    Mod4 Shift 114 :MacroCmd {SendToNextHead}

### Dependencies
Several dependencies are needed for this script to operate correctly.
* python
* wmctrl
* xdotool
* xprop
* xrandr

### move-to-next-monitor upstream
The original concept was written by [jcOOke](https://github.com/jc00ke/move-to-next-monitor). A reiteration in python was provided by [vanaoff](https://github.com/vanaoff/move-to-next-monitor).
The upstream package for this rpm is [https://gitlab.com/bgstack15/move-to-next-monitor](https://gitlab.com/bgstack15/move-to-next-monitor).

# Reason for being in stackrpms
No one actually provided a distro package for this program.

# Reverse dependency matrix
Distro     | move-to-next-monitor version
---------- | ---------------
All        | 0.0.1

# Differences from upstream
Unknown
