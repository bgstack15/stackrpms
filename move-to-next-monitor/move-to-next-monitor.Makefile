# File: Makefile for move-to-next-monitor
# Location: bgscripts source package
# Author: bgstack15
# Startdate: 2018-11-30
# Title: Makefile for move-to-next-monitor source package
# Purpose: To use traditional Unix make utility
# History:
# Usage:
# Reference:
# Improve:
# Document:
# Dependencies:

APPNAME    = move-to-next-monitor
APPVERSION = 0.0.1
SRCDIR     = $(CURDIR)
prefix     = /usr
BINDIR     = $(DESTDIR)$(prefix)/bin
LICENSEDIR = $(DESTDIR)$(prefix)/share/licenses/$(APPNAME)

awkbin     :=$(shell which awk)
cpbin      :=$(shell which cp)
echobin    :=$(shell which echo)
findbin    :=$(shell which find)
grepbin    :=$(shell which grep)
installbin :=$(shell which install)
rmbin      :=$(shell which rm)
sedbin     :=$(shell which sed)
sortbin    :=$(shell which sort)
truebin    :=$(shell which true)

all:
	${echobin} "No compilation in this package."

.PHONY: clean install install_files uninstall list

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | ${awkbin} -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | ${sortbin} | ${grepbin} -E -v -e '^[^[:alnum:]]' -e '^$@$$'

install: install_files

install_files: 
	${echobin} Installing files to ${DESTDIR}
	${installbin} -d ${BINDIR} ${LICENSEDIR}
	ls -l ${SRCDIR}
	${installbin} -m '0755' ${SRCDIR}/move-to-next-monitor ${BINDIR}/
	${installbin} -m '0644' ${SRCDIR}/LICENSE ${LICENSEDIR}/

uninstall:
	${echobin} SRCDIR=${SRCDIR}
	${rmbin} -f ${BINDIR}/move-to-next-monitor
	${rmbin} -f ${LICENSEDIR}/LICENSE

	# remove all installed directories that are now blank.
	for word in $$( ${findbin} ${DESTDIR} -mindepth 1 -type d -printf '%p\n' | ${awkbin} '{print length, $$0 }' | ${sortbin} -rn | ${awkbin} '{print $$2}' ) ; do ${findbin} $${word} -mindepth 1 1>/dev/null 2>&1 | read 1>/dev/null 2>&1 || { rmdir "$${word}" 2>/dev/null || ${truebin} ; } ; done

clean:
	-${echobin} "target $@ not implemented yet! Gotta say unh."
