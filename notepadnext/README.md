# Readme for notepadnext

## Upstream
[https://github.com/dail8859/NotepadNext](https://github.com/dail8859/NotepadNext)

## Reason for being in stackrpms
NotepadNext is a cross-platform reimplementation of [Notepad++](https://notepad-plus-plus.org/). Upstream has expressed a [disinterest](https://github.com/dail8859/NotepadNext/pull/97) in a dpkg build recipe.

## Alternatives
Use notepadpp in this same repository, or use NotepadNext [AppImage](https://github.com/dail8859/NotepadNext/releases).

## Reverse dependency matrix
Distro     | notepadnext version
---------- | ----------------
Devuan     | 0.5.6

## Additional info

## Differences from upstream
N/A
