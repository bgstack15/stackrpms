Format: 3.0 (quilt)
Source: notepadnext
Binary: notepadnext
Architecture: any
Version: 0.10-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/dail8859/NotepadNext
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), libqt6core5compat6-dev, libqt6core6, qmake6, qt6-base-private-dev, qtbase5-private-dev
Package-List:
 notepadnext deb editors optional arch=any
Files:
 00000000000000000000000000000000 1 notepadnext.orig.tar.gz
 00000000000000000000000000000000 1 notepadnext.debian.tar.xz
