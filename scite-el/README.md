# scite upstream
https://src.fedoraproject.org/rpms/scite/tree/f29

# Reason for being in stackrpms
Fedora and CentOS do not package scite and I have use of them. CentOS 7 relies on version 3 of the application, but CentOS 8 and Fedora relies on version 4.

# Reverse dependency matrix
Distro     | scite version
---------- | ---------------
CentOS 6   | 3.7.6
CentOS 7   | 3.7.6
CentOS 8   | 4.4.3
Fedora 30  | 4.4.3

# Differences
## From upstream
See file [stackrpms-scite.spec.diff](stackrpms-scite.spec.diff)
## Between scite-fedora and scite-el
Run file [fc-el-scite-diff.sh](fc-el-scite-diff.sh).

    scite-fedora/fc-el-scite-diff.sh | vi -
