# el is on 3.x, fc is on 4.x
%global pkgversion 3.7.6
%define tarballversion %( echo %pkgversion | tr -d '.' )

%define scl_env %{nil}
%define scl_buildreq coreutils
%if 0%{?el6}
   %define scl_env devtoolset-7
   %define scl_buildreq devtoolset-7-toolchain
%endif

Name: scite
Version: %{pkgversion}
Release: 1%{?dist}
Summary: SCIntilla-based GTK text editor
License: MIT
Group: Applications/Editors
Url: http://www.scintilla.org/SciTE.html
Packager: B Stack <bgstack15@gmail.com>

Source0: https://www.scintilla.org/scite%{tarballversion}.tgz
Patch0: scite-utf8.patch

BuildRequires: desktop-file-utils
BuildRequires: gcc-c++
BuildRequires: %{scl_buildreq}
%if !0%{?el6}
BuildRequires: gtk3-devel
%endif
BuildRequires: gtk2-devel

Provides: bundled(scintilla) = %{version}

%description
SciTE is a SCIntilla based Text Editor. Originally built to demonstrate
Scintilla, it has grown to be a generally useful editor with facilities for
building and running programs.

%prep
%setup -q -c
%patch0
#remove bundled lua
rm -rf scite/lua

%build

%if 0%{?el6}
   %if "%{?scl_env}" != ""
      scl enable %{scl_env} /bin/bash <<'EOFSCL'
   %endif
   make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags}" \
      CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" DEBUG=1 -C scintilla/gtk
   #Build without lua support
   make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags}" \
      CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" DEBUG=1 NO_LUA=1 -C scite/gtk
   %if "%{?scl_env}" != ""
EOFSCL
   %endif
%else
   make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags}" \
      CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" GTK3=1 DEBUG=1 -C scintilla/gtk
   #Build without lua support
   make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags}" \
      CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" GTK3=1 DEBUG=1 NO_LUA=1 -C scite/gtk
%endif

%install
make DESTDIR=%{buildroot} -C scite/gtk install
ln -s SciTE %{buildroot}%{_bindir}/scite

# include man-page
install -D -p -m 644 scite/doc/scite.1 %{buildroot}%{_mandir}/man1/scite.1

desktop-file-install --delete-original  \
  --dir %{buildroot}%{_datadir}/applications            \
  --remove-category Application                         \
  --remove-key Encoding					\
  %{buildroot}%{_datadir}/applications/SciTE.desktop

%if 0%{?fedora}
%posttrans
/usr/bin/update-desktop-database &> /dev/null || :
%else
%post
/usr/bin/update-desktop-database &> /dev/null || :

%postun
/usr/bin/update-desktop-database &> /dev/null || :
%endif

%files
%doc scite/License.txt
%{_mandir}/man1/scite.1*
%{_bindir}/SciTE
%{_bindir}/scite
%{_datadir}/scite/
%{_datadir}/pixmaps/*
%{_datadir}/applications/*

%changelog
* Wed Jun 03 2020 B Stack <bgstack15@gmail.com> - 4.4.3-1/3.7.6-1
- Update version

* Tue May 05 2020 B Stack <bgstack15@gmail.com> - 4.3.3-1/3.7.6-1
- Update version

* Wed Mar 04 2020 B Stack <bgstack15@gmail.com> - 4.3.1-1/3.7.6-1
- Update version

* Thu Jan 16 2020 B Stack <bgstack15@gmail.com> - 4.3.0-1/3.7.6-1
- Update version

* Thu Dec 19 2019 B Stack <bgstack15@gmail.com> - 4.2.3-1/3.7.6-1
- Update version

* Mon Nov 11 2019 B Stack <bgstack15@gmail.com> - 4.2.2-1/3.7.6-1
- Update version

* Mon Nov 11 2019 B Stack <bgstack15@gmail.com> - 4.2.1-1/3.7.6-1
- Rewritten to build Fedora and Enterprise Linux versions from a single spec with minor flags changed

* Thu Jun  6 2019 B Stack <bgstack15@gmail.com> - 4.1.6-1/3.7.5-1
- Rewritten to build Fedora and Enterprise Linux versions from a single spec with minor flags changed

* Mon May  6 2019 B Stack <bgstack15@gmail.com> - 4.1.5-1
- Brought to latest version

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.7.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.7.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Aug 12 2017 Johan Swensson <kupo@kupo.se> 3.7.5-1
- Update to 3.7.5

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.7-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Sep 27 2016 Johan Swensson <kupo@kupo.se> - 3.6.7-1
- Update to 3.6.7

* Mon Feb 22 2016 Johan Swensson <kupo@kupo.se> - 3.6.3-1
- Update to 3.6.3

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Johan Swensson <kupo@kupo.se> - 3.6.2-1
- Update to 3.6.2

* Fri Sep 18 2015 Johan Swensson <kupo@kupo.se> - 3.6.1-1
- Update to 3.6.1

* Sun Aug 23 2015 Johan Swensson <kupo@kupo.se> - 3.6.0-1
- Update to 3.6.0

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.5.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed May 06 2015 Johan Swensson <kupo@kupo.se> - 3.5.5-2
- Added bundled(scintilla)

* Thu Apr 30 2015 Johan Swensson <kupo@kupo.se> - 3.5.5-1
- Update to 3.5.5

* Sat Feb 21 2015 Johan Swensson <kupo@kupo.se> - 3.5.3-1
- Update to 3.5.3

* Sat Dec 20 2014 Johan Swensson <kupo@kupo.se> - 3.5.2-1
- Update to 3.5.2

* Thu Oct 16 2014 Johan Swensson <kupo@kupo.se> - 3.5.1-1
- Update to 3.5.1

* Mon Aug 18 2014 Johan Swensson <kupo@kupo.se> - 3.5.0-1
- Update to 3.5.0

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Jul 24 2014 Johan Swensson <kupo@kupo.se> - 3.4.4-1
- update to 3.4.4
- remove bundled lua library
- build without lua scripting support for now
- use --remove-key instead of sed
- add update-desktop-database scriptlets
- remove unnecessary README file
- add support for el6
- fix build conditional
- fixed BuildRequires conditional
- fix LDFLAGS macro
- remove deprecated key in desktop file
- use GTK3 (el7 and fedora)
- drop vendor from desktop-file-install
- use correct LDFLAGS and CXXFLAGS
- install man page correctly
- drop no longer needed install step and defattr

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.22-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.22-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.22-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Dec 4 2010 Jorge Torres <jtorresh@gmail.com> 2.22-1
- Update to 2.22

* Sat Apr 17 2010 Jorge Torres <jtorresh@gmail.com> 2.11-1
- Update to 2.11
- Fix bug #564689

* Thu Oct 1 2009 Jorge Torres <jtorresh@gmail.com> 2.01-1
- Update to 2.01

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.77-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Apr 4 2009 Jorge Torres <jtorresh@gmail.com> 1.77-1
- Upgrade to 1.77

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.74-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.74-3
- Autorebuild for GCC 4.3

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.74-2
- Rebuild for selinux ppc32 issue.

* Wed Jun 20 2007 Jorge Torres <jtorresh@gmail.com> 1.74-1
- Upgrade to 1.74.
- Default to UTF-8 encoding (fixes bug #240558).
- Remove iconv workaround for desktop file encoding.

* Fri Sep 01 2006 Jorge Torres <jtorresh@gmail.com> 1.71-1
- Upgrade to 1.71
- Use 'iconv' to work around a desktop file encoding issue

* Thu Jun 22 2006 Jorge Torres <jtorresh@gmail.com> 1.70-1
- Upgrade to 1.70
- Drop man-page capitalization patch

* Sun Jun 04 2006 Jorge Torres <jtorresh@gmail.com> 1.69-3
- Remove usage of MultipleArgs in desktop file.

* Fri Jun 02 2006 Jorge Torres <jtorresh@gmail.com> 1.69-2
- Changed license to MIT.
- Added patches to support compiling with %%{optflags}.
- Misc fixes.

* Thu Jun 01 2006 Jorge Torres <jtorresh@gmail.com> 1.69-1
- Initial package version.
