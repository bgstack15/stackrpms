# 7w upstream
https://gitlab.com/bgstack15/7w forked from https://github.com/NathanRVance/7w

# Reason for being in stackrpms
This is an original package of the C/ncurses implementation of 7 Wonders.

# Reverse dependency matrix
Distro     | 7w version
---------- | ---------------
All        | 0.0.1

# Differences from upstream
None
