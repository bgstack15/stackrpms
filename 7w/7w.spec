%global     use_master 1
%if 0%{use_master}
%global vername master
%else
%global vername %{version}
%endif

# turn off debug package
%define debug_package %{nil}

Name:       7w
Version:    0.0.1
Release:    1%{?dist}
Summary:    Ncurses-based implementation of 7 Wonders

Group:      Games/Boardgame
License:    GPL2
#OriginalURL: https://github.com/NathanRVance/7w
URL:        https://gitlab.com/bgstack15/%{name}
Source0:    %{URL}/-/archive/%{vername}/%{name}-%{vername}.tar.gz
Source1:    %{name}.svg

Packager:   B Stack <bgstack15@gmail.com>
BuildRequires: gcc
BuildRequires: ncurses-devel
BuildRequires: ImageMagick
Requires:   ncurses
# do not require the icon theme. the rpm can make the dirs, and it won't hurt on terminal-only systems to have the directories with just these icons.
#Requires:      hicolor-icon-theme

%description
7 Wonders is a drafting card game by Antoine Bauza and adapted for ncurses-based gameplay by Nathan Vance. Other works by the computer adaptation author include cettlers, an ncurses adapation of Settlers of Catan.

%prep
#%%setup -n %%{name}-%%{vername}
tar -zxf %{SOURCE0}
cp -p %{SOURCE1} .

%build
cd %{name}-*
cd Release
%make_build

%install
pushd %{name}-*
#%%make_install
# upstream has no install instructions
install -d %{buildroot}%{_licensedir}/%{name} %{buildroot}%{_bindir}
install -m0644 LICENSE %{buildroot}%{_licensedir}/%{name}/
install -m0755 Release/%{name} %{buildroot}%{_bindir}/

# desktop files
install -d %{buildroot}%{_datadir}/applications
cat <<EOF > %{buildroot}%{_datadir}/applications/%{name}.desktop
#!/usr/bin/xdg-open
[Desktop Entry]
Name=7 Wonders
Exec=%{_bindir}/%{name}
Type=Application
StartupNotify=true
Path=%{_bindir}
Icon=%{name}
StartupWMClass=_7wonders
Comment=Drafting card game
Terminal=true
Categories=Game;BoardGame;
EOF

# application start script
# none

# man pages
# none

# mimetypes
# none

# icons
popd
for res in 16 22 24 32 48 64 96 128 256 ;do
  dir=%{buildroot}%{_datadir}/icons/hicolor/${res}x${res}/apps
  mkdir -p ${dir}
  for icon in %{name} ; do
    convert ${icon}.svg -filter Lanczos -resize ${res}x${res}  \
      ${dir}/${icon}.png
  done
done
scalable_dir="%{buildroot}%{_datadir}/icons/hicolor/scalable/apps"
install -d "${scalable_dir}"
install -m0644 "%{name}.svg" "${scalable_dir}"

%clean
%{__rm} -rf %{buildroot} || :

%post
touch --no-create %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :
update-desktop-database 1>/dev/null 2>&1 || :

%preun
# is it a final removal?
#if test "$1" = "0" ;
#then
#fi

%postun
update-desktop-database &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
#%license %attr(444, -, -) NONE.txt
#%doc %attr(444, -, -) NONE.txt
%license %attr(444, -, -) %{_licensedir}/*
#%doc %attr(444, -, -) %%{_docdir}/*/*
%{_bindir}/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/*
#%%{_datadir}/%%{name}

%changelog
* Fri Apr  5 2019 B Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial rpm built
