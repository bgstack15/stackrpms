# Lightdm-webkit2-greeter upstream
https://github.com/berlam/debian-lightdm-webkit-greeter
https://launchpad.net/lightdm-webkit-greeter/trunk/2.0.0/+download/lightdm-webkit-greeter-2.0.0.tar.gz

# Reason for being in stackrpms
The Chicago95 package provides a lightdm-webkit2-greeter theme as part of the total conversion experience turning Xfce into Windows 95. No Devuan package exists. The distro that developed lightdm-webkit-greeter no longer exists.

# Reverse dependency matrix
Distro       | Chicago95 version | Lightdm-webkit2-greeter version
------------ | ----------------- | -------------------------------
Devuan ceres | 0.0.1             | 2.2.5

# Differences from upstream
None
