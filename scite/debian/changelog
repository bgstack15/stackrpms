scite (1:5.5.4-100+stackrpms) unstable; urgency=medium

  * Add stackrpms lua script and customized global.properties
  * Use lua 5.4.4
  * Add epoch so stackrpms scite always has precedence over base package

 -- B. Stack <bgstack15@gmail.com>  Tue, 31 Dec 2024 10:05:14 -0500

scite (5.5.4-1) unstable; urgency=medium

  * New upstream version 5.5.4
  * Add salsa-ci.yml

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 18 Dec 2024 00:06:09 +0100

scite (5.5.3-1) unstable; urgency=medium

  * New upstream version 5.5.3
  * Add more files to filter out
  * Set filter-pristine-tar to active

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 20 Oct 2024 14:49:32 +0200

scite (5.5.2-1) unstable; urgency=medium

  * New upstream version 5.5.2

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 21 Aug 2024 13:32:02 +0200

scite (5.5.1-1) unstable; urgency=medium

  * New upstream version 5.5.1

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 22 Jul 2024 11:40:58 +0200

scite (5.5.0-1) unstable; urgency=medium

  * New upstream version 5.5.0
  * Add another lintian source override for html source is missing
  * Upgrade Standards Version to 4.7.0 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 23 Apr 2024 14:34:26 +0200

scite (5.4.3-1) unstable; urgency=medium

  * New upstream version 5.4.3
  * Update copyright year to 2024

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 15 Mar 2024 12:59:41 +0100

scite (5.4.1-1) unstable; urgency=medium

  * New upstream release.
  * Update the d/copyright file.
  * debian/patches:
    - Refresh all patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 28 Dec 2023 09:13:01 +0000

scite (5.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    - Refresh and renumber all patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 26 Nov 2023 07:32:58 +0000

scite (5.3.9-1) unstable; urgency=medium

  * New upstream version 5.3.9

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 05 Nov 2023 01:29:57 +0100

scite (5.3.8-1) unstable; urgency=medium

  * New upstream version 5.3.8
  * Add d/conffiles file to remove obsolete conffile
    /etc/scite/nimrod.properties properly (Closes: #1052403)

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 23 Sep 2023 02:09:13 +0200

scite (5.3.7-1) unstable; urgency=medium

  * New upstream version 5.3.7
  * Add lintian override for another false positive spelling error
    (ment/meant)
  * Add Upstream Contact field to d/copyright

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 06 Sep 2023 15:49:56 +0200

scite (5.3.6-1) unstable; urgency=medium

  * Upload to unstable

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 11 Jun 2023 20:21:47 +0200

scite (5.3.6-1~exp1) experimental; urgency=medium

  * New upstream version 5.3.6

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 31 May 2023 18:35:52 +0200

scite (5.3.5-1~exp1) experimental; urgency=medium

  * New upstream version 5.3.5

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 13 Mar 2023 12:39:51 +0100

scite (5.3.4-1~exp1) experimental; urgency=medium

  * Add scintillaVersion.txt.orig to files to filter
  * New upstream version 5.3.4

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 11 Mar 2023 00:28:35 +0100

scite (5.3.3-1) unstable; urgency=medium

  * New upstream version 5.3.3
  * Update copyright year for debian packaging
  * Upgrade Standards Version to 4.6.2 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 08 Feb 2023 00:11:51 +0100

scite (5.3.2-1) unstable; urgency=medium

  * New upstream version 5.3.2
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 07 Dec 2022 14:10:05 +0100

scite (5.3.1-1) unstable; urgency=medium

  * New upstream version 5.3.1

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 12 Oct 2022 15:27:24 +0200

scite (5.3.0-1) unstable; urgency=medium

  [ Antonio Valentino ]
  * Fix d/copyright formatting.

  [ Andreas Rönnquist ]
  * New upstream version 5.3.0
  * Remove lintian override for afe/safe - not needed

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 27 Aug 2022 13:55:18 +0200

scite (5.2.4-1) unstable; urgency=medium

  * New upstream version 5.2.4
  * Upgrade Standards Version to 4.6.1 (No changes required)
  * Make sure in clean targets that we use GTK3 flag to avoid warnings
  * Add lintian source override for source-is-missing tags
  * Use square brackets in lintian override
  * Fix spelling-error-in-binary override order
  * Update copyright years for Debian packaging

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 10 Jul 2022 19:18:17 +0200

scite (5.2.3-1) unstable; urgency=medium

  * New upstream version 5.2.3
    - Fixes scrolllbars not being updated properly (Closes: #857540)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 22 May 2022 12:49:35 +0200

scite (5.2.2-1) unstable; urgency=medium

  * New upstream version 5.2.2

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 31 Mar 2022 14:42:20 +0200

scite (5.2.1-1) unstable; urgency=medium

  * New upstream version 5.2.1
  * Remove patch 0005-Fix-pixmap-leak-on-GTK.patch - applied upstream

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 24 Feb 2022 10:27:44 +0100

scite (5.2.0-2) unstable; urgency=medium

  * Add patch to fix pixmap leak (Closes: #1005965)

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 22 Feb 2022 13:07:01 +0100

scite (5.2.0-1) unstable; urgency=medium

  * New upstream version 5.2.0
  * Filter out scintilla/tgzsrc.orig
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 09 Feb 2022 14:45:58 +0100

scite (5.1.6-1) unstable; urgency=medium

  * New upstream version 5.1.6
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 10 Dec 2021 14:52:42 +0100

scite (5.1.5-1) unstable; urgency=medium

  * New upstream version 5.1.5

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 08 Nov 2021 18:11:31 +0100

scite (5.1.3-1) unstable; urgency=medium

  * New upstream version 5.1.3
  * Refresh patches
  * Upgrade Standards-Version to 4.6.0.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 23 Sep 2021 20:26:06 +0200

scite (5.1.1-1) unstable; urgency=medium

  * Upload to unstable

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 18 Aug 2021 18:52:51 +0200

scite (5.1.1-1~exp1) experimental; urgency=medium

  * New upstream version 5.1.1

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 26 Jul 2021 15:21:59 +0200

scite (5.1.0-1~exp1) experimental; urgency=medium

  * New upstream version 5.1.0

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 23 Jun 2021 14:50:53 +0200

scite (5.0.3-1~exp1) experimental; urgency=medium

  * New upstream version 5.0.3
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 08 Jun 2021 13:30:50 +0200

scite (5.0.2-1~exp1) experimental; urgency=medium

  * New upstream version 5.0.2
  * Install examples

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 08 May 2021 15:35:20 +0200

scite (5.0.1-1~exp1) experimental; urgency=medium

  * New upstream version 5.0.1

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 12 Apr 2021 15:33:09 +0200

scite (5.0.0-1~exp1) experimental; urgency=medium

  * New upstream version 5.0.0
  * Lexilla has been moved to own folder - Update build accordingly
  * Update copyright with info for lexilla
  * Update Standards-Version to 4.5.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 09 Mar 2021 15:51:26 +0100

scite (4.4.5-2) unstable; urgency=medium

  * Remove menu file and XPM image - thanks to Pino Toscano
    (Closes: #970139)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 13 Sep 2020 11:26:58 +0200

scite (4.4.5-1) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * New upstream version 4.4.5
  * Remove patches 0001-hardening_flags.patch and 0006-cross.patch,
    applied upstream
  * Refresh patch 0007-Build-with-Debian-packaged-Lua.patch
  * Remove patch 0004-spelling.patch
  * Update lintian override for spelling error Lables/Labels
  * Override lintian warning national-encoding in three places
  * Move documentation to /usr/share/doc/scite and link to /usr/share/scite
  * Override dh_compress to avoid compressing xpm image file
  * Add a doc-base file
  * Upgrade to Debhelper 13
  * Add Rules-Requires-Root: no

  [ Antonio Valentino ]
  * Fix BUILD_DATE

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 12 Sep 2020 16:03:12 +0200

scite (4.4.4-1) unstable; urgency=medium

  * New upstream version 4.4.4
  * Refresh patches
  * Update lintian override spelling-error-in-binary moved to
    /usr/lib/scite/liblexilla.so
  * Add forwarded not-needed to
    0007-Build-with-Debian-packaged-Lua.patch
  * Add Forwarded fields to 0006-cross.patch
  * Clean up liblexilla and more files with clean

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 21 Jul 2020 20:50:44 +0200

scite (4.3.3-2) unstable; urgency=medium

  * Remove removed files from debian/copyright

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 29 Apr 2020 16:04:11 +0200

scite (4.3.3-1) unstable; urgency=medium

  * New upstream version 4.3.3
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 27 Apr 2020 04:06:52 +0200

scite (4.3.1-1) unstable; urgency=medium

  * New upstream version 4.3.1
  * Upgrade to Standards Version 4.5.0 (No changes required)
  * Remove unused lintian override source-is-missing for
    scintilla/bin/lexilla.so

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 05 Mar 2020 02:53:01 +0100

scite (4.3.0-1) unstable; urgency=medium

  * New upstream version 4.3.0
  * Refresh patches
  * Add lintian override for source-is-missing lexilla.so

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 17 Jan 2020 23:36:24 +0100

scite (4.2.3-1) unstable; urgency=medium

  * Make sure an import merge to debian/master
  * New upstream version 4.2.3

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 11 Dec 2019 13:09:14 +0100

scite (4.2.2-1) unstable; urgency=medium

  * New upstream version 4.2.2

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 11 Dec 2019 12:08:26 +0100

scite (4.2.1-1) unstable; urgency=medium

  * New upstream version 4.2.1
  * Upgrade to Standards Version 4.4.1 (No changes required)
  * Add lintian override for spelling afe/safe not present in source

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 25 Oct 2019 01:48:18 +0200

scite (4.2.0-2) unstable; urgency=medium

  * Use secure URI in Homepage field.
  * Update standards version, no changes needed.
  * Set upstream metadata fields: Archive.
  * Remove obsolete fields Name from debian/upstream/metadata.
  * Fix day-of-week for changelog entries 1.36-1, 1.35-1, 1.34-2.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 05 Sep 2019 17:22:38 +0000

scite (4.2.0-1) unstable; urgency=medium

  * New upstream version 4.2.0
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 08 Jul 2019 14:49:06 +0200

scite (4.1.7-1~exp1) experimental; urgency=medium

  * New upstream version 4.1.7

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 14 Jun 2019 12:25:34 +0200

scite (4.1.6-1~exp1) experimental; urgency=medium

  * New upstream version 4.1.6
  * Refresh patches

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 10 Jun 2019 14:38:59 +0200

scite (4.1.5-1~exp1) experimental; urgency=medium

  * New upstream version 4.1.5
  * Upgrade to compat 12
  * Refresh patches
  * Remove unused lintian override for spelling error

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 17 Apr 2019 19:10:13 +0200

scite (4.1.3-1) unstable; urgency=medium

  * New upstream version 4.1.3
  * Update Standards-Version to 4.3.0 (No changes required)
  * Refresh patches
  * Add lintian override for spelling error 'ment'

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 10 Jan 2019 14:08:25 +0100

scite (4.1.2-1) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * New upstream version 4.1.2
    - Fixes povray syntax on Linux (Closes: #587229)

  [ Ondřej Nový ]
  * d/watch: Use https protocol

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 02 Oct 2018 14:50:26 +0200

scite (4.1.1-1) unstable; urgency=medium

  * New upstream version 4.1.1
  * Update standards version to 4.2.1, no changes required
  * Remove empty tests (Closes: #907597)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 09 Sep 2018 13:35:49 +0200

scite (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0
  * Build with Debian packaged Lua 5.3

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 19 Jun 2018 23:02:30 +0200

scite (4.0.5-2) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: (Closes: #900016)
    + Let dh_auto_build pass cross tools to make.
    + cross.patch: make pkg-config subtsitutable.

  [ Antonio Valentino ]
  * Refresh all patches.

  [ Andreas Rönnquist ]
  * Use DEP-14 branch naming (debian/master and upstream/latest)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 24 May 2018 19:41:57 +0200

scite (4.0.5-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * debian/control
    - improve package description
    - Add myself to uploaders
  * debian/watch
    - Remove checking for zip

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 11 May 2018 14:21:56 +0200

scite (4.0.4-1) unstable; urgency=medium

  * New upstream release.
  * Standard version bumped to 4.0.4 (no change).
  * Set compat to 11.
  * Switch to salsa and git-buildpackage, update Vcs-* fields.
  * Update the watch file.
  * debian/patches
    - refresh and renumber all patches
    - new spelling.patch
    - new patch to add the "Keywords" entry to the desktop file
  * debian/rules
    - do not parse the debian changelog
    - enable all hardening flags
  * Install a link for the SciTE.1 man page.
  * Add autopkgtest to test installability.
  * Move images into the debian/images folder.
  * Install scintilla doc (new debian/install file)
  * Do not use remote resources in html files.
  * Drop debian/missing-sources: no longer necessary.
  * Add a gbp.conf file in the debian directory.
  * Add upstream metadata.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 15 Apr 2018 07:24:39 +0000

scite (4.0.0-1) unstable; urgency=medium

  * New upstream release
  * Standard version bumped to 4.0.0 (no change)
  * debian/patches
    - drop desktop_file.patch (no longer necessary)
    - refresh remaining patches

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 16 Aug 2017 17:11:14 +0000

scite (3.7.2-1) unstable; urgency=medium

  * New upstream release
  * Refresh all patches

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 06 Jan 2017 13:21:06 +0000

scite (3.7.0-1) unstable; urgency=medium

  * New upstream release
  * debian/patches
    - refresh all patches
    - fix cme warnings
  * debian/copyright
    - fix format URL

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 31 Oct 2016 16:18:30 +0000

scite (3.6.6-1) unstable; urgency=medium

  * New upstream release
  * Refresh all patches
  * debian/rules
    - update clean target (do not remove orig files bundled in the
      orig archive)
  * debian/copyright
    - disambiguate license for lua (use expat)
  * debian/control
    - standard version bump to 3.9.8
    - refromatting (with the cme tool)
    - fix VCS URI

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 02 Jun 2016 18:38:09 +0000

scite (3.6.0-1) unstable; urgency=medium

  * New upstream release
  * Refresh all patches
  * Standards version bumped to 3.9.6 (no change)
  * Implemented reproducible build

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 17 Aug 2015 12:20:12 +0200

scite (3.5.0-1) unstable; urgency=medium

  * New upstream release

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 14 Aug 2014 17:12:34 +0000

scite (3.4.2-1) unstable; urgency=medium

  * New uptream release
  * All patches have been refreshed
  * debian/copyright
    - updated copyright entries
    - copyright file converted in DEP-5 format

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 24 May 2014 21:39:26 +0000

scite (3.4.1-1) unstable; urgency=low

  * New upstream release
  * Standard version bumped to 3.9.5 (no change)
  * debian/patch
    - drop link_libdl.patch (applied upstream)
    - refresh all remainimg patches
    - new patch (do_not_use_remote_logo.patch) to avoid the
      "privacy-breach-logo" error detected by lintian
  * Added missing source (debian/missing-sources/scintilla/include/Face.py)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 03 Apr 2014 20:11:51 +0000

scite (3.3.5-1) unstable; urgency=low

  * new upstream release
  * debian/control:
    - provide the editor virtual package (closes: #398753)
    - move the Homepage field to the source section (silence cme warnings)
  * debian/patches:
    - refresh all patches
    - new patch for linking libdl.so even on kfreensd-* and hurd
      (closes: #722260). Thanks to Pino Toscano.
  * debian/rules:
    - avoid to remove "scintilla/src/Editor.cxx.orig" that is in the orig
      archive (override_dh_clean)
    - small refinements to the override_dh_auto_clean target

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 31 Aug 2013 09:26:55 +0000

scite (3.3.4-1) unstable; urgency=low

  * new upstream release
  * standard version bumped to 3.9.4 (no changes)
  * drop disable_background_save.patch (applied upstream)
  * use canonical vcs filed in the control file
  * compat have been set to 9 (automatically enables hardening flags)
  * new patch (hardening_flags.patch) that ensures that hardening flags are
    actually used in the makefiles
  * new patch for fixing the desktop file (removed deprecated entries and
    values, added missing Keywords entry)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 20 Aug 2013 19:34:22 +0200

scite (3.3.1-1) unstable; urgency=low

  * new upstream release

 -- Michael Vogt <mvo@debian.org>  Wed, 08 May 2013 21:37:16 +0200

scite (3.2.4-1) experimental; urgency=low

  * new upstream release

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 17 Jan 2013 21:13:29 +0100

scite (3.2.3-1) experimental; urgency=low

  * new upstream release (closes: #683743)
  * updated watch file
  * standard version bumped to 3.9.3
  * buld against GTK3 instead of GTK2 (closes: #654959)
  * use "gnomeprefix" instead of "prefix" in override_dh_auto_install
    in oreder to allow correct installation of the desktop file
  * install scite manpage
  * drop disable_background_save.patch (applied upstream)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 24 Oct 2012 19:54:43 +0200

scite (3.0.2-3) unstable; urgency=low

  [ Michael Vogt ]
  * add missing man-page (closes: #690429)

  [ Antonio Valentino ]
  * install global configuration files under /etc (closes: #119604)
  * disable background save in order to avoid save failures and data
    loss (closes: #682320)
  * Antonio Valentino added to uploaders

 -- Michael Vogt <mvo@debian.org>  Wed, 24 Oct 2012 17:02:07 +0200

scite (3.0.2-2) unstable; urgency=low

  * debian/scite.desktop, debian/Scite32M.xpm:
    - dropped, use upstream version instead (closes: #653200)

 -- Michael Vogt <mvo@debian.org>  Tue, 03 Jan 2012 18:47:49 +0100

scite (3.0.2-1) unstable; urgency=low

  * new upstream release (closes: #618346)
  * move to debhelper 8, drop dpatch
  * drop custom desktop file and config customization patches

 -- Michael Vogt <mvo@debian.org>  Mon, 19 Dec 2011 09:00:56 +0100

scite (2.25-1) unstable; urgency=low

  * new upstream release #618346

 -- Michael Vogt <mvo@debian.org>  Thu, 14 Apr 2011 12:05:44 +0200

scite (2.03-1) unstable; urgency=low

  * new upstream version (closes: #570096)
  * debian/patches/02_config.dpatch:
    - refreshed
  * debian/patches/03_nolua_ftbfs_fix.dpatch:
   - dropped, no longer needed

 -- Michael Vogt <mvo@debian.org>  Tue, 16 Feb 2010 21:08:57 +0100

scite (1.76-1) unstable; urgency=low

  * New upstream release
  * ack NMU (thanks!), closes: 476850

 -- Michael Vogt <mvo@debian.org>  Fri, 25 Apr 2008 16:46:19 +0200

scite (1.75-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply patch from Ubuntu by Michael Bienia to fix FTBFS on the platforms
    where Lua support is disabled (like amd64). (Closes: #456351)

 -- Paul Wise <pabs@debian.org>  Sat, 19 Apr 2008 19:56:40 +0800

scite (1.75-1) unstable; urgency=low

  * New upstream release (closes: #410654, #446411)

 -- Michael Vogt <mvo@debian.org>  Mon, 10 Dec 2007 11:46:50 +0100

scite (1.71-1) unstable; urgency=low

  * New upstream release (closes: #380090)

 -- Michael Vogt <mvo@debian.org>  Sun,  3 Sep 2006 10:34:09 +0200

scite (1.69-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Fri,  2 Jun 2006 09:24:12 +0200

scite (1.68-2) unstable; urgency=low

  * Drop DH_COMPAT, we use debian/compat now
  * Upgrade debhelper build-dependency
  * Include ScintillaHistory.html as the package changelog (closes: #237682)
  * Update copyright information
  * Add homepage to package description
  * Bump Standards-Version (no changes)
  [Thanks to Paul Wise for the patch]

 -- Michael Vogt <mvo@debian.org>  Tue, 23 May 2006 20:43:36 +0800

scite (1.68-1) unstable; urgency=low

  * New upstream release (closes: #360305), the
    rebuild closes: #355404 as well
  * added watch file and updated copyright download url
    (thanks to Paul Wise)
  * debian/scite.desktop: changed to "exec=scite %F"

 -- Michael Vogt <mvo@debian.org>  Sun,  2 Apr 2006 10:54:26 +0200

scite (1.67-1) unstable; urgency=low

  * New upstream release
  * debian/patches/02_config:
    - redone, didn't applied cleanly

 -- Michael Vogt <mvo@debian.org>  Sat, 17 Dec 2005 12:36:14 +0100

scite (1.66-1) unstable; urgency=low

  * New upstream release
  * removed debian/patches/03_new_pango,04_amd64, both are now
    upstream

 -- Michael Vogt <mvo@debian.org>  Wed,  7 Sep 2005 10:46:57 +0200

scite (1.64-2) unstable; urgency=low

  * debian/patches/03_new_pango:
    - support for the new pango (closes: #320975,#323634)
  * debian/patches/04_amd65
    - make it build on amd64 (closes: #296996)

 -- Michael Vogt <mvo@debian.org>  Fri, 26 Aug 2005 12:24:31 +0200

scite (1.64-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Mon,  6 Jun 2005 09:56:52 +0200

scite (1.63-3) unstable; urgency=low

  * build-depends on dpatch now

 -- Michael Vogt <mvo@debian.org>  Fri,  6 May 2005 12:50:14 +0200

scite (1.63-2) unstable; urgency=low

  * use dpatch now
  * remove a bad .desktop file

 -- Michael Vogt <mvo@debian.org>  Fri,  6 May 2005 12:31:02 +0200

scite (1.63-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Tue, 12 Apr 2005 10:33:57 +0200

scite (1.62-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Wed, 10 Nov 2004 10:19:38 +0100

scite (1.61-2) unstable; urgency=low

  * added desktop file (thanks to  Teofilo Ruiz Suarez)
    (closes: #266344)

 -- Michael Vogt <mvo@debian.org>  Wed, 18 Aug 2004 07:51:16 +0200

scite (1.61-1) unstable; urgency=low

  * New upstream release
  * Disable Lua extensions new in 1.60 on 64bit platforms.
    Code is just broken (Closes: #249844)
    (thans to Frank Lichtenheld)
  * new scite supports drag'n'drop for selected text again
    (closes: #259709)

 -- Michael Vogt <mvo@debian.org>  Mon,  2 Aug 2004 11:39:24 +0200

scite (1.60-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Mon, 10 May 2004 16:08:12 +0200

scite (1.59-2) unstable; urgency=low

  * added latex mode (closes: #241230)

 -- Michael Vogt <mvo@debian.org>  Fri,  2 Apr 2004 17:21:41 +0200

scite (1.59-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Sun, 29 Feb 2004 18:14:38 +0100

scite (1.58-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Wed, 14 Jan 2004 14:08:58 +0100

scite (1.57-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Sun, 30 Nov 2003 20:47:42 +0100

scite (1.55-1) unstable; urgency=low

  * New upstream release
  * should autobuild again (closes: #206543)
  * typo in description fixed (closes:  #211500)
  * changed "netscape" to "x-www-browser" for help invocation
    (closes: #204072)

 -- Michael Vogt <mvo@debian.org>  Sat, 27 Sep 2003 12:59:49 +0200

scite (1.54-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Tue, 19 Aug 2003 11:52:47 +0200

scite (1.53-2) unstable; urgency=low

  * updated build-depends to libgtk2.0-dev (closes: #193603)

 -- Michael Vogt <mvo@debian.org>  Sat, 17 May 2003 08:58:10 +0200

scite (1.53-1) unstable; urgency=low

  * New upstream release
  * build with gtk2 now (closes: #193458)

 -- Michael Vogt <mvo@debian.org>  Fri, 16 May 2003 11:14:45 +0200

scite (1.51-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Sun, 23 Feb 2003 00:19:03 +0100

scite (1.49-1) unstable; urgency=low

  * New upstream release

 -- Michael Vogt <mvo@debian.org>  Tue,  5 Nov 2002 23:51:39 +0100

scite (1.48-2) unstable; urgency=low

  * fixed a upstream g++-3.1 build failure (closes:  #161361)

 -- Michael Vogt <mvo@debian.org>  Thu, 19 Sep 2002 12:22:25 +0200

scite (1.48-1) unstable; urgency=low

  * new upstream version (closes: #160586)
  * new debian maintainer (closes: #159616)

 -- Michael Vogt <mvo@debian.org>  Wed, 18 Sep 2002 01:19:25 +0200

scite (1.46-1) unstable; urgency=low

  * New upstream release
  * Fixed broken toolbar (Closes: #115903)
  * Rebuilt to /usr (Closes: #144728)
  * Moved more of the Debian specific stuff into 'rules' rather
    than modifying the makefiles.

 -- Aubin Paul <aubin@debian.org>  Thu, 20 Jun 2002 19:07:05 -0400

scite (1.44-1) unstable; urgency=low

  * New upstream Release

 -- Aubin Paul <aubin@punknews.org>  Wed, 13 Feb 2002 21:18:07 -0500

scite (1.41-1) unstable; urgency=low

  * New upstream Release

 -- Aubin Paul <aubin@punknews.org>  Tue,  6 Nov 2001 13:12:27 -0500

scite (1.40-1) unstable; urgency=low

  * New upstream Release

 -- Aubin Paul <aubin@punknews.org>  Fri, 26 Oct 2001 15:02:18 -0400

scite (1.39-2) unstable; urgency=low

  * Made some changes to comply with updated policy.

 -- Aubin Paul <aubin@punknews.org>  Tue, 18 Sep 2001 16:30:33 -0400

scite (1.39-1) unstable; urgency=low

  * New Upstream Release

 -- Aubin Paul <aubin@punknews.org>  Tue, 18 Sep 2001 13:43:39 -0400

scite (1.38-3) unstable; urgency=low

  * Rebuilt to fix small packaging bug.

 -- Aubin Paul <aubin@punknews.org>  Fri, 27 Jul 2001 09:59:25 -0400

scite (1.38-2) unstable; urgency=low

  * Small bug fix to allow compilation with GCC-3.0 and GCC-2.9x (Closes: #104813)

 -- Aubin Paul <aubin@punknews.org>  Tue, 24 Jul 2001 15:05:09 -0400

scite (1.38-1) unstable; urgency=low

  * New upstream release

 -- Aubin Paul <aubin@punknews.org>  Thu,  5 Jul 2001 14:00:16 -0400

scite (1.37-1) unstable; urgency=low

  * Additional fixes for GTK+ > 1.2.9

 -- Aubin Paul <aubin@debian.org>  Mon, 16 Apr 2001 19:22:23 -0400

scite (1.36-20010325cvs-1) unstable; urgency=low

  * Fixes for GTK-1.2.9 issues from CVS (Closes: #90632)

 -- Aubin Paul <aubin@debian.org>  Mon, 16 Apr 2001 19:22:23 -0400

scite (1.36-2) unstable; urgency=low

  * Additional 64-bit Changes. Sorry.

 -- Aubin Paul <aubin@debian.org>  Fri,  2 Mar 2001 00:25:21 -0500

scite (1.36-1) unstable; urgency=low

  * New upstream Release
  * Fixes for 64-bit Architectures (Closes: #85384)

 -- Aubin Paul <aubin@debian.org>  Tue, 18 Jan 2000 18:34:06 -0500

scite (1.35-1) unstable; urgency=low

  * New upstream Release

 -- Aubin Paul <aubin@punknews.org>  Tue, 18 Jan 2000 18:34:06 -0500

scite (1.34-2) unstable; urgency=low

  * Fixed Build-Depends line

 -- Aubin Paul <aubin@punknews.org>  Tue, 18 Jan 2000 18:34:06 -0500

scite (1.34-1) unstable; urgency=low

  * Initial Release.

 -- Aubin Paul <aubin@punknews.org>  Wed,  6 Dec 2000 23:00:06 -0500
