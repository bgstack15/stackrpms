Format: 3.0 (quilt)
Source: scite
Binary: scite
Architecture: any
Version: 1:5.5.4-100+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://scintilla.org/SciTE.html
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/scite
Vcs-Git: https://salsa.debian.org/debian/scite.git
Build-Depends: debhelper-compat (= 13), libgtk-3-dev, libglib2.0-dev, liblua5.4-dev
Package-List:
 scite deb editors optional arch=any
Files:
 00000000000000000000000000000000 1 scite.orig.tar.gz
 00000000000000000000000000000000 1 scite+stackrpms.debian.tar.xz
Original-Maintainer: Michael Vogt <mvo@debian.org>
