# use this file to display the differences between salsa upstream and my work.
# usage: stackrpms/scite/stackrpms-diff.sh | vi -
cd ~/dev
diff -x .*.swp -Naur salsa/scite/debian stackrpms/scite/debian
