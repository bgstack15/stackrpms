#!/bin/sh
# Startdate: 2022-12-08-5 09:19
# Purpose: prepare 
# Dependencies:
#     rmadison, git, awk, wget, tar
cd ~/dev/salsa/scite || { mkdir -p ~/dev/salsa; cd ~/dev/salsa ; git clone https://salsa.debian.org/debian/scite.git scite ; cd ~/dev/salsa/scite ; } ; git pull
# get latest scite source version, without character period
latest="$( rmadison --url debian --suite sid scite | awk -F'|' '{print $2}' | awk -F'-' '{print $1}' | tail -n1 | xargs )"
latest_no_period="$( echo "${latest}" | tr -d '.' )"
cd ~/dev ; wget --continue "https://www.scintilla.org/scite${latest_no_period}.tgz" ; ln -s "scite${latest_no_period}.tgz" scite_"${latest}".orig.tar.gz
mkdir -p scite.1 ; cd scite.1
tar -zxf ~/dev/"scite${latest_no_period}.tgz" ; cp -pr ~/dev/stackrpms/scite/debian . ;
