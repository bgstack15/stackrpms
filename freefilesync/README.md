# Readme for freefilesync

## FreeFileSync upstream
https://freefilesync.org
Forked from [https://github.com/PhantomX/chinforpms/blob/master/\_pasture/freefilesync/freefilesync.spec](https://github.com/PhantomX/chinforpms/blob/master/_pasture/freefilesync/freefilesync.spec)
https://gitlab.com/opensource-tracking/FreeFileSync

## Reason for being in stackrpms
Fedora, CentOS, and Devuan do not package FreeFileSync. I maintained a separate spec file for this application, but rebased to a fork from PhantomX.

## Alternatives
* rsync  traditional cli sync utility
* grsync  gtk frontend for rsync
* duplicati  sync with cloud storage offerings
* rclone  another cloud storage sync utility
### Alternative packages
* https://launchpad.net/~xtradeb/+archive/ubuntu/apps/+packages
* https://aur.archlinux.org/packages/freefilesync/

## Reverse dependency matrix
Distro         | FreeFileSync version | gtk version
-------------- | -------------------- | ----------------
CentOS 7       | 10.24                | 2
CentOS 8       | 10.24                | 2
Fedora 35      | 11.23                | 2
Fedora 36      | 11.25                | 2
Fedora Rawhide | 11.25                | 2
Devuan Ceres   | 11.28                | 3

## Additional info
Upstream officially still [uses gtk2](https://freefilesync.org/forum/viewtopic.php?t=7660&p=26079#p26063) but the code supports gtk3 starting around version 10.25. With version 10.25, only stdc++=20 is supported, so el7 cannot go any higher: I was unable to find g++-10 for el7.
On the rpm platforms, where gtk2 is still supported as a primary environment, I compile FreeFileSync against gtk2. On Devuan where gtk2 has been obsoleted, I compile against gtk3.
As of version 11.20 (2022-04), you need g++-11 for `-std=c++2b`.

As of version 11.28, I am really only maintaining the dpkg. The rpm is not getting updated, built, tested, or used.

## CentOS compilation of freefilesync

### Dependencies to build FreeFileSync on CentOS 7
All of the packages in the next section, and also
* devtoolset-7 [Software Collections (SCL)][3]
* custom patches provided in this repo.

### Sources of the bgstack15/FreeFileSync copr packages
Custom packages:
* openssl: [bgstack15][1]
* libssh2: [city-fan][2]
* curl: city-fan
* libmetalink: city-fan
* libpsl: city-fan

### Credits
This package, freefilesync, is made possible by the concerted efforts of many people and groups
* Zenju, author of the upstream project [FreeFileSync](http://freefilesync.org)
* [PhantomX](https://github.com/PhantomX/)/chinforpms [freefilesync spec](https://github.com/PhantomX/chinforpms/tree/master/freefilesync)
* My [original freefilesync spec](https://gitlab.com/bgstack15/freefilesync-rpm/)

### References
[1][https://bgstack15.wordpress.com]
[2][http://www.city-fan.org/ftp/contrib/yum-repo/]
[3][https://www.softwarecollections.org/en/scls/rhscl/devtoolset-7/]

## Differences from upstream
None
