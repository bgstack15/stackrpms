Format: 3.0 (quilt)
Source: freefilesync
Binary: freefilesync
Architecture: any
Version: 14.2-100+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://freefilesync.org/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 13~), g++-12, imagemagick, libcurl4-openssl-dev (>= 7.86.0-1), libglibmm-2.4-dev, libselinux1-dev, libssh2-1-dev (>= 1.10), libssl-dev (>= 3.0~), pkgconf, unzip, libgtk-3-dev, libwxgtk3.2-dev, libfontconfig-dev, zlib1g-dev
Package-List:
 freefilesync deb utils optional arch=any
Files:
 00000000000000000000000000000000 1 freefilesync.orig.tar.gz
 00000000000000000000000000000000 1 freefilesync+stackrpms.debian.tar.xz
