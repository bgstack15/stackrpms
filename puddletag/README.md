# puddletag upstream
https://gitlab.com/bgstack15/puddletag which itself is a fork of https://github.com/keithgg/puddletag
Forked from https://packages.debian.org/sid/puddletag

# Reason for being in stackrpms
Devuan has puddletag, however, the main version of the application depends on python2 and pyqt4. Debian (upstream of Devuan) just dropped the pyqt4 release, and the options to continue support is in either the pyqt5 branch of puddletag or the py3 (qt4) branch of puddletag. So now 

# Alternatives
https://build.opensuse.org/package/view_file/home:ecsos/puddletag/puddletag.spec?expand=1

# Reverse dependency matrix
Distro     | puddletag version
---------- | ----------------
Devuan     | 2.0.0

# Differences from upstream
