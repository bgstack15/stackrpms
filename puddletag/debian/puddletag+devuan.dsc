Format: 3.0 (quilt)
Source: puddletag
Binary: puddletag
Architecture: all
Version: 2.0.1-1+devuan
Maintainer: Ben Stack <bgstack15@gmail.com>
Homepage: http://docs.puddletag.net/
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 11), python3, dh-python
Build-Depends-Indep: python3-sphinx, python3-sphinx-bootstrap-theme, python3-wheel, python3-markdown, python3-pyrss2gen, python3-pyqt5, python3-configobj, python3-mutagen, python3-pyparsing
Package-List:
 puddletag deb sound optional arch=all
Files:
 00000000000000000000000000000000 1 puddletag_2.0.1.orig.tar.gz
 00000000000000000000000000000000 1 puddletag_2.0.1-1+devuan.debian.tar.xz
Original-Maintainer: Sandro Tosi <morph@debian.org>
Python-Version: all
