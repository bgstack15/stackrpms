Format: 3.0 (quilt)
Source: fprintd-tk
Binary: fprintd-tk
Architecture: all
Version: 0.0.1-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.ddns.net/cgit/fprintd-tk
Standards-Version: 4.6.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 fprintd-tk deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 fprintd-tk.orig.tar.gz
 00000000000000000000000000000000 1 fprintd-tk.debian.tar.xz
