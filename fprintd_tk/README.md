# Readme for fprintd-tk

## Overview
Fprintd-tk is a small project that shows a graphical interface for controlling fingerprint enrollments.

## Upstream
The original project is at <https://bgstack15.ddns.net/cgit/fprintd-tk>.

## Alternatives
Use `fprintd-enroll` on cli.

## Reason for being in stackrpms
The upstream should not be cluttered with the [OBS](https://build.opensuse.org/project/show/home:bgstack15) build assets.

## Improvements

## Dependencies
See the upstream project.

## References

## Differences from upstream
None.
