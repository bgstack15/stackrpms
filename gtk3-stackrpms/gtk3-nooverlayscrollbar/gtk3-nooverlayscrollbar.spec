Name:    gtk3-nooverlayscrollbar
Version: 7.0.1
Release: 1
Summary: disable overlay scrollbar in gtk3
License: GPL-2.0

URL:     https://git.devuan.org/devuan/clearlooks-phenix-cinnabar-theme/src/branch/suites/unstable/52gtk3-nooverlayscrollbar
Source0: https://git.devuan.org/devuan/clearlooks-phenix-cinnabar-theme/src/branch/suites/unstable/52gtk3-nooverlayscrollbar

BuildArch:  noarch

%description
Disables overlay scrollbar for all of gtk3 by adjusting global environment variables

%prep

%build

%install
# Devuan uses /etc/X11/Xsession.d but Fedora uses /etc/X11/xinit/xinitrc.d/
install -D -m0644 %{SOURCE0} %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d/"$( basename %{SOURCE0} )"

%files
%{_sysconfdir}/X11/xinit/xinitrc.d/*

%changelog
* Thu May 27 2021 B. Stack <bgstack15@gmail.com> - 7.0.1-1
- initial rpm release
