# Readme for gtk3-stackrpms
Gtk3-stackrpms is a custom set of things that together represent all of my requested fixes to gtk3.

* gtk3-nocsd
* gtk3-nooverlayscrollbar
* gtk3-automnemonics

## Reason for being in stackrpms
All the packages are native to Devuan but not Fedora, so this gtk3-stackrpms collection is designed to make it easy to install all of these on Fedora.

## gtk3-stackrpms upstreams
Because gtk3-stackrpms is a collection, there are multiple upstreams.

### gtk3-nocsd
Devuan GNU+Linux already packages gtk3-nocsd natively.
* [https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-nocsd=3-1](https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-nocsd=3-1)
* From Debian: [https://packages.debian.org/sid/gtk3-nocsd](https://packages.debian.org/sid/gtk3-nocsd)

Fedora rpm spec:
* [https://github.com/PhantomX/chinforpms/blob/main/gtk3-nocsd/gtk3-nocsd.spec](https://github.com/PhantomX/chinforpms/blob/main/gtk3-nocsd/gtk3-nocsd.spec)

Raw code:
* [https://github.com/PCMan/gtk3-nocsd/](https://github.com/PCMan/gtk3-nocsd/)

### gtk3-nooverlayscrollbar
Devuan GNU+Linux already packages gtk3-nooverlayscrollbar natively. The package is really just one file in /etc/X11/Xsession.d that sets a few key environment variables to 0.
* [https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-nooverlayscrollbar=7.0.1-3](https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-nooverlayscrollbar=7.0.1-3)
* [https://git.devuan.org/devuan/clearlooks-phenix-cinnabar-theme/src/branch/suites/unstable/52gtk3-nooverlayscrollbar](https://git.devuan.org/devuan/clearlooks-phenix-cinnabar-theme/src/branch/suites/unstable/52gtk3-nooverlayscrollbar)

Fedora rpm spec:
No upstream! My spec is original.

Raw code:
From the Devuan links above

### gtk3-automnemonics
This is a module provided by Gord Squash: [sgm](https://github.com/thesquash/sgm) and packaged in Devuan natively by me.
* [https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-automnemonics=0.90.0-2](https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-automnemonics=0.90.0-2)
* [https://git.devuan.org/devuan/sgm](https://git.devuan.org/devuan/sgm)

Fedora rpm spec:
No upstream! My spec is original.

Raw code:
* [sgm](https://github.com/thesquash/sgm)

## Alternatives
The Arch Linux community has tons of gtk3 tweaks and patches.

## Dependencies
[My build](https://bgstack15.ddns.net/cgit/gtk3-classic-build/) of [gtk3-classic](https://github.com/lah7/gtk3-classic) is very important too.

## Additional info

## References

## Differences from upstreams
Nothing noteworthy
