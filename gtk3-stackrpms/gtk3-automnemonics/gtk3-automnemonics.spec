Name:    gtk3-automnemonics
Version: 0.90.0
Release: 1
Summary: disable overlay scrollbar in gtk3
License: LGPL-2.1
# References:
#    https://docs.fedoraproject.org/en-US/packaging-guidelines/Meson/

URL:     https://github.com/thesquash/sgm
Source0: %{url}/archive/refs/tags/%{version}.tar.gz
Source1: 53gtk3-automnemonics

BuildRequires: gcc
BuildRequires: meson
BuildRequires: pkgconfig(gtk+-3.0)

%description
Adds a gtk3 module that reacts to theme setting gtk-auto-mnemonics = 0
to always show underscores. This feature was removed in upstream gtk3
in version 3.9.8

%prep
%autosetup -v -c

%build
cd sgm-%{version}
%meson
%meson_build

%install
cd sgm-%{version}
%meson_install
# Devuan uses /etc/X11/Xsession.d but Fedora uses /etc/X11/xinit/xinitrc.d/
install -D -m0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d/"$( basename %{SOURCE1} )"

%files
%{_sysconfdir}/X11/xinit/xinitrc.d/*
%{_libdir}/gtk-3.0/modules/*

%changelog
* Thu May 27 2021 B. Stack <bgstack15@gmail.com> - 0.90.0-1
- initial rpm release
