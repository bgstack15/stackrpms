Format: 3.0 (quilt)
Source: connman-gtk-xdg-autostart
Binary: connman-gtk-xdg-autostart
Architecture: all
Version: 0.0.1-1
Maintainer: Ben Stack <bgstack15@gmail.com>
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12)
Package-List:
 connman-gtk-xdg-autostart deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 connman-gtk-xdg-autostart.orig.tar.gz
 00000000000000000000000000000000 1 connman-gtk-xdg-autostart.debian.tar.xz
