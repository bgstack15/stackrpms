# Readme for connman-gtk-xdg-autostart
This package exists merely to drop a symlink in /etc/xdg/autostart for connman-gtk.

## connman-gtk-xdg-autostart upstream
None.

## Alternatives
Manually start connman-gtk in a graphical terminal session.

### How to use in Fluxbox
Either use `fbautostart`, or set in ~/.fluxbox/startup an entry for

    connman-gtk &

## Reason for being in stackrpms
I probably should submit a bug report to connman-gtk (on Debian) for making an xdg-autostart file. But why bother?

## Reverse dependency matrix
Distro     | connman-gtk-xdg-autostart version
---------- | ---------------------------------
All dpkg   | 0.0.1

## Make original tarball
Use this readme file as the only contents of a tarball in a single directory. Make the tarball resemble:

    $ tar -ztf ../connman-gtk-xdg-autostart_0.0.1.orig.tar.gz 
    connman-gtk-xdg-autostart-0.0.1/
    connman-gtk-xdg-autostart-0.0.1/README.md

## References
palemoon-ublock-origin/README

## Differences from upstream
None
