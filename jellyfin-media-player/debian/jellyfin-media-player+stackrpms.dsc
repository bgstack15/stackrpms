Format: 3.0 (quilt)
Source: jellyfin-media-player
Binary: jellyfin-media-player
Architecture: any
Version: 1.11.1-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
XSBC-Original-Maintainer: Ian Walton <ian@iwalton.com>, Joshua Boniface <joshua@boniface.me>
Homepage: https://jellyfin.org/
Standards-Version: 3.9.4
Vcs-Browser: https://github.org/jellyfin/jellyfin-media-player
Vcs-Git: https://github.org/jellyfin/jellyfin-media-player.git
Build-Depends: debhelper (>= 12), autoconf, automake, libtool, libharfbuzz-dev, libfreetype-dev | libfreetype6-dev, libfontconfig-dev | libfontconfig1-dev, libx11-dev, libxrandr-dev, libvdpau-dev, libva-dev, mesa-common-dev, libegl1-mesa-dev, yasm, libasound2-dev, libpulse-dev, libuchardet-dev, zlib1g-dev, libfribidi-dev, git, libgnutls28-dev, libgl-dev | libgl1-mesa-dev, libsdl2-dev, cmake, wget, python3, g++, qtwebengine5-dev, qtquickcontrols2-5-dev, libqt5x11extras5-dev, libcec-dev, libmpv-dev, unzip, qtbase5-private-dev
Package-List:
 jellyfin-media-player deb video optional arch=any
Files:
 00000000000000000000000000000000 1 jellyfin-media-player.orig.tar.gz
 00000000000000000000000000000000 1 jellyfin-media-player.debian.tar.xz
