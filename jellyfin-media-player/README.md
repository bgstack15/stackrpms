# README for jellyfin-media-player
Jellyfin-media-player is a client software for a GNU/Linux desktop system. The upstream project [jellyfin-media-player](https://github.com/jellyfin/jellyfin-media-player#building-at-a-glance-linux) provides .deb files, but I wanted to build my own anyways.

## Upstream
This project's upstream is at <https://github.com/jellyfin/jellyfin-media-player#building-at-a-glance-linux>. The stackrpms build recipe is at <https://bgstack15.ddns.net/cgit/stackrpms/tree/jellyfin-media-player>.

## Alternatives
Use the .deb files [released](https://github.com/jellyfin/jellyfin-media-player/releases) by the official team.

## Reason for existence
I just wanted to build this and host it in my own package repository. That is all.

## Using
Install it.

## Dependencies
See original package dependencies. I fixed the dpkg-source warnings about updated package names, like `libgl-mesa-dev -> libgl-dev`.

## Building
I use the Open Build Service to build this [package](https://build.opensuse.org/package/show/home:bgstack15/jellyfin-media-player).

## References
