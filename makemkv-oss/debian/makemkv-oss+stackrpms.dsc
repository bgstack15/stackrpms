Format: 3.0 (quilt)
Source: makemkv-oss
Binary: makemkv-oss
Architecture: any
Version: 1.17.9-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://www.makemkv.com
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), libavcodec-dev, libc6-dev, libexpat1-dev, libgl1-mesa-dev, qtbase5-dev, qtchooser, libssl-dev, pkg-config, zlib1g-dev
Package-List:
 makemkv-oss deb video optional arch=any
Files:
 00000000000000000000000000000000 1 makemkv-oss.orig.tar.gz
 00000000000000000000000000000000 1 makemkv-oss+stackrpms.debian.tar.xz
