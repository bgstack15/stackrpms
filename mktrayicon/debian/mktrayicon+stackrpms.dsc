Format: 3.0 (quilt)
Source: mktrayicon
Binary: mktrayicon
Architecture: any
Version: 0.0.1-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/jonhoo/mktrayicon/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), go-md2man, libgtk-3-dev, libx11-dev
Package-List:
 mktrayicon deb x11 optional arch=any
Files:
 00000000000000000000000000000000 1 mktrayicon.orig.tar.gz
 00000000000000000000000000000000 1 mktrayicon+stackrpms.debian.tar.xz
