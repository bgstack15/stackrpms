# Readme for mktrayicon

## mktrayicon upstream
[https://gitlab.com/bgstack15/mktrayicon/](https://gitlab.com/bgstack15/mktrayicon/)

## Alternatives
This package is based on my fork of an [original upstream](https://github.com/jonhoo/mktrayicon). I still monitor that one a little bit, but this fork is also distinctly my own.

## Reason for being in stackrpms
No one packages this small project for any distros that I know of.

## Reverse dependency matrix
Distro     | mktrayicon version
---------- | ---------------
All dpkg   | 0.0.1

## Differences from upstream
My version of mktrayicon includes a few merge requests that have been ignored by the original upstream:
* middle-click and scroll
* separators in menu
