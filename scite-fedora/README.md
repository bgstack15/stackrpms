# scite upstream
https://src.fedoraproject.org/rpms/scite/tree/f29

# Reason for being in stackrpms
Fedora and CentOS do not package scite and I have use of them. CentOS 7 relies on version 3 of the application, but CentOS 8 and Fedora relies on version 5.

Devuan Ceres already packages [scite](https://salsa.debian.org/debian/scite) but I want to add some customizations for myself.

# Reverse dependency matrix
Distro       | scite version
------------ | ---------------
CentOS 6     | 3.7.6
CentOS 7     | 3.7.6
CentOS 8     | 5.3.0
Fedora 35    | 5.3.0
Devuan Ceres | 5.3.0

# References
void linux [scite](https://github.com/void-linux/void-packages/blob/master/srcpkgs/scite/template) package guided me at version 5.0.1.

# Differences
## From upstream for Fedora
See file [stackrpms-scite.spec.diff](stackrpms-scite.spec.diff)
Customized SciTEGlobal.properties, to use my settings and also my custom lua script.
## Between scite-fedora and scite-el
Run file [fc-el-scite-diff.sh](fc-el-scite-diff.sh).

    scite-fedora/fc-el-scite-diff.sh | vi -

## From upstream for Devuan
Customized SciTEGlobal.properties, to use my settings and also my custom lua script.
