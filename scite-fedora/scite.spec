# Tunables
# el7 is on 3.x, el8/fc is on 5.x
%global pkgversion 5.3.1
# Use lua
%define with_lua 1
# Use bundled lua. If 0, then use distro lua
%define with_bundled_lua 0
# Add my custom lua script
%define with_custom_script 1
# Choose GTK=2 or GTK=3. El6 is hardcoded to 2.
%define gtk_ 3

# Calculated or derived flags. Do not mess with these.
%define tarballversion %( echo %pkgversion | tr -d '.' )
%define luaflag NO_LUA=1
%if 0%{with_lua}
%define luaflag %nil
%endif
%define gtkflag GTK3=1
%if 0%{gtk_} == 2
%define gtkflag %nil
%endif
# Turn off lto for local builds but leave them on for copr
%if !0%{?copr_username:1}
%global _lto_cflags %nil
%endif
%global __brp_check_rpaths %{nil}
%define scl_env %{nil}
%define scl_buildreq coreutils
%if 0%{?el6}
   %define scl_env devtoolset-7
   %define scl_buildreq devtoolset-7-toolchain
   %define gtk_ 2
%endif
%global all_build_flags '%{?_smp_mflags} LDFLAGS="%{?__global_ldflags} -shared -dynamiclib" \
   CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" %{gtkflag} DEBUG=1 %{luaflag} libdir=%{_libdir}'
# Just for vim:
#'

Name: scite
Version: %{pkgversion}
Release: 1%{?dist}
Summary: SCIntilla-based GTK text editor
License: MIT
Group: Applications/Editors
Url: http://www.scintilla.org/SciTE.html
Packager: B. Stack <bgstack15@gmail.com>

Source0: https://www.scintilla.org/scite%{tarballversion}.tgz
%if 0%{with_custom_script}
Source1: stackrpms.lua
%endif
Source2: SciTEGlobal.properties
Patch0: scite-utf8.patch
%if 0%{with_lua} && !0%{with_bundled_lua}
Patch2: use-lua5.4.4.patch
Patch3: 0007-Build-with-Debian-packaged-Lua.patch
%endif

BuildRequires: desktop-file-utils
BuildRequires: gcc-c++
BuildRequires: %{scl_buildreq}
%if 0%{gtk_} == 3
BuildRequires: gtk3-devel
%else
BuildRequires: gtk2-devel
%endif
%if 0%{with_lua} && !0%{with_bundled_lua}
BuildRequires: lua-devel
%endif

Provides: bundled(scintilla) = %{version}

%description
SciTE is a SCIntilla based Text Editor. Originally built to demonstrate
Scintilla, it has grown to be a generally useful editor with facilities for
building and running programs.

%prep
echo "USING gtkflag=\"%{gtkflag}\""
%setup -q -c
%patch0
%if !0%{with_lua}
#remove bundled lua
rm -rf scite/lua
%else
%if !0%{with_bundled_lua}
# Use distro lua
rm -rf scite/lua
%patch2 -p1
%patch3 -p1
%endif
%endif

%build
%if "%{?scl_env}" != ""
   scl enable %{scl_env} /bin/bash <<'EOFSCL'
%endif
make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags} -shared -dynamiclib" \
   CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" %{gtkflag} DEBUG=1 %{luaflag} -C lexilla/src \
   libdir=%{_libdir}
make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags} -shared -dynamiclib" \
   CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" %{gtkflag} DEBUG=1 %{luaflag} -C scintilla/gtk \
   libdir=%{_libdir}
#Build without lua support
make %{?_smp_mflags} LDFLAGS="%{?__global_ldflags}" \
   CFLAGS="%{optflags}" CXXFLAGS="%{optflags}" %{gtkflag} DEBUG=1 %{luaflag} -C scite/gtk \
   libdir=%{_libdir}
%if "%{?scl_env}" != ""
EOFSCL
%endif

%install
make DESTDIR=%{buildroot} %{gtkflag} -C scite/gtk install libdir=%{_libdir}
ln -s SciTE %{buildroot}%{_bindir}/scite

# include man-page
install -D -p -m 644 scite/doc/scite.1 %{buildroot}%{_mandir}/man1/scite.1

desktop-file-install --delete-original  \
  --dir %{buildroot}%{_datadir}/applications            \
  --remove-category Application                         \
  --remove-key Encoding					\
  %{buildroot}%{_datadir}/applications/SciTE.desktop

%if 0%{with_custom_script}
install -D -p -m 644 %{SOURCE1} %{buildroot}%{_datadir}/scite/stackrpms.lua
%endif
install -D -p -m 666 %{SOURCE2} %{buildroot}%{_datadir}/scite/SciTEGlobal.properties
%if 0%{?fedora}
%posttrans
/usr/bin/update-desktop-database &> /dev/null || :
%else
%post
/usr/bin/update-desktop-database &> /dev/null || :

%postun
/usr/bin/update-desktop-database &> /dev/null || :
%endif

%files
%doc scite/License.txt
%{_mandir}/man1/scite.1*
%{_bindir}/SciTE
%{_bindir}/scite
%{_datadir}/scite/
%{_datadir}/pixmaps/*
%{_datadir}/applications/*
%{_libdir}/*

%changelog
* Sun Oct 16 2022 B. Stack <bgstack15@gmail.com> - 5.3.1-1/3.7.6-1
- Update version

* Tue Sep 06 2022 B. Stack <bgstack15@gmail.com> - 5.3.0-1/3.7.6-1
- Update version

* Mon Jul 11 2022 B. Stack <bgstack15@gmail.com> - 5.2.4-1/3.7.6-1
- Update version

* Tue May 31 2022 B. Stack <bgstack15@gmail.com> - 5.2.3-1/3.7.6-1
- Update version
- split back out patch build-with-debian-packaged-lua

* Mon Apr 04 2022 B. Stack <bgstack15@gmail.com> - 5.2.2-2/3.7.6-1
- Update version
- Add tunables: gtk, lua, bundled lua, add stackrpms.lua
- Add customized SciTEGlobal.properties

* Thu Mar 24 2022 B. Stack <bgstack15@gmail.com> - 5.2.1-1/3.7.6-1
- Update version

* Thu Feb 10 2022 B. Stack <bgstack15@gmail.com> - 5.2.0-1/3.7.6-1
- Update version

* Wed Dec 08 2021 B. Stack <bgstack15@gmail.com> - 5.1.5-1/3.7.6-1
- Update version

* Mon Nov 08 2021 B. Stack <bgstack15@gmail.com> - 5.1.4-1/3.7.6-1
- Update version
- set __brp_check_rpaths to ignore hardcoded /usr/lib64 rpath on SciTE binary

* Thu Sep 30 2021 B. Stack <bgstack15@gmail.com> - 5.1.3-1/3.7.6-1
- Update version

* Wed Jul 28 2021 B. Stack <bgstack15@gmail.com> - 5.1.1-1/3.7.6-1
- Update version

* Tue Jun 29 2021 B. Stack <bgstack15@gmail.com> - 5.1.0-1/3.7.6-1
- Update version

* Mon Jun 07 2021 B. Stack <bgstack15@gmail.com> - 5.0.3-1/3.7.6-1
- Update version

* Tue Apr 20 2021 B. Stack <bgstack15@gmail.com> - 5.0.1-1/3.7.6-1
- Update version

* Thu Oct 01 2020 B. Stack <bgstack15@gmail.com> - 4.4.5-1/3.7.6-1
- Update version

* Thu Jul 23 2020 B. Stack <bgstack15@gmail.com> - 4.4.4-1/3.7.6-1
- Update version

* Wed Jun 03 2020 B. Stack <bgstack15@gmail.com> - 4.4.3-1/3.7.6-1
- Update version

* Tue May 05 2020 B. Stack <bgstack15@gmail.com> - 4.3.3-1/3.7.6-1
- Update version

* Wed Mar 04 2020 B. Stack <bgstack15@gmail.com> - 4.3.1-1/3.7.6-1
- Update version

* Thu Jan 16 2020 B. Stack <bgstack15@gmail.com> - 4.3.0-1/3.7.6-1
- Update version

* Thu Dec 19 2019 B. Stack <bgstack15@gmail.com> - 4.2.3-1/3.7.6-1
- Update version

* Mon Nov 11 2019 B. Stack <bgstack15@gmail.com> - 4.2.2-1/3.7.6-1
- Update version

* Mon Nov 11 2019 B. Stack <bgstack15@gmail.com> - 4.2.1-1/3.7.6-1
- Rewritten to build Fedora and Enterprise Linux versions from a single spec with minor flags changed

* Thu Jun  6 2019 B. Stack <bgstack15@gmail.com> - 4.1.6-1/3.7.5-1
- Rewritten to build Fedora and Enterprise Linux versions from a single spec with minor flags changed

* Mon May  6 2019 B. Stack <bgstack15@gmail.com> - 4.1.5-1
- Brought to latest version

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.7.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.7.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Aug 12 2017 Johan Swensson <kupo@kupo.se> 3.7.5-1
- Update to 3.7.5

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.7-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Sep 27 2016 Johan Swensson <kupo@kupo.se> - 3.6.7-1
- Update to 3.6.7

* Mon Feb 22 2016 Johan Swensson <kupo@kupo.se> - 3.6.3-1
- Update to 3.6.3

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Johan Swensson <kupo@kupo.se> - 3.6.2-1
- Update to 3.6.2

* Fri Sep 18 2015 Johan Swensson <kupo@kupo.se> - 3.6.1-1
- Update to 3.6.1

* Sun Aug 23 2015 Johan Swensson <kupo@kupo.se> - 3.6.0-1
- Update to 3.6.0

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.5.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed May 06 2015 Johan Swensson <kupo@kupo.se> - 3.5.5-2
- Added bundled(scintilla)

* Thu Apr 30 2015 Johan Swensson <kupo@kupo.se> - 3.5.5-1
- Update to 3.5.5

* Sat Feb 21 2015 Johan Swensson <kupo@kupo.se> - 3.5.3-1
- Update to 3.5.3

* Sat Dec 20 2014 Johan Swensson <kupo@kupo.se> - 3.5.2-1
- Update to 3.5.2

* Thu Oct 16 2014 Johan Swensson <kupo@kupo.se> - 3.5.1-1
- Update to 3.5.1

* Mon Aug 18 2014 Johan Swensson <kupo@kupo.se> - 3.5.0-1
- Update to 3.5.0

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.4.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Jul 24 2014 Johan Swensson <kupo@kupo.se> - 3.4.4-1
- update to 3.4.4
- remove bundled lua library
- build without lua scripting support for now
- use --remove-key instead of sed
- add update-desktop-database scriptlets
- remove unnecessary README file
- add support for el6
- fix build conditional
- fixed BuildRequires conditional
- fix LDFLAGS macro
- remove deprecated key in desktop file
- use GTK3 (el7 and fedora)
- drop vendor from desktop-file-install
- use correct LDFLAGS and CXXFLAGS
- install man page correctly
- drop no longer needed install step and defattr

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.22-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.22-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.22-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Dec 4 2010 Jorge Torres <jtorresh@gmail.com> 2.22-1
- Update to 2.22

* Sat Apr 17 2010 Jorge Torres <jtorresh@gmail.com> 2.11-1
- Update to 2.11
- Fix bug #564689

* Thu Oct 1 2009 Jorge Torres <jtorresh@gmail.com> 2.01-1
- Update to 2.01

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.77-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Apr 4 2009 Jorge Torres <jtorresh@gmail.com> 1.77-1
- Upgrade to 1.77

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.74-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.74-3
- Autorebuild for GCC 4.3

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 1.74-2
- Rebuild for selinux ppc32 issue.

* Wed Jun 20 2007 Jorge Torres <jtorresh@gmail.com> 1.74-1
- Upgrade to 1.74.
- Default to UTF-8 encoding (fixes bug #240558).
- Remove iconv workaround for desktop file encoding.

* Fri Sep 01 2006 Jorge Torres <jtorresh@gmail.com> 1.71-1
- Upgrade to 1.71
- Use 'iconv' to work around a desktop file encoding issue

* Thu Jun 22 2006 Jorge Torres <jtorresh@gmail.com> 1.70-1
- Upgrade to 1.70
- Drop man-page capitalization patch

* Sun Jun 04 2006 Jorge Torres <jtorresh@gmail.com> 1.69-3
- Remove usage of MultipleArgs in desktop file.

* Fri Jun 02 2006 Jorge Torres <jtorresh@gmail.com> 1.69-2
- Changed license to MIT.
- Added patches to support compiling with %%{optflags}.
- Misc fixes.

* Thu Jun 01 2006 Jorge Torres <jtorresh@gmail.com> 1.69-1
- Initial package version.
