# use this file to display the differences between fedora and el scite rpm source directories
# usage: scite-fedora/fc-el-scite-diff.sh | vi -
diff -x debian -x stackrpms*.diff -x '*scite*.sh' -x '*z' -x '.*.swp' -Naur scite-fedora scite-el
