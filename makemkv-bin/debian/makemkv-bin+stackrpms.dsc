Format: 3.0 (quilt)
Source: makemkv-bin
Binary: makemkv-bin
Architecture: any
Version: 1.17.9-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://www.makemkv.com
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), makemkv-oss
Package-List:
 makemkv-bin deb video optional arch=any
Files:
 00000000000000000000000000000000 1 makemkv-bin.orig.tar.gz
 00000000000000000000000000000000 1 makemkv-bin+stackrpms.debian.tar.xz
