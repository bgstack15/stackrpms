# Readme for python-freeipa

## python-freeipa upstream

1. [Upstream](https://github.com/waldur/python-freeipa)
2. [Docs](https://python-freeipa.readthedocs.io/en/latest/)

## Reason for being in stackrpms
Debian does not package python-freeipa, which is a separate client software for FreeIPA and is not part of the official components of a freeipa client server installation. I wanted python-freeipa mostly for [freeipa-cert-alert](https://bgstack15.ddns.net/cgit/freeipa-cert-alert/).

## Alternatives
* ipa commands that are part of native client server installation

### Alternative packages
* https://src.fedoraproject.org/rpms/python-freeipa

## Dependencies
Distro         | python3-freeipa version
-------------- | -----------------------
Devuan Ceres   | 1.0.7

## Additional info

### Credits
[1][https://codesearch.debian.net/] Debian Code Search

### References
[1]: <https://packages.debian.org/sid/python3-yaml> PyYAML packaging in Debian

## Differences from upstream
None
