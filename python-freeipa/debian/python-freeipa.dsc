Format: 3.0 (quilt)
Source: python-freeipa
Binary: python3-freeipa
Architecture: any
Version: 1.0.7-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/waldur/python-freeipa/
Standards-Version: 4.6.1
Vcs-Browser: https://bgstack15.ddns.net/cgit/python-freeipa/
Vcs-Git: https://bgstack15.ddns.net/cgit/python-freeipa/
Build-Depends: debhelper-compat (= 13), dh-python, python3-all-dev, python3-setuptools
Package-List:
 python3-freeipa deb python optional arch=any
Files:
 00000000000000000000000000000000 1 python-freeipa.orig.tar.gz
 00000000000000000000000000000000 1 python-freeipa.debian.tar.xz
