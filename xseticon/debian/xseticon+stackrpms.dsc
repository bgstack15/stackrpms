Format: 3.0 (quilt)
Source: xseticon
Binary: xseticon
Architecture: any
Version: 0.1+bzr14-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: http://www.leonerd.org.uk/code/xseticon/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), libgd-dev, libglib2.0-dev, libjpeg-dev, libxmu-dev
Package-List:
 xseticon deb x11 optional arch=any
Files:
 00000000000000000000000000000000 1 xseticon.orig.tar.gz
 00000000000000000000000000000000 1 xseticon+stackrpms.debian.tar.xz
