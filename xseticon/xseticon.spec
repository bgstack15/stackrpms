%define debug_package %{nil}
Name:		xseticon
Version:	0.1
Release:	2%{?dist}
Summary:	set x application icons

Group:	Applications/X11
License:	GPLv2
URL:		http://www.leonerd.org.uk/code/xseticon/
#Source0:	%%{url}/%%{name}-%%{version}+bzr14.tar.gz
Source0: https://gitlab.com/bgstack15/big-sources/raw/master/%{name}/%{name}-%{version}+bzr14.tar.gz
Source1: README

BuildRequires:	libXmu-devel
BuildRequires:	gcc
BuildRequires:	gd-devel
BuildRequires:	glib2-devel
#BuildArch: all
#Requires:	
%global dirname %{name}-%{version}+bzr14

%description
Xterm, and likely many other X11 programs, do not set themselves window icons, which window managers typically use to represent that program window in switcher lists, taskbars, and so on. This program can set the X11 window icon for any given window, to that of a given image file.

%prep
%setup -q -c %{dirname}
cd *
cp -p %{SOURCE1} .
# install to prefix provided by rpmbuild
sed -i -r -e '/^\s*PREFIX=.*/s:^\s*PREFIX=.*:PREFIX=$(DESTDIR)/usr:' Makefile

%build
#%%configure
cd *
make %{?_smp_mflags}

%install
cd *
%make_install

%files
%{_bindir}/%{name}
%if 0%{?el6}%{?el7}
%doc */LICENSE
%else
%license */LICENSE
%endif
%doc */README

%changelog
* Wed Dec 26 2018 B Stack <bgstack15@gmail.com> 0.1-1
- Initial rpm built
