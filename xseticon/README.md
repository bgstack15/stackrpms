# xseticon upstream
http://www.leonerd.org.uk/code/xseticon/

# Reason for being in stackrpms
No one packages xseticon, and I find it useful. This is an original package.

# Reverse dependency matrix
Distro     | xseticon version
---------- | ----------------
All        | 0.1

# Differences from upstream
None
