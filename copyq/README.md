# CopyQ upstream
https://github.com/hluk/CopyQ
https://src.fedoraproject.org/rpms/copyq/blob/f30/f/copyq.spec

# Reason for being in stackrpms
CentOS does not package CopyQ, which is my preferred clipboard manager for desktop environments.

# Reverse dependency matrix
Distro     | copyq version
---------- | ---------------
CentOS 7   | 3.9.2
CentOS 8   | 3.9.2

# Differences from upstream
See file [stackrpms-copyq.spec.diff](stackrpms-copyq.spec.diff)
