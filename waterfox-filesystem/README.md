# waterfox-filesystem upstream
Forked from https://github.com/PhantomX/chinforpms/blob/master/waterfox-ublock-origin/waterfox-ublock-origin.spec

# Reason for being in stackrpms
Needed for Waterfox.

# Reverse dependency matrix
Distro     | Waterfox version | waterfox-filesystem version
---------- | ---------------- | ---------------------------
rpm        | all              | any (1.0)

# Differences from upstream
None
