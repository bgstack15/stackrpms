# Readme for librewolf in stackrpms DEPRECATED

**DEPRECATED!**

See package [librewolf-prefs-stackrpms](../librewolf-prefs-stackrpms) as of 2022-10.

## Librewolf upstream
This is a custom patchset on top of the [Debian OBS](https://gitlab.com/librewolf-community/browser/debian) repository for Devuan Ceres.

## Reason for being in stackrpms
These are changes just for stackrpms and are not appropriate for the upstream AfterMozilla OBS for LibreWolf.

## Alternatives
I just use the [AfterMozilla LibreWolf](https://build.opensuse.org/package/show/home:bgstack15:aftermozilla/librewolf) package which I also build. That is the upstream for this package.

## Dependencies
Exact same as Mozilla Firefox.

## References
Check the contents of internal prefs package for the various web browsers' prefs.js-type files.

## Differences from upstream
The differences are boiled down in simple diff files here. The main differences are just adding my custom prefs.js (now handled by project librewolf-stackrpms in this same [repository](https://bgstack15.ddns.net/cgit/librewolf-prefs-stackrpms/) ([gitlab](https://gitlab.com/bgstack15/librewolf-prefs-stackrpms)) and deleting some search engine entries in the suggestion list.
