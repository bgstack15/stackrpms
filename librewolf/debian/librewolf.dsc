Format: 3.0 (quilt)
Source: librewolf
Binary: librewolf
Architecture: any all
Version: 102.0-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
XSBC-Original-Maintainer: Maintainers of Mozilla-related packages <team+pkg-mozilla@tracker.debian.org>
Standards-Version: 3.9.8.0
Build-Depends: autotools-dev, debhelper (>= 9.20160114), libx11-dev, libx11-xcb-dev, libxt-dev, libgtk-3-dev, libglib2.0-dev, libdrm-dev, libstartup-notification0-dev, libjpeg-dev, zlib1g-dev, libreadline-dev, python3 (>= 3.6), dpkg-dev, libnspr4-dev (>= 2:4.32~), libnss3-dev (>= 2:3.72~), libvpx-dev (>= 1.8.0), libdbus-glib-1-dev, libffi-dev, libevent-dev, libasound2-dev, libjack-dev, libpulse-dev, yasm, nasm (>= 2.14) [amd64 i386], rustc (>= 1.53), cargo (>= 0.54), llvm-dev, libclang-dev, clang, cbindgen (>= 0.19.0), nodejs (>= 10), zip, unzip, locales, xvfb, xfonts-base, xauth, ttf-bitstream-vera, fonts-freefont-ttf, fonts-dejima-mincho, iso-codes
Build-Conflicts: graphicsmagick-imagemagick-compat, libhildonmime-dev, liboss4-salsa-dev, libosso-dev
Package-List:
 librewolf deb web optional arch=any
Files:
 00000000000000000000000000000000 1 librewolf.debian.tar.xz
 00000000000000000000000000000000 1 librewolf.orig.tar.xz
