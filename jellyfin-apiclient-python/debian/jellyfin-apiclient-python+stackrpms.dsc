Format: 3.0 (quilt)
Source: jellyfin-apiclient-python
Binary: jellyfin-apiclient-python
Architecture: any
Version: 1.9.2-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://github.com/jellyfin/jellyfin-apiclient-python
Standards-Version: 4.5.1
Testsuite: autopkgtest
Build-Depends: debhelper-compat (= 13), dh-python, python3-all-dev, python3-requests, python3-setuptools, python3-websocket
Package-List:
 jellyfin-apiclient-python deb video optional arch=any
Files:
 00000000000000000000000000000000 1 jellyfin-apiclient-python.orig.tar.gz
 00000000000000000000000000000000 1 jellyfin-apiclient-python.debian.tar.xz
