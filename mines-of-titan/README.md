# Mines of Titan upstream
https://gitlab.com/bgstack15/big-sources/blob/master/mines-of-titan/mines-of-titan-0.0.2.tar.gz

# Reason for being in stackrpms
Devuan does not package this closed-source RPG game.

# Reverse dependency matrix
Distro     | Mines of Titan version
---------- | ---------------
All        | 0.0.3

# Differences from upstream
None
