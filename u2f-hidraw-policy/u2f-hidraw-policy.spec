%global _hardened_build 1

Name:           u2f-hidraw-policy
Version:        1.0.2
Release:        10%{?dist}
Summary:        Udev rule to allow desktop access to HIDRAW U2F tokens

License:        LGPLv2+
URL:            https://github.com/amluto/u2f-hidraw-policy/
Source0:        https://github.com/amluto/%{name}/archive/%{version}.tar.gz

BuildRequires:	gcc

# We need the udev libraries and the _udevrulesdir macro
BuildRequires:  systemd systemd-devel

# systemd owns /usr/lib/udev/rules.d
Requires:       systemd

%description
u2f-hidraw-policy is a udev helper that detects U2F HID tokens as described
by the U2F spec.


%prep
%setup -q

%build
%make_build CFLAGS="%{optflags} %{__global_ldflags}"

%install
make install DESTDIR="%{buildroot}/usr"

%files
%{_udevrulesdir}/60-u2f-hidraw.rules
%{_udevrulesdir}/../u2f_hidraw_id

%license LICENSE
%doc README.md

%changelog
* Sat Jul 27 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sun Feb 03 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Sat Jul 21 2018 luto@kernel.org - 1.0.2-8
- Add BR: gcc

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Oct 30 2015 Andy Lutomirski <luto@mit.edu> - 1.0.2-1
- New version, simplifying packaging
- Harden the build

* Mon Oct 19 2015 Andy Lutomirski <luto@mit.edu> - 1.0.1-1
- New version, fixing LICENSE

* Mon Oct 19 2015 Andy Lutomirski <luto@kernel.org> - 1.0-1
- New package
