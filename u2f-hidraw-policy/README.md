# u2f-hidraw-policy upstream
https://src.fedoraproject.org/rpms/u2f-hidraw-policy.git

# Reason for being in stackrpms
CentOS 8 does not package u2f-hidraw-policy yet and waterfox depends on any version of it.

# Reverse dependency matrix
Distro     | Waterfox version | u2f-hidraw-policy version
---------- | ---------------- | -------------------------
CentOS 8   | any              | any (1.0.2)

# Differences from upstream
None
