Format: 3.0 (quilt)
Source: fluxbox
Binary: fluxbox
Architecture: any
Version: 1.4.0-2+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
XSBC-Original-Maintainer: Dmitry E. Oboukhov <unera@debian.org>
Homepage: http://fluxbox.org
Standards-Version: 3.9.4
Build-Depends: autoconf, automake, autotools-dev, bzip2, debhelper (>= 9), dh-autoreconf, libfribidi-dev, libgtk2.0-dev, libimlib2-dev, libtool, libx11-dev, libxext-dev, libxft-dev, libxinerama-dev, libxpm-dev, libxrandr-dev, libxt-dev
Package-List:
 fluxbox deb x11 optional arch=any
Files:
 00000000000000000000000000000000 1 fluxbox.orig.tar.gz
 00000000000000000000000000000000 1 fluxbox.debian.tar.xz
