# Readme for fluxbox

## Overview
This is for my customized dpkg of [Fluxbox](http://fluxbox.org/index.php) which exists quite happily in vanilla form in [Debian Sid](https://packages.debian.org/sid/fluxbox).

## fluxbox upstream
I modified the debian/ from snapshot.debian.org/package/[fluxbox](http://snapshot.debian.org/package/fluxbox/)/

This set of overlay patches probably will not be maintained, so user beware! That being said, upstream is frozen so you won't get behind here either.

## Reason for being in stackrpms
I wrote exactly one patch: [debian/patches/add-clientmachine-if-forwarded.patch](debian/patches/add-clientmachine-if-forwarded.patch)
Also by using the custom package here I can incorporate all the useful patches added in what appears to be a neo-upstream on [github](https://github.com/fluxbox/fluxbox) which has neat patches like [toolbar: allow labeled general action buttons](https://github.com/fluxbox/fluxbox/commit/393ba45f91480bb10f510248837c6051b7ff6a03) and [document iconified title deco](https://github.com/fluxbox/fluxbox/commit/2047b1a2ba9ea5d3df77c8de9b4e2b3fd3a40a6d) which are not present in the Debian Fluxbox 1.3.5 release.

I chose Fluxbox as my daily driver desktop environment/window manager because it is stable. And it is stable, with a few minor flaws!

So this one patch I wrote adds to the titlebar of a forwarded window "(on $CLIENT)" which was inspired by [Xfwm4](https://gitlab.xfce.org/xfce/xfwm4/-/blob/master/src/client.c#L215).

## Improvements

* Add a configurable option for "Show (on $CLIENT)" so users can decide if they like it.
* Some programs do not properly maintain `WM_CLIENT_MACHINE` X property, like `xfe(1)` so that could be misleading.
* Maybe store local hostname on the Display, like Xfwm4 so we do not need to keep calling gethostname()
* Find way to rearrange the iconbar window entries ([issue #94](https://sourceforge.net/p/fluxbox/feature-requests/94/) in original repo) which was also requested in one of the github forks ([luebking/fluxbox#1](https://github.com/luebking/fluxbox/issues/1) with comments:

> If you however mean to drag around the icon, that's probably not too hard to do in src/Icon{Button,barTool}*

## Alternatives
Just use the native package fluxbox for the given distro.

Github indicates a large number of [forks exist](https://github.com/fluxbox/fluxbox/network/members) of the source. Almost all of them do not provide substantive improvements.

## Dependencies
Exact same as native package fluxbox.

## References
Every link in this document.
Official upstream link?: <http://git.fluxbox.org/fluxbox.git/>

## Differences from upstream
Only one d/p/series and d/p/add-clientmachine-if-forwarded.patch
