# oddjob-mkhomedir upstream
None

# Reason for being in stackrpms
This is a dummy package for making "oddjob-mkhomedir" installable so FreeIPA client can be installed. While other mkhomedir methods exist in Devuan, freeipa has a specifically-named dependency on oddjob-mkhomedir.

# Reverse dependency matrix
Distro       | freeipa-client version | oddjob-mkhomedir version
------------ | ---------------------- | ------------------------
Devuan ceres | 4.8.1                  | any

# Differences from upstream
None
